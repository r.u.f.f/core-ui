import {IEndpoint} from 'projects/core-ui/src/lib/interfaces/endpoint/IEndpoint';
import {ServiceLocator} from 'projects/core-ui/src/lib/locators/service.locator';

export function CollectionProviderDecorator(options: {
  endpoint: IEndpoint,
  apiUrl?: string
}) {
  return function <T extends { new(...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
      $dp = ServiceLocator.getDataProviderFactory().collection(options.endpoint, this);
    };
  };
}
