import {
  FilterType,
  IFilterItem,
  IResp
} from 'projects/core-ui/src/lib/interfaces/data-structures/queries';

export interface IFilter {
  toRequestJSON(): IResp[];

  getSettings(): IFilterItem[];
}

export class Filter implements IFilter {

  constructor(private filterItems: IFilterItem[]) {

  }

  public toRequestJSON(): IResp[] {
    return this
      .getSettings()
      .map(i => i.toResponse())
      .filter((i) => {
        return (i.val !== null && i.val !== undefined) ||
          (i.minVal !== null && i.minVal !== undefined) ||
          (i.maxVal !== null && i.maxVal !== undefined);
      })
      .filter((i) => {
        return !(Array.isArray(i.val) && i.val.length === 0);
      })
      .filter((i) => {
        return !(i.type === FilterType.ILIKE && !i.val);
      });
  }

  public getSettings(): IFilterItem[] {
    return this.filterItems;
  }

}
