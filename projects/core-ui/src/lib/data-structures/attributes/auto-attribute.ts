import {Attribute} from 'projects/core-ui/src/lib/data-structures/attributes/attribute';
import {IBaseModel} from 'projects/core-ui/src/lib/interfaces/data-structures/models/IBaseModel';

export class AutoAttribute extends Attribute {

  constructor(private labels: { [s: string]: any },
              private attributes: { [s: string]: any },
              private name,
              private model: IBaseModel) {
    super();

    if (!this.model.schema()[this.name]) {
      throw new Error(`Incorrect implementation. Model ${this.model.constructor.name} have no attribute [${this.name}]!`);
    }
  }

  setValue(value: any): this {
    this.attributes[this.name] = value;
    this.model.onAttributesChanged().emit(this.name);
    return this;
  }

  value(): any {
    return this.attributes[this.name];
  }

  valueByKey(key: string): any {
    return this.attributes[this.name][key];
  }

  label(): string {
    return this.labels[this.name];
  }

  errors(): Array<string> {
    return this.model.errors(this.name);
  }

  is(val: any): boolean {
    return this.attributes[this.name] === val;
  }

  public getName(): string {
    return this.name;
  }

  public description(): string {
    return this.model.schema()[this.name].$d ? this.model.schema()[this.name].$d : '';
  }
}
