import {ABaseModel} from 'projects/core-ui/src/lib/data-structures/models/base.model';
import {IModel} from 'projects/core-ui/src/lib/interfaces/data-structures/models/IModel';
import {IModelDataProvider} from 'projects/core-ui/src/lib/interfaces/data-provider/IModelDataProvider';

export abstract class AModel extends ABaseModel implements IModel {
    /**
     * @inheritDoc
     */
    dataProvider(): IModelDataProvider {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    }

    /**
     * @inheritDoc
     */
    pk(): string {
        if (this.schema()['id'] === undefined) {
            throw new Error(
                `Could not to find default primary key in model.
You must redeclare pk() in model or define 'id' field in schema.`
            );
        }
        return 'id';
    }

    /**
     * @inheritDoc
     */
    requestFormatter(): object {
        return {};
    }

    getDataToSave(): object {
        return super.getDataToSave();
    }
}
