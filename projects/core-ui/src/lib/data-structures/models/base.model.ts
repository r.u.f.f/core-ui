import {EventEmitter} from '@angular/core';
import {validate} from 'validate.js';

import {
  AttributesType,
  DescriptionsType, ErrorsType, ErrorsTypeV2,
  IBaseModel,
  LabelsType,
  SchemaType
} from 'projects/core-ui/src/lib/interfaces/data-structures/models';
import {IAttribute} from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
import {AutoAttribute} from 'projects/core-ui/src/lib/data-structures/attributes';
import {EnumModel} from 'projects/core-ui/src/lib/data-structures/models/enum.model';

export abstract class ABaseModel implements IBaseModel {

  private ALLOW_NULL_VALUES = true;

  /**
   * Internal dictionary for keeping attributes
   * @private
   */
  protected attributes = {};
  protected enums = {};
  protected editMode = false;

  protected $services = {
    $onAttributesChanged: new EventEmitter<string>(),
    $nullValuesMap: {
      string: '',
      number: 0,
      boolean: false,
      bool: false,
      array: [],
      date_int_ms: new Date(),
    },
    $errors: {},
    $scenario: {
      active: {},
      list: [],
    }
  };


  public constructor() {
    this.initEmptyAttributes();
  }

  public startEdit(): this {
    this.editMode = true;

    return this;
  }

  public finishEdit(): this {
    this.editMode = false;

    return this;
  }

  isEditModeEnabled(): boolean {
    return this.editMode;
  }

  /**
   * @inheritDoc
   */
  abstract schema(): SchemaType;

  /**
   * Detect null values for each type
   * @param {*} type
   * @return {*}
   */
  private nullValueByType(type: any): any {

    if (Array.isArray(type)) {
      return this.$services.$nullValuesMap.array;
    }

    if (typeof type === 'function') {
      const c: any | IBaseModel = type;
      return new c();
    }

    if (this.$services.$nullValuesMap.hasOwnProperty(type)) {
      return this.$services.$nullValuesMap[type];
    }
    return undefined;
  }

  /**
   * Initi attributes after model construct
   * @return {IBaseModel}
   */
  private initEmptyAttributes(): IBaseModel {
    Object
      .keys(this.schema())
      .forEach(n => this.setAttributeBySchema(n, this.nullValueByType(this.schema()[n].$t)));
    return this;
  }

  /**
   * Defining each attribute by declared schema
   * @param {string} name
   * @param value
   * @return {IBaseModel}
   */
  private setAttributeBySchema(name: string, value: any): this {
    if (this.schema().hasOwnProperty(name)) {

      const cast = (name: string, value: any, type?: string | any): any => {
        /**@override IBaseModel this */
        const t = type ? type : this.schema()[name].$t;
        if (typeof t === 'function') { // we think that it is another model
          const a = (new t());
          if (value != null) {
            a.setAttributes(value);
          }
          return a;
        }
        switch (t) {
          case 'string':
            return String(value);
          case 'date_int_ms':
            return new Date(value);
          case 'number':
            return Number(value);
          case 'bool':
          case 'boolean':
            if (value === '0') {
              value = false;
            }
            if (value === '1') {
              value = true;
            }
            return Boolean(value);
        }
      };

      if (Array.isArray(this.schema()[name].$t)) {
        this.attributes[name] = [];
        if (this.schema()[name].$t.length !== 1) {
          throw new Error(`Allowed only one type for array definitions for attribute "${name}"`);
        }
        if (!Array.isArray(value)) {
          if (this.ALLOW_NULL_VALUES === true && value === null) {
            value = [];
          } else {
            throw new Error(`Value must be array according to scheme definition for attribute "${name}"`);
          }
        }
        value.forEach((val) => {
          this.attributes[name].push(cast(name, val, this.schema()[name].$t[0]));
        });

      } else {
        this.attributes[name] = cast(name, value);
      }

      this.fillAttributesCopies();
    }
    return this;
  }

  private fillAttributesCopies(): this {
    Object
      .keys(this.schema())
      .forEach((attrName) => {
        if (this.schema()[attrName].$c &&
          this.attributes.hasOwnProperty(this.schema()[attrName].$c)) {
          this.attributes[attrName] = this.attributes[this.schema()[attrName].$c];
        }
      });
    return this;
  }

  /**
   * @inheritDoc
   */
  labels(): LabelsType {
    return Object
      .keys(this.schema())
      .reduce((p: { [s: string]: string }, c) => {
        if (this.schema()[c].$l) {
          p[c] = this.schema()[c].$l;
          return p;
        }
        p[c] = c
          .replace(/([A-Z])/g, ' $1')
          .replace(/^./, function (str) {
            return str.toUpperCase();
          });
        p[c] = c.split('_').map((word: string) => {
          return word.charAt(0).toUpperCase() + word.slice(1);
        }).join(' ');
        return p;
      }, {});
  }

  /**
   * @inheritDoc
   */
  descriptions(): DescriptionsType {
    return {};
  }

  /**
   * @inheritDoc
   */
  attribute(name: string): IAttribute {
    return new AutoAttribute(this.labels(), this.attributes, name, this);
  }

  /**
   * Provide attribute value
   */
  value(name: string): any {
    return this.attribute(name).value();
  }

  /**
   * Provide label by attribute name
   */
  label(name: string): any {
    return this.attribute(name).label();
  }

  /**
   * @inheritDoc
   */
  setAttribute(name: string, value: any): this {
    return this.setAttributeBySchema(name, value);
  }

  /**
   * @inheritDoc
   */
  setAttributes(attributes: AttributesType): this {
    Object
      .keys(attributes)
      .forEach((name) => {
        this.setAttributeBySchema(name, attributes[name]);
      });
    return this;
  }

  /**
   * @inheritDoc
   */
  validate(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.clearErrors();
      this.$services.$errors = validate(this.attributes, Object
        .keys(this.schema())
        .reduce((p, c) => {
          if (this.schema()[c].$v) {
            p[c] = this.schema()[c].$v;
          }
          return p;
        }, {}));
      if (this.$services.$errors === undefined) {
        this.$services.$errors = {};
        resolve();
      } else {
        reject();
      }
    });
  }

  /**
   * Represent this model as JSON object annotation
   * @return {*}
   */
  toJSON() {
    return Object.keys(this.schema()).reduce((p, c, i, a) => {
      if (this.schema()[c].$t === 'date_int_ms') {
        p[c] = this.attributes[c].getTime();
      } else {
        p[c] = this.attributes[c];
      }
      return p;
    }, {});
  }

  /**
   * @inheritDoc
   */
  errors(name: string): Array<string> {
    return Array.isArray(this.$services.$errors[name]) ? this.$services.$errors[name] : [];
  }

  /**
   * @inheritDoc
   */
  isValid(): boolean {
    return Object.keys(this.$services.$errors).length === 0;
  }

  /**
   * @inheritDoc
   */
  clearErrors(): this {
    Object
      .keys(this.$services.$errors)
      .forEach((v) => {
        delete this.$services.$errors[v];
      });
    return this;
  }

  /**
   * @todo add ability to detect models
   * @param {Array<string>} names
   * @return {{[p: string]: any}}
   */
  copyRawAttributes(names?: Array<string>): { [name: string]: any } {
    try {
      const ser = JSON.stringify(Object.keys(this.schema()).reduce((p, c) => {
        if (Array.isArray(names)) {
          if (names.indexOf(c) !== -1) {
            p[c] = this.attribute(c).value();
          }
        } else {
          p[c] = this.attribute(c).value();
        }
        return p;
      }, {}));
      return JSON.parse(ser);
    } catch (e) {
      console.log(`model clone: could not to serialize / un serialize attributes!, message ${e.message}`, e);
    }
  }

  /**
   * @inheritDoc
   */
  clone(): this {
    const c: any = this.constructor,
      cloned = new c;
    cloned.setAttributes(this.copyRawAttributes());
    return cloned;
  }

  /**
   * @inheritDoc
   */
  onAttributesChanged(): EventEmitter<string> {
    return this.$services.$onAttributesChanged;
  }

  /**
   * @inheritDoc
   */
  public setError(name: string, message: string): this {
    if (this.schema().hasOwnProperty(name)) {
      if (!Array.isArray(this.$services.$errors[name])) {
        this.$services.$errors[name] = [];
      }
      this.$services.$errors[name].push(message);
    }
    return this;
  }


  /**
   * @inheritDoc
   */
  public setErrors(errors: ErrorsType): this {
    Object.keys(errors).forEach((name: string) => {
      if (Array.isArray(errors[name])) {
        errors[name].forEach((message: string) => {
          this.setError(name, message);
        });
      }
    });

    return this;
  }

  /**
   * @todo implement array path processing like items[3]name
   */
  private setErrorsByPath(path: string, ...message: string[]) {
    const steps = path.split('.');
    let model: IBaseModel = this;
    let lastStepW = '';
    while (steps.length) {
      const lastStep = steps.shift();
      if (lastStep && lastStep.length > 2 && lastStep[0] === '[' && lastStep[lastStep.length - 1] === ']') {
        const index = parseInt(lastStep.substr(1, lastStep.length - 1), 10);
        lastStepW = steps.shift();
        if (isNaN(index) === false && Array.isArray(model) && model.length - 1 >= index) {
          model = model[index];
        }
        continue;
      }

      if (steps.length >= 1) {
        // code for checking items[3]name must be here
        try {
          model = model.attribute(lastStep).value();
        } catch (e) {

        }
      }
      lastStepW = lastStep;
    }
    if (model !== null && typeof model === 'object' && lastStepW !== '') {
      const errs = {};
      errs[lastStepW] = message;
      model.setErrors(errs);
    }
    return this;
  }


  public countErrors(...paths: string[]): number {
    let count = 0;
    paths.forEach((path) => {
      count = this.countErrorsInModel(count, this, path.split('.'));
    });
    return count;
  }

  public countErrorsInModel(count: number, model: IBaseModel, steps: string[]): number {
    let lastStep = '';
    while (steps.length) {
      lastStep = steps.shift();
      if (lastStep === '[*]' && Array.isArray(model)) {
        model.forEach((item) => {
          count = this.countErrorsInModel(count, item, steps);
        });
      }
      if (steps.length >= 1 &&
        !Array.isArray(model) &&
        model !== null &&
        typeof model === 'object') {
        try {
          model = model.attribute(lastStep).value();
        } catch (e) {

        }
      }
      if (steps.length === 0 &&
        !Array.isArray(model) &&
        model !== null &&
        typeof model === 'object' &&
        model.attribute(lastStep).errors().length > 0) {
        count++;
      }
    }
    return count;
  }

  public setErrorsV2(errors: ErrorsTypeV2): this {
    for (const errorsKey in errors) {
      if (errors.hasOwnProperty(errorsKey)) {
        this.setErrorsByPath(errors[errorsKey].attribute, ...errors[errorsKey].messages);
      }
    }
    return this;
  }


  public getDataToSave(): object {
    const data = {};

    Object
      .keys(this.schema())
      .forEach((attrName) => {
        if (this.schema()[attrName].$s) {
          const value = this.attribute(attrName).value();
          data[attrName] = (typeof value.getDataToSave === 'function' ? value.getDataToSave() : value);
        }
      });

    Object.keys(data).forEach((key) => {

      if (typeof data[key] === 'object' && Object.keys(data[key]).length === 0) {
        delete data[key];
      }

      if (Array.isArray(data[key]) && data[key].length === 0) {
        delete data[key];
      }

      if (!data[key]) {
        delete data[key];
      }
    });

    return data;
  }

  setScenario() {

  }

  public enum(name): EnumModel {
    if (!this.enums.hasOwnProperty(name)) {
      throw new Error(`enums have no property ${name}`);
    }

    return this.enums[name];
  }
}
