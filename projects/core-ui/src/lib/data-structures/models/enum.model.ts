export class EnumModel {
    private readonly enumerable;
    private readonly textList;
    private readonly allValuesList;
    private readonly allKeysList;

    private vll: { value: number | string, label: string }[] = [];

    constructor(enumerable, textList) {
        this.enumerable = enumerable;
        this.textList = textList;
        this.allValuesList = Object.keys(this.enumerable).map((key) => this.enum[key]);
        this.allKeysList = Object.keys(this.enumerable);
    }

    get enum() {
        return this.enumerable;
    }

    get wordings() {
        return this.textList;
    }

    getAllValuesList(): Array<number | string> {
        return this.allValuesList;
    }

    getValueLabelList(): Array<{ value: number | string, label: string }> {
        if (this.vll.length === 0) {
            this.allValuesList.forEach(val => {

                this.vll.push({
                    value: val,
                    label: this.wordings[val]
                });
            });
        }

        return this.vll;
    }

    getTextByValue(value: number | string): string {
        if (!this.wordings[value]) {
            return 'Not in Enum!';
        }

        return this.wordings[value];
    }

    getTextByKey(key: string): string {
        if (!this.wordings[this.enum[key]]) {
            return `Not wording for ${key}! EnumModel: ${this}.`;
        }

        return this.wordings[this.enum[key]];
    }

    getValueByKey(key) {
        return this.enum[key];
    }
}
