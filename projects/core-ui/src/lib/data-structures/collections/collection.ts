import {BaseCollection} from 'projects/core-ui/src/lib/data-structures/collections/base.collection';
import {ICollection} from 'projects/core-ui/src/lib/interfaces/data-structures/collections';
import {ICollectionDataProvider} from 'projects/core-ui/src/lib/interfaces/data-provider';
import {IFilterSettings} from 'projects/core-ui/src/lib/interfaces/data-structures/queries';

export abstract class ACollection extends BaseCollection implements ICollection {
  /**
   * @inheritDoc
   */
  dataProvider(): ICollectionDataProvider {
    if (typeof this['$dp'] === 'object') {
      return this['$dp'];
    }
    throw new Error('You should use any of data provider decorators');
  }

  /**
   *
   * @returns {string[]}
   */
  public fullTextSearchKeys(): string[] {
    return [];
  }

  /**
   * @inheritDoc
   */
  public filter(): IFilterSettings[] {
    return [];
  }

  /**
   * Provide list of keys that able to be sorted
   * @returns {string[]}
   */
  public sortKeys(): string[] {
    return [];
  }
}
