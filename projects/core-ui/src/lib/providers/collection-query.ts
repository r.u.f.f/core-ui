import {Injectable} from '@angular/core';
import {ICollectionQuery, SORT} from 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery';
import {IFilter} from 'projects/core-ui/src/lib/data-structures/queries/filter';
import {BaseQuery} from 'projects/core-ui/src/lib/data-structures/queries';

@Injectable()
export class CollectionQuery extends BaseQuery implements ICollectionQuery {
  private s: { [attributeName: string]: number } = {};

  private q = {};

  private filterSettings: IFilter;

  private _sort: { [attributeName: string]: number } = {};

  private _query = {};

  setTerm(term: string, fts: string[] = []): this {
    try {
      this.q = JSON.parse(term);
      return this;
    } catch (e) {

    }

    if (fts.length === 0) {
      fts.push('name');
    }
    this.q = term ? fts.reduce((p, c) => {
      const t = {};
      t[c] = {'$regex': term, $options: 'i'};
      p.$or.push(t);
      return p;
    }, {$or: []}) : {};
    return this;
  }

  // toJSON(): any {
  //     return {
  //         forGrid: true,
  //         jsonQuery: JSON.stringify(this.q),
  //         limit: this.limit(),
  //         skip: this.offset()
  //     };
  // }
  toJSON(): any {

    let settings = [];
    try {
      settings = this.filterSettings.toRequestJSON();
    } catch (e) {

    }
    return {
      filter: settings,
      limit: this.limit(),
      skip: this.offset(),
      sort: this.sort(),
    };
  }

  public setFilter(filter: IFilter): this {
    this.filterSettings = filter;
    return this;
  }

  public filter(): any {
    return this.q;
  }

  public sort(): { [p: string]: number } {
    return this.s;
  }

  /**
   *
   * @param {string} attributeName
   * @param {string} direction
   * @returns {this}
   */
  public setSort(attributeName: string, direction: string): this {
    if ([SORT.ASC, SORT.DESC].indexOf(direction) === -1) {
      return this;
    }
    Object.keys(this.s).forEach((n: string) => {
      delete this.s[n];
    });
    switch (direction) {
      case SORT.ASC:
        this.s[attributeName] = 1;
        break;
      case SORT.DESC:
        this.s[attributeName] = -1;
    }

    return this;
  }

}
