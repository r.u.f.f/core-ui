import {EventEmitter} from '@angular/core';
import {IReloader} from 'projects/core-ui/src/lib/interfaces/data-provider/IReloader';
import {ICollectionQuery} from 'projects/core-ui/src/lib/interfaces/data-structures/queries';


export interface ICollectionDataProvider extends IReloader {
  /**
   * Provide all records count
   * @return {number}
   */
  allCount(): number;

  /**
   * Count items on a page
   * @return {number}
   */
  onPageCount(): number;

  /**
   * Raise events
   * @return {EventEmitter<string>}
   */
  eventEmitter(): EventEmitter<string>;

  /**
   * Provide items all count
   * @return {Promise<this>}
   */
  count(): Promise<this>;

  /**
   * Provide records according query
   * @param {ICollectionQuery} query
   * @return {Promise<this>}
   */
  find(query?: ICollectionQuery): Promise<this>;

  /**
   * Provide records according query
   * Does not clear collection
   * @param {ICollectionQuery} query
   * @return {Promise<this>}
   */
  findChunk(query?: ICollectionQuery): Promise<this>;

  /**
   * Provide only unique records according query
   * @param {string} fieldName
   * @param {ICollectionQuery} query
   * @returns {Promise<this>}
   */
  findUnique(fieldName: string, query?: ICollectionQuery): Promise<this>;

  /**
   * Remove items by query
   * @param {ICollectionQuery} query
   * @returns {Promise<this>}
   */
  remove(query: ICollectionQuery): Promise<this>;

  /**
   * Provide last query
   */
  lastQuery(): ICollectionQuery;

  /**
   * Provide reference for embedded collection query.
   */
  query(): ICollectionQuery;

  /**
   * This flag indicates transport activity
   * @return {boolean}
   */
  isWaitingForTransport(): boolean;
}
