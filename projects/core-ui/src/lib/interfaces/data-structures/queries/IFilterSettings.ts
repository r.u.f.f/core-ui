import {EnumModel} from 'projects/core-ui/src/lib/data-structures/models';
import {IBaseModel} from 'projects/core-ui/src/lib/interfaces/data-structures/models';
import {IAttribute} from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';

export enum FilterType {
    EQ = 'eq',
    NEQ = 'neq',
    IN = 'in',
    IN_ENUM = 'in_enum',
    ENUM = 'enum',
    GT = 'gt',
    LT = 'lt',
    GTE = 'gte',
    LTE = 'lte',
    ILIKE = 'ilike',
    RANGE = 'range',
    DATE = 'date',
}

export interface IResp {
    type: FilterType;
    name: string;
    alias?: string;
    val?: any;
    minVal?: any;
    maxVal?: any;
}

export interface IFilterItem {

    getEnum(): EnumModel;

    getAlias(): string;

    getName(): string;

    getType(): FilterType;

    v(): IAttribute;

    vEnum(index: string | number): IAttribute;

    min(): IAttribute;

    max(): IAttribute;

    toResponse(): IResp;
}

export interface IFilterSettings {
    type: FilterType;
    name: string;
    enum?: EnumModel;
    label?: string;
    alias?: string;
}


class FilterAttribute implements IAttribute {

    private val: any = null;

    constructor(private lbl: string, initVal?: any) {
        if (initVal !== undefined) {
            this.val = initVal;
        }
    }

    description(): string {
        return '';
    }

    errors(): Array<string> {
        return [];
    }

    is(val: any): boolean {
        return val === this.val;
    }

    label(): string {
        return this.lbl;
    }

    setValue(value: any): this {
        this.val = value;
        return this;
    }

    value(): any {
        return this.val;
    }

}

export class FilterItem implements IFilterItem {
    private val: IAttribute | null = null;
    private minVal: IAttribute | null = null;
    private maxVal: IAttribute | null = null;

    private enumVal: { [attribute: string]: IAttribute } | { [attribute: number]: IAttribute } = {};


    constructor(private setting: IFilterSettings) {

    }

    getAlias(): string {
        try {
            return this.setting.alias;
        } catch (e) {
            return '';
        }
    }

    getName(): string {
        return this.setting.name;
    }

    getType(): FilterType {
        return this.setting.type;
    }

    max(): IAttribute {
        if (this.maxVal === null) {
            this.maxVal = new FilterAttribute(
                this.setting.label ? this.setting.label : this.setting.name,
                this.getType() === FilterType.DATE ? 0 : null,
            );
        }
        return this.maxVal;
    }

    min(): IAttribute {
        if (this.minVal === null) {
            this.minVal = new FilterAttribute(
                this.setting.label ? this.setting.label : this.setting.name,
                this.getType() === FilterType.DATE ? 0 : null,
            );
        }
        return this.minVal;
    }

    vEnum(index: string | number): IAttribute {
        if (!this.enumVal.hasOwnProperty(index)) {
            this.enumVal[index] = new FilterAttribute(
                this.setting.enum.getTextByKey(index as string) as string,
                false
            );
        }
        return this.enumVal[index];
    }

    v(): IAttribute {
        if (this.val === null) {
            this.val = new FilterAttribute(
                this.setting.label ? this.setting.label : this.setting.name,
            );
        }
        return this.val;
    }

    public getEnum(): EnumModel {
        return this.setting.enum;
    }

    public toResponse(): IResp {

        const result: IResp = {
            type: this.getType(),
            name: this.setting.name,
            alias: this.setting.alias,
        };

        switch (this.getType()) {
            case FilterType.IN_ENUM:
                result.val = Object
                    .keys(this.enumVal)
                    .map((key: string) => {
                        return {
                            key,
                            enabled: !!this.enumVal[key].value()
                        };
                    })
                    .filter((i) => {
                        return i.enabled === true;
                    })
                    .map((i) => {
                        console.log(i.key);
                        return this.setting.enum.enum[i.key];
                    });
                break;
            case FilterType.DATE:
                result.minVal = this.min().value();
                result.maxVal = this.max().value();
                if (result.minVal !== 0 && (result.minVal === result.maxVal)) {
                    result.maxVal += (3600 * 24) - 1; // adding one day minus last second
                }
                /* if (this.min().value() < this.max().value()) {
                   result.minVal = this.min().value();
                   result.maxVal = this.max().value();
                 } else {
                   result.minVal = this.max().value();
                   result.maxVal = this.min().value();
                 }*/
                break;
            default:
                result.val = this.v().value();
                break;
        }
        return result;
    }
}


export class FilterSettingsFactory {
    static fromSettings(...settings: IFilterSettings[]): IFilterItem[] {
        return settings.map((item: IFilterSettings) => {
            return new FilterItem(item);
        });
    }

    static fromSettingsAndModel(model: IBaseModel, ...settings: IFilterSettings[]): IFilterItem[] {
        return settings.map((item: IFilterSettings) => {
            item.label = model.attribute(item.name).label();
            return new FilterItem(item);
        });
    }
}
