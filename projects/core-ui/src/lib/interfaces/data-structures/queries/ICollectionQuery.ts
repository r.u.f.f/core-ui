import {IFilter} from 'projects/core-ui/src/lib/data-structures/queries';

export const SORT = {
    ASC: 'asc',
    DESC: 'desc'
};


export interface ICollectionQuery {

    toJSON(): any;


    limit(): number;

    offset(): number;

    setLimit(limit: number): this;

    /**
     * Define params for imitate full text search
     * @deprecated
     */
    setTerm(term: string, fts: string[]): this;

    /**
     *
     * @param {number} offset
     * @returns {this}
     */
    setOffset(offset: number): this;

    /**
     * Define filter for query building
     */
    setFilter(filter: IFilter): this;

    /**
     * Provide mongo filter
     */
    filter(): any;

    /**
     * Provide sort settings
     * Example:
     * `
     *      {'created_at':1,'created_by':1}
     * `
     * @returns {{[p: string]: string}}
     */
    sort(): { [attributeName: string]: number };

    /**
     * Define sort rules to query
     * Example:
     * `
     *      collectionQuery.setSort('created_at','asc');
     * `
     * @param {string} attributeName
     * @param {string} direction
     * @returns {this}
     */
    setSort(attributeName: string, direction: string): this;
}
