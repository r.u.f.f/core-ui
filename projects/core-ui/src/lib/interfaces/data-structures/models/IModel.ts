import {IBaseModel} from './IBaseModel';
import {IModelDataProvider} from 'projects/core-ui/src/lib/interfaces/data-provider';

export interface IModel extends IBaseModel {
    /**
     * Provide data provider
     * @return {IModelDataProvider}
     */
    dataProvider(): IModelDataProvider;

    /**
     * Provide primary key attribute name
     * @return {string | Array<string>}
     */
    pk(): string | Array<string>;

    /**
     * Provide request body data
     * @return {object}
     */
    requestFormatter(): object;
}
