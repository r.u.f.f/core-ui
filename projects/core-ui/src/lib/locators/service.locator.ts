import {Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {IDataProviderFactory} from 'projects/core-ui/src/lib/interfaces/data-provider/IDataProviderFactory';
import {CollectionQuery} from 'projects/core-ui/src/lib/providers/collection-query';


export class ServiceLocator {

  static injector: Injector;
  static dataProviderFactory: IDataProviderFactory;

  static http(): HttpClient {
    return ServiceLocator.injector.get(HttpClient);
  }

  static colQuery(): CollectionQuery {
    return ServiceLocator.injector.get(CollectionQuery);
  }

  /**
   * Provide data provider factory
   */
  static setDataProviderFactory(dpf: IDataProviderFactory) {
    ServiceLocator.dataProviderFactory = dpf;
  }

  /**
   * Provide instance of data provider
   */
  static getDataProviderFactory(): IDataProviderFactory {
    return ServiceLocator.dataProviderFactory;
  }
}
