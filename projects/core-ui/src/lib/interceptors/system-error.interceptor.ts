import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class SystemErrorInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req)
      .pipe(
        catchError((response: any) => {
          if (response instanceof HttpErrorResponse) {
            const {error} = response;

            const systemError = error && error.errors ? error.errors.find(item => item.attribute === 'system') : null;

            if (systemError) {
              // systemError.messages.forEach((message => this.toast.danger(message)));
            }
          }

          return of(response);
        })
      );
  }
}
