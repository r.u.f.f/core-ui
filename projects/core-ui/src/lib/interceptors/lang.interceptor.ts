import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Injectable} from '@angular/core';

@Injectable()
export class LangInterceptor implements HttpInterceptor {

  constructor() {
    // private langService: LangService
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const authReq = req.clone({
      // headers: req.headers.set(this.langService.getLangHeader(), this.langService.getLangCode())
    });

    return next.handle(authReq);
  }

}
