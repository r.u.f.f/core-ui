Include list of interceptors bundle what you will need
to app module providers array

Example:
```$xslt
/* "Bundle" of Interceptors */
import {...} from '...core';

/** Http interceptor providers in outside-in order */
export const interceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: LangInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: ContentTypeInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: SystemErrorInterceptor, multi: true},
];

```
