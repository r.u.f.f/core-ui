import { EnumModel } from 'projects/core-ui/src/lib/data-structures/models';
import { IBaseModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models';
import { IAttribute } from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
export declare enum FilterType {
    EQ = "eq",
    NEQ = "neq",
    IN = "in",
    IN_ENUM = "in_enum",
    ENUM = "enum",
    GT = "gt",
    LT = "lt",
    GTE = "gte",
    LTE = "lte",
    ILIKE = "ilike",
    RANGE = "range",
    DATE = "date"
}
export interface IResp {
    type: FilterType;
    name: string;
    alias?: string;
    val?: any;
    minVal?: any;
    maxVal?: any;
}
export interface IFilterItem {
    getEnum(): EnumModel;
    getAlias(): string;
    getName(): string;
    getType(): FilterType;
    v(): IAttribute;
    vEnum(index: string | number): IAttribute;
    min(): IAttribute;
    max(): IAttribute;
    toResponse(): IResp;
}
export interface IFilterSettings {
    type: FilterType;
    name: string;
    enum?: EnumModel;
    label?: string;
    alias?: string;
}
export declare class FilterItem implements IFilterItem {
    private setting;
    private val;
    private minVal;
    private maxVal;
    private enumVal;
    constructor(setting: IFilterSettings);
    getAlias(): string;
    getName(): string;
    getType(): FilterType;
    max(): IAttribute;
    min(): IAttribute;
    vEnum(index: string | number): IAttribute;
    v(): IAttribute;
    getEnum(): EnumModel;
    toResponse(): IResp;
}
export declare class FilterSettingsFactory {
    static fromSettings(...settings: IFilterSettings[]): IFilterItem[];
    static fromSettingsAndModel(model: IBaseModel, ...settings: IFilterSettings[]): IFilterItem[];
}
