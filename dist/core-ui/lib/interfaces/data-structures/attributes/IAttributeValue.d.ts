export interface IAttributeValue {
    /**
     * Provide attribute value
     * @return {any}
     */
    value(): any;
}
