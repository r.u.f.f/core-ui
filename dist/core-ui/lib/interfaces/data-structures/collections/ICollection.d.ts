import { IBaseCollection } from './IBaseCollection';
import { ICollectionDataProvider } from 'projects/core-ui/src/lib/interfaces/data-provider';
import { IFilterSettings } from 'projects/core-ui/src/lib/interfaces/data-structures/queries';
export interface ICollection extends IBaseCollection {
    /**
     * Provide data provider
     * @return {ICollectionDataProvider}
     */
    dataProvider(): ICollectionDataProvider;
    /**
     * Provide list for full text search
     * @returns {string[]}
     */
    fullTextSearchKeys(): string[];
    /**
     * Reference to filter
     */
    filter(): IFilterSettings[];
    /**
     * List of keys that can be sorted
     * @returns {string[]}
     */
    sortKeys(): string[];
}
