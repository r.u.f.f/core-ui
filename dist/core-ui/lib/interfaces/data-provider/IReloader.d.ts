export interface IReloader {
    reload(): Promise<any>;
}
