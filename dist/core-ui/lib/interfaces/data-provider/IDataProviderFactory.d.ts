import { IEndpoint } from 'projects/core-ui/src/lib/interfaces/endpoint';
import { IBaseCollection } from 'projects/core-ui/src/lib/interfaces/data-structures/collections';
import { ICollectionDataProvider } from 'projects/core-ui/src/lib/interfaces/data-provider/ICollectionDataProvider';
import { IModelDataProvider } from 'projects/core-ui/src/lib/interfaces/data-provider/IModelDataProvider';
import { IBaseModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models';
export interface IDataProviderFactory {
    /**
     * makes a new instance of data provider for collections
     * @param {IEndpoint} e
     * @param {IBaseCollection} c
     * @returns {ICollectionDataProvider}
     */
    collection(e: IEndpoint, c: IBaseCollection | any): ICollectionDataProvider;
    /**
     * makes a new instance of model data provider
     * @param {IEndpoint} e
     * @param {IBaseModel} m
     * @returns {IModelDataProvider}
     */
    model(e: IEndpoint, m: IBaseModel | any): IModelDataProvider;
}
