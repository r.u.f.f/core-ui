import { EventEmitter } from '@angular/core';
export interface IModelDataProvider {
    /**
     * Update current Record
     * @return {Promise}
     */
    update(): Promise<any>;
    /**
     * Update field for current Record
     * @return {Promise}
     */
    updateField(field: string): Promise<any>;
    /**
     * Create this record in storage
     * @return {Promise}
     */
    create(): Promise<any>;
    /**
     * Remove record from storage
     * @return {Promise}
     */
    remove(): Promise<any>;
    /**
     * Find Record over storage
     * @param {string | number} pk
     * @return {Promise}
     */
    find(pk?: string | number): Promise<any>;
    /**
     * This method helps for checking does any communication pending or not
     * @return {boolean}
     */
    isWaitingForTransport(): boolean;
    /**
     * Raise events
     * @return {EventEmitter<string>}
     */
    eventEmitter(): EventEmitter<string>;
}
