import { Observable } from 'rxjs';
import { IModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models/IModel';
import { IAttribute } from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
export declare enum ActionNames {
    PLinkAction = "p-link-action",
    PSearchAction = "p-search-action",
    PModalAction = "p-modal-action",
    PModelBulkRemoveAction = "p-model-bulk-remove-action",
    PCollectionBulkRemoveAction = "p-collection-bulk-remove-action",
    PCollectionChooseItemsAction = "p-collection-choose-items-action"
}
/**
 * Simple bulk action interface
 */
export interface IModelBulkActon {
    /**
     * Provide action name
     * @return string
     */
    name(): string;
    /**
     * Provide action label
     * @return {string}
     */
    label(): string;
    /**
     * Provide list of button classes
     * @return {string[]}
     */
    btnClasses(): string[];
    /**
     * Provide list of icon classes
     * @return {string[]}
     */
    iconClasses(): string[];
    /**
     * Is action able to run
     * @param {IModel[]} models
     * @return {boolean}
     */
    canRun<T extends IModel>(models: T[]): boolean;
    /**
     * Execute action over all models
     * @param {IModel[]} models
     * @return {Promise<boolean>}
     */
    run<T extends IModel>(models: T[]): Promise<any>;
    /**
     * Check attribute for existing
     * If attribute exist, input field appears
     * @return {boolean}
     */
    hasAttribute(): boolean;
    /**
     *
     * Get attribute
     * @return {IAttribute | null}
     */
    getAttribute(): IAttribute;
    /**
     *
     * Return Observable of data when action was run
     * @return Observable<any>
     */
    onFired(): Observable<any>;
}
