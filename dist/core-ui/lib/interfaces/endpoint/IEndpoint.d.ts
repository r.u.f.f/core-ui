export interface IEndpoint {
    API_BASE_PATH: string;
    ACTION?: {
        [key: string]: string;
    };
}
