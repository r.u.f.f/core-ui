import { ABaseModel } from 'projects/core-ui/src/lib/data-structures/models/base.model';
import { IModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models/IModel';
import { IModelDataProvider } from 'projects/core-ui/src/lib/interfaces/data-provider/IModelDataProvider';
export declare abstract class AModel extends ABaseModel implements IModel {
    /**
     * @inheritDoc
     */
    dataProvider(): IModelDataProvider;
    /**
     * @inheritDoc
     */
    pk(): string;
    /**
     * @inheritDoc
     */
    requestFormatter(): object;
    getDataToSave(): object;
}
