import { EventEmitter } from '@angular/core';
import { AttributesType, DescriptionsType, ErrorsType, ErrorsTypeV2, IBaseModel, LabelsType, SchemaType } from 'projects/core-ui/src/lib/interfaces/data-structures/models';
import { IAttribute } from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
import { EnumModel } from 'projects/core-ui/src/lib/data-structures/models/enum.model';
export declare abstract class ABaseModel implements IBaseModel {
    private ALLOW_NULL_VALUES;
    /**
     * Internal dictionary for keeping attributes
     * @private
     */
    protected attributes: {};
    protected enums: {};
    protected editMode: boolean;
    protected $services: {
        $onAttributesChanged: EventEmitter<string>;
        $nullValuesMap: {
            string: string;
            number: number;
            boolean: boolean;
            bool: boolean;
            array: any[];
            date_int_ms: Date;
        };
        $errors: {};
        $scenario: {
            active: {};
            list: any[];
        };
    };
    constructor();
    startEdit(): this;
    finishEdit(): this;
    isEditModeEnabled(): boolean;
    /**
     * @inheritDoc
     */
    abstract schema(): SchemaType;
    /**
     * Detect null values for each type
     * @param {*} type
     * @return {*}
     */
    private nullValueByType;
    /**
     * Initi attributes after model construct
     * @return {IBaseModel}
     */
    private initEmptyAttributes;
    /**
     * Defining each attribute by declared schema
     * @param {string} name
     * @param value
     * @return {IBaseModel}
     */
    private setAttributeBySchema;
    private fillAttributesCopies;
    /**
     * @inheritDoc
     */
    labels(): LabelsType;
    /**
     * @inheritDoc
     */
    descriptions(): DescriptionsType;
    /**
     * @inheritDoc
     */
    attribute(name: string): IAttribute;
    /**
     * Provide attribute value
     */
    value(name: string): any;
    /**
     * Provide label by attribute name
     */
    label(name: string): any;
    /**
     * @inheritDoc
     */
    setAttribute(name: string, value: any): this;
    /**
     * @inheritDoc
     */
    setAttributes(attributes: AttributesType): this;
    /**
     * @inheritDoc
     */
    validate(): Promise<any>;
    /**
     * Represent this model as JSON object annotation
     * @return {*}
     */
    toJSON(): {};
    /**
     * @inheritDoc
     */
    errors(name: string): Array<string>;
    /**
     * @inheritDoc
     */
    isValid(): boolean;
    /**
     * @inheritDoc
     */
    clearErrors(): this;
    /**
     * @todo add ability to detect models
     * @param {Array<string>} names
     * @return {{[p: string]: any}}
     */
    copyRawAttributes(names?: Array<string>): {
        [name: string]: any;
    };
    /**
     * @inheritDoc
     */
    clone(): this;
    /**
     * @inheritDoc
     */
    onAttributesChanged(): EventEmitter<string>;
    /**
     * @inheritDoc
     */
    setError(name: string, message: string): this;
    /**
     * @inheritDoc
     */
    setErrors(errors: ErrorsType): this;
    /**
     * @todo implement array path processing like items[3]name
     */
    private setErrorsByPath;
    countErrors(...paths: string[]): number;
    countErrorsInModel(count: number, model: IBaseModel, steps: string[]): number;
    setErrorsV2(errors: ErrorsTypeV2): this;
    getDataToSave(): object;
    setScenario(): void;
    enum(name: any): EnumModel;
}
