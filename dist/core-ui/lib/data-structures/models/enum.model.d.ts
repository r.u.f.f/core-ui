export declare class EnumModel {
    private readonly enumerable;
    private readonly textList;
    private readonly allValuesList;
    private readonly allKeysList;
    private vll;
    constructor(enumerable: any, textList: any);
    get enum(): any;
    get wordings(): any;
    getAllValuesList(): Array<number | string>;
    getValueLabelList(): Array<{
        value: number | string;
        label: string;
    }>;
    getTextByValue(value: number | string): string;
    getTextByKey(key: string): string;
    getValueByKey(key: any): any;
}
