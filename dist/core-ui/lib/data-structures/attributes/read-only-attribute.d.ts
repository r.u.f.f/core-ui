import { IAttribute } from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
export declare class ReadOnlyAttribute implements IAttribute {
    protected lbl: string;
    protected val: any;
    private errs;
    constructor(lbl: string, val: any);
    label(): string;
    setValue(value: any): this;
    value(): any;
    errors(): Array<string>;
    is(val: any): boolean;
    description(): string;
}
