import { Attribute } from 'projects/core-ui/src/lib/data-structures/attributes/attribute';
import { IBaseModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models/IBaseModel';
export declare class AutoAttribute extends Attribute {
    private labels;
    private attributes;
    private name;
    private model;
    constructor(labels: {
        [s: string]: any;
    }, attributes: {
        [s: string]: any;
    }, name: any, model: IBaseModel);
    setValue(value: any): this;
    value(): any;
    valueByKey(key: string): any;
    label(): string;
    errors(): Array<string>;
    is(val: any): boolean;
    getName(): string;
    description(): string;
}
