import { IAttribute } from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
export declare abstract class Attribute implements IAttribute {
    abstract label(): string;
    abstract setValue(value: any): this;
    abstract value(): any;
    abstract errors(): Array<string>;
    abstract is(val: any): boolean;
    abstract description(): string;
    abstract getName(): string;
}
