import { IAttribute } from 'projects/core-ui/src/lib/interfaces/data-structures/attributes';
export declare class SimpleAttribute implements IAttribute {
    private _val;
    private _label;
    private _description;
    constructor(_val: any, _label: string, _description?: string);
    value(): any;
    setValue(value: any): this;
    label(): string;
    description(): string;
    errors(): Array<string>;
    is(val: any): boolean;
}
