import { IBaseCollection } from 'projects/core-ui/src/lib/interfaces/data-structures/collections';
import { IBaseModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models';
export declare abstract class BaseCollection implements IBaseCollection {
    private _data;
    /**
     * @inheritDoc
     */
    count(): number;
    /**
     * @inheritDoc
     */
    data(): Array<IBaseModel>;
    /**
     * @inheritDoc
     */
    abstract model(): IBaseModel;
    /**
     * @inheritDoc
     */
    add(model: IBaseModel): this;
    /**
     * @inheritDoc
     */
    clear(): this;
}
