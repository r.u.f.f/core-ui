import { BaseCollection } from 'projects/core-ui/src/lib/data-structures/collections/base.collection';
import { ICollection } from 'projects/core-ui/src/lib/interfaces/data-structures/collections';
import { ICollectionDataProvider } from 'projects/core-ui/src/lib/interfaces/data-provider';
import { IFilterSettings } from 'projects/core-ui/src/lib/interfaces/data-structures/queries';
export declare abstract class ACollection extends BaseCollection implements ICollection {
    /**
     * @inheritDoc
     */
    dataProvider(): ICollectionDataProvider;
    /**
     *
     * @returns {string[]}
     */
    fullTextSearchKeys(): string[];
    /**
     * @inheritDoc
     */
    filter(): IFilterSettings[];
    /**
     * Provide list of keys that able to be sorted
     * @returns {string[]}
     */
    sortKeys(): string[];
}
