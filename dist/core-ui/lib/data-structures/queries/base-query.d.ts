export declare class BaseQuery {
    protected _limit: number;
    protected _offset: number;
    limit(): number;
    offset(): number;
    toJSON(): any;
    setLimit(limit: number): this;
    setOffset(offset: number): this;
}
