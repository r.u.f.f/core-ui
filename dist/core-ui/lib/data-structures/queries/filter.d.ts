import { IFilterItem, IResp } from 'projects/core-ui/src/lib/interfaces/data-structures/queries';
export interface IFilter {
    toRequestJSON(): IResp[];
    getSettings(): IFilterItem[];
}
export declare class Filter implements IFilter {
    private filterItems;
    constructor(filterItems: IFilterItem[]);
    toRequestJSON(): IResp[];
    getSettings(): IFilterItem[];
}
