import { ICollectionQuery } from 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery';
import { IFilter } from 'projects/core-ui/src/lib/data-structures/queries/filter';
import { BaseQuery } from 'projects/core-ui/src/lib/data-structures/queries';
import * as i0 from "@angular/core";
export declare class CollectionQuery extends BaseQuery implements ICollectionQuery {
    private s;
    private q;
    private filterSettings;
    private _sort;
    private _query;
    setTerm(term: string, fts?: string[]): this;
    toJSON(): any;
    setFilter(filter: IFilter): this;
    filter(): any;
    sort(): {
        [p: string]: number;
    };
    /**
     *
     * @param {string} attributeName
     * @param {string} direction
     * @returns {this}
     */
    setSort(attributeName: string, direction: string): this;
    static ɵfac: i0.ɵɵFactoryDef<CollectionQuery, never>;
    static ɵprov: i0.ɵɵInjectableDef<CollectionQuery>;
}
