import { EventEmitter } from '@angular/core';
import { IModel } from 'projects/core-ui/src/lib/interfaces/data-structures/models';
import * as i0 from "@angular/core";
export interface IModelListActionEvent {
    name: string;
    result: boolean;
}
export declare class ModelList {
    private _list;
    private _em;
    /**
     * @return EventEmitter<IModelListActionEvent>
     */
    em(): EventEmitter<IModelListActionEvent>;
    /**
     * Provide reference for list
     * @return {IModel[]}
     */
    list(): IModel[];
    static ɵfac: i0.ɵɵFactoryDef<ModelList, never>;
    static ɵprov: i0.ɵɵInjectableDef<ModelList>;
}
