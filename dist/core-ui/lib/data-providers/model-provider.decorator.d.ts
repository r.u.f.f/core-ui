import { IEndpoint } from 'projects/core-ui/src/lib/interfaces/endpoint/IEndpoint';
export declare function ModelProviderDecorator(options: {
    endpoint: IEndpoint;
    apiUrl?: string;
}): <T extends new (...args: any[]) => {}>(constructor: T) => {
    new (...args: any[]): {
        $dp: import("core-ui").IModelDataProvider;
    };
} & T;
