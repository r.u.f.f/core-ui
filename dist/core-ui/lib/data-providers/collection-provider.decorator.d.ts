import { IEndpoint } from 'projects/core-ui/src/lib/interfaces/endpoint/IEndpoint';
export declare function CollectionProviderDecorator(options: {
    endpoint: IEndpoint;
    apiUrl?: string;
}): <T extends new (...args: any[]) => {}>(constructor: T) => {
    new (...args: any[]): {
        $dp: import("core-ui").ICollectionDataProvider;
    };
} & T;
