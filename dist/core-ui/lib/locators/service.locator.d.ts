import { Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDataProviderFactory } from 'projects/core-ui/src/lib/interfaces/data-provider/IDataProviderFactory';
import { CollectionQuery } from 'projects/core-ui/src/lib/providers/collection-query';
export declare class ServiceLocator {
    static injector: Injector;
    static dataProviderFactory: IDataProviderFactory;
    static http(): HttpClient;
    static colQuery(): CollectionQuery;
    /**
     * Provide data provider factory
     */
    static setDataProviderFactory(dpf: IDataProviderFactory): void;
    /**
     * Provide instance of data provider
     */
    static getDataProviderFactory(): IDataProviderFactory;
}
