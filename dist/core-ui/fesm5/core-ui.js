import { __extends, __spread } from 'tslib';
import { ServiceLocator as ServiceLocator$1 } from 'projects/core-ui/src/lib/locators/service.locator';
import { Attribute as Attribute$1 } from 'projects/core-ui/src/lib/data-structures/attributes/attribute';
import { BaseCollection as BaseCollection$1 } from 'projects/core-ui/src/lib/data-structures/collections/base.collection';
import { EventEmitter, ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵgetInheritedFactory } from '@angular/core';
import { validate } from 'validate.js';
import { AutoAttribute as AutoAttribute$1 } from 'projects/core-ui/src/lib/data-structures/attributes';
import { ABaseModel as ABaseModel$1 } from 'projects/core-ui/src/lib/data-structures/models/base.model';
import { FilterType as FilterType$1 } from 'projects/core-ui/src/lib/interfaces/data-structures/queries';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CollectionQuery as CollectionQuery$1 } from 'projects/core-ui/src/lib/providers/collection-query';
import { SORT as SORT$1 } from 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery';
import { BaseQuery as BaseQuery$1 } from 'projects/core-ui/src/lib/data-structures/queries';

function ModelProviderDecorator(options) {
    return function (constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.$dp = ServiceLocator$1.getDataProviderFactory().model(options.endpoint, _this);
                return _this;
            }
            return class_1;
        }(constructor));
    };
}

function CollectionProviderDecorator(options) {
    return function (constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.$dp = ServiceLocator$1.getDataProviderFactory().collection(options.endpoint, _this);
                return _this;
            }
            return class_1;
        }(constructor));
    };
}

var Attribute = /** @class */ (function () {
    function Attribute() {
    }
    return Attribute;
}());

var AutoAttribute = /** @class */ (function (_super) {
    __extends(AutoAttribute, _super);
    function AutoAttribute(labels, attributes, name, model) {
        var _this = _super.call(this) || this;
        _this.labels = labels;
        _this.attributes = attributes;
        _this.name = name;
        _this.model = model;
        if (!_this.model.schema()[_this.name]) {
            throw new Error("Incorrect implementation. Model " + _this.model.constructor.name + " have no attribute [" + _this.name + "]!");
        }
        return _this;
    }
    AutoAttribute.prototype.setValue = function (value) {
        this.attributes[this.name] = value;
        this.model.onAttributesChanged().emit(this.name);
        return this;
    };
    AutoAttribute.prototype.value = function () {
        return this.attributes[this.name];
    };
    AutoAttribute.prototype.valueByKey = function (key) {
        return this.attributes[this.name][key];
    };
    AutoAttribute.prototype.label = function () {
        return this.labels[this.name];
    };
    AutoAttribute.prototype.errors = function () {
        return this.model.errors(this.name);
    };
    AutoAttribute.prototype.is = function (val) {
        return this.attributes[this.name] === val;
    };
    AutoAttribute.prototype.getName = function () {
        return this.name;
    };
    AutoAttribute.prototype.description = function () {
        return this.model.schema()[this.name].$d ? this.model.schema()[this.name].$d : '';
    };
    return AutoAttribute;
}(Attribute$1));

var ReadOnlyAttribute = /** @class */ (function () {
    function ReadOnlyAttribute(lbl, val) {
        this.lbl = lbl;
        this.val = val;
        this.errs = [];
    }
    ReadOnlyAttribute.prototype.label = function () {
        return this.lbl;
    };
    ReadOnlyAttribute.prototype.setValue = function (value) {
        return this;
    };
    ReadOnlyAttribute.prototype.value = function () {
        return this.val;
    };
    ReadOnlyAttribute.prototype.errors = function () {
        return this.errs;
    };
    ReadOnlyAttribute.prototype.is = function (val) {
        return this.value() === val;
    };
    ReadOnlyAttribute.prototype.description = function () {
        return '';
    };
    return ReadOnlyAttribute;
}());

var SimpleAttribute = /** @class */ (function () {
    function SimpleAttribute(_val, _label, _description) {
        if (_description === void 0) { _description = ''; }
        this._val = _val;
        this._label = _label;
        this._description = _description;
    }
    SimpleAttribute.prototype.value = function () {
        return this._val;
    };
    SimpleAttribute.prototype.setValue = function (value) {
        this._val = value;
        return this;
    };
    SimpleAttribute.prototype.label = function () {
        return this._label;
    };
    SimpleAttribute.prototype.description = function () {
        return this._description;
    };
    SimpleAttribute.prototype.errors = function () {
        return [];
    };
    SimpleAttribute.prototype.is = function (val) {
        return this._val === val;
    };
    return SimpleAttribute;
}());

var ACollection = /** @class */ (function (_super) {
    __extends(ACollection, _super);
    function ACollection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @inheritDoc
     */
    ACollection.prototype.dataProvider = function () {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    };
    /**
     *
     * @returns {string[]}
     */
    ACollection.prototype.fullTextSearchKeys = function () {
        return [];
    };
    /**
     * @inheritDoc
     */
    ACollection.prototype.filter = function () {
        return [];
    };
    /**
     * Provide list of keys that able to be sorted
     * @returns {string[]}
     */
    ACollection.prototype.sortKeys = function () {
        return [];
    };
    return ACollection;
}(BaseCollection$1));

var BaseCollection = /** @class */ (function () {
    function BaseCollection() {
        this._data = [];
    }
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.count = function () {
        return this._data.length;
    };
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.data = function () {
        return this._data;
    };
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.add = function (model) {
        this._data.push(model);
        return this;
    };
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.clear = function () {
        this._data.splice(0, this.count());
        return this;
    };
    return BaseCollection;
}());

var ABaseModel = /** @class */ (function () {
    function ABaseModel() {
        this.ALLOW_NULL_VALUES = true;
        /**
         * Internal dictionary for keeping attributes
         * @private
         */
        this.attributes = {};
        this.enums = {};
        this.editMode = false;
        this.$services = {
            $onAttributesChanged: new EventEmitter(),
            $nullValuesMap: {
                string: '',
                number: 0,
                boolean: false,
                bool: false,
                array: [],
                date_int_ms: new Date(),
            },
            $errors: {},
            $scenario: {
                active: {},
                list: [],
            }
        };
        this.initEmptyAttributes();
    }
    ABaseModel.prototype.startEdit = function () {
        this.editMode = true;
        return this;
    };
    ABaseModel.prototype.finishEdit = function () {
        this.editMode = false;
        return this;
    };
    ABaseModel.prototype.isEditModeEnabled = function () {
        return this.editMode;
    };
    /**
     * Detect null values for each type
     * @param {*} type
     * @return {*}
     */
    ABaseModel.prototype.nullValueByType = function (type) {
        if (Array.isArray(type)) {
            return this.$services.$nullValuesMap.array;
        }
        if (typeof type === 'function') {
            var c = type;
            return new c();
        }
        if (this.$services.$nullValuesMap.hasOwnProperty(type)) {
            return this.$services.$nullValuesMap[type];
        }
        return undefined;
    };
    /**
     * Initi attributes after model construct
     * @return {IBaseModel}
     */
    ABaseModel.prototype.initEmptyAttributes = function () {
        var _this = this;
        Object
            .keys(this.schema())
            .forEach(function (n) { return _this.setAttributeBySchema(n, _this.nullValueByType(_this.schema()[n].$t)); });
        return this;
    };
    /**
     * Defining each attribute by declared schema
     * @param {string} name
     * @param value
     * @return {IBaseModel}
     */
    ABaseModel.prototype.setAttributeBySchema = function (name, value) {
        var _this = this;
        if (this.schema().hasOwnProperty(name)) {
            var cast_1 = function (name, value, type) {
                /**@override IBaseModel this */
                var t = type ? type : _this.schema()[name].$t;
                if (typeof t === 'function') { // we think that it is another model
                    var a = (new t());
                    if (value != null) {
                        a.setAttributes(value);
                    }
                    return a;
                }
                switch (t) {
                    case 'string':
                        return String(value);
                    case 'date_int_ms':
                        return new Date(value);
                    case 'number':
                        return Number(value);
                    case 'bool':
                    case 'boolean':
                        if (value === '0') {
                            value = false;
                        }
                        if (value === '1') {
                            value = true;
                        }
                        return Boolean(value);
                }
            };
            if (Array.isArray(this.schema()[name].$t)) {
                this.attributes[name] = [];
                if (this.schema()[name].$t.length !== 1) {
                    throw new Error("Allowed only one type for array definitions for attribute \"" + name + "\"");
                }
                if (!Array.isArray(value)) {
                    if (this.ALLOW_NULL_VALUES === true && value === null) {
                        value = [];
                    }
                    else {
                        throw new Error("Value must be array according to scheme definition for attribute \"" + name + "\"");
                    }
                }
                value.forEach(function (val) {
                    _this.attributes[name].push(cast_1(name, val, _this.schema()[name].$t[0]));
                });
            }
            else {
                this.attributes[name] = cast_1(name, value);
            }
            this.fillAttributesCopies();
        }
        return this;
    };
    ABaseModel.prototype.fillAttributesCopies = function () {
        var _this = this;
        Object
            .keys(this.schema())
            .forEach(function (attrName) {
            if (_this.schema()[attrName].$c &&
                _this.attributes.hasOwnProperty(_this.schema()[attrName].$c)) {
                _this.attributes[attrName] = _this.attributes[_this.schema()[attrName].$c];
            }
        });
        return this;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.labels = function () {
        var _this = this;
        return Object
            .keys(this.schema())
            .reduce(function (p, c) {
            if (_this.schema()[c].$l) {
                p[c] = _this.schema()[c].$l;
                return p;
            }
            p[c] = c
                .replace(/([A-Z])/g, ' $1')
                .replace(/^./, function (str) {
                return str.toUpperCase();
            });
            p[c] = c.split('_').map(function (word) {
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join(' ');
            return p;
        }, {});
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.descriptions = function () {
        return {};
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.attribute = function (name) {
        return new AutoAttribute$1(this.labels(), this.attributes, name, this);
    };
    /**
     * Provide attribute value
     */
    ABaseModel.prototype.value = function (name) {
        return this.attribute(name).value();
    };
    /**
     * Provide label by attribute name
     */
    ABaseModel.prototype.label = function (name) {
        return this.attribute(name).label();
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setAttribute = function (name, value) {
        return this.setAttributeBySchema(name, value);
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setAttributes = function (attributes) {
        var _this = this;
        Object
            .keys(attributes)
            .forEach(function (name) {
            _this.setAttributeBySchema(name, attributes[name]);
        });
        return this;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.validate = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.clearErrors();
            _this.$services.$errors = validate(_this.attributes, Object
                .keys(_this.schema())
                .reduce(function (p, c) {
                if (_this.schema()[c].$v) {
                    p[c] = _this.schema()[c].$v;
                }
                return p;
            }, {}));
            if (_this.$services.$errors === undefined) {
                _this.$services.$errors = {};
                resolve();
            }
            else {
                reject();
            }
        });
    };
    /**
     * Represent this model as JSON object annotation
     * @return {*}
     */
    ABaseModel.prototype.toJSON = function () {
        var _this = this;
        return Object.keys(this.schema()).reduce(function (p, c, i, a) {
            if (_this.schema()[c].$t === 'date_int_ms') {
                p[c] = _this.attributes[c].getTime();
            }
            else {
                p[c] = _this.attributes[c];
            }
            return p;
        }, {});
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.errors = function (name) {
        return Array.isArray(this.$services.$errors[name]) ? this.$services.$errors[name] : [];
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.isValid = function () {
        return Object.keys(this.$services.$errors).length === 0;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.clearErrors = function () {
        var _this = this;
        Object
            .keys(this.$services.$errors)
            .forEach(function (v) {
            delete _this.$services.$errors[v];
        });
        return this;
    };
    /**
     * @todo add ability to detect models
     * @param {Array<string>} names
     * @return {{[p: string]: any}}
     */
    ABaseModel.prototype.copyRawAttributes = function (names) {
        var _this = this;
        try {
            var ser = JSON.stringify(Object.keys(this.schema()).reduce(function (p, c) {
                if (Array.isArray(names)) {
                    if (names.indexOf(c) !== -1) {
                        p[c] = _this.attribute(c).value();
                    }
                }
                else {
                    p[c] = _this.attribute(c).value();
                }
                return p;
            }, {}));
            return JSON.parse(ser);
        }
        catch (e) {
            console.log("model clone: could not to serialize / un serialize attributes!, message " + e.message, e);
        }
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.clone = function () {
        var c = this.constructor, cloned = new c;
        cloned.setAttributes(this.copyRawAttributes());
        return cloned;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.onAttributesChanged = function () {
        return this.$services.$onAttributesChanged;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setError = function (name, message) {
        if (this.schema().hasOwnProperty(name)) {
            if (!Array.isArray(this.$services.$errors[name])) {
                this.$services.$errors[name] = [];
            }
            this.$services.$errors[name].push(message);
        }
        return this;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setErrors = function (errors) {
        var _this = this;
        Object.keys(errors).forEach(function (name) {
            if (Array.isArray(errors[name])) {
                errors[name].forEach(function (message) {
                    _this.setError(name, message);
                });
            }
        });
        return this;
    };
    /**
     * @todo implement array path processing like items[3]name
     */
    ABaseModel.prototype.setErrorsByPath = function (path) {
        var message = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            message[_i - 1] = arguments[_i];
        }
        var steps = path.split('.');
        var model = this;
        var lastStepW = '';
        while (steps.length) {
            var lastStep = steps.shift();
            if (lastStep && lastStep.length > 2 && lastStep[0] === '[' && lastStep[lastStep.length - 1] === ']') {
                var index = parseInt(lastStep.substr(1, lastStep.length - 1), 10);
                lastStepW = steps.shift();
                if (isNaN(index) === false && Array.isArray(model) && model.length - 1 >= index) {
                    model = model[index];
                }
                continue;
            }
            if (steps.length >= 1) {
                // code for checking items[3]name must be here
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            lastStepW = lastStep;
        }
        if (model !== null && typeof model === 'object' && lastStepW !== '') {
            var errs = {};
            errs[lastStepW] = message;
            model.setErrors(errs);
        }
        return this;
    };
    ABaseModel.prototype.countErrors = function () {
        var _this = this;
        var paths = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            paths[_i] = arguments[_i];
        }
        var count = 0;
        paths.forEach(function (path) {
            count = _this.countErrorsInModel(count, _this, path.split('.'));
        });
        return count;
    };
    ABaseModel.prototype.countErrorsInModel = function (count, model, steps) {
        var _this = this;
        var lastStep = '';
        while (steps.length) {
            lastStep = steps.shift();
            if (lastStep === '[*]' && Array.isArray(model)) {
                model.forEach(function (item) {
                    count = _this.countErrorsInModel(count, item, steps);
                });
            }
            if (steps.length >= 1 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object') {
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            if (steps.length === 0 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object' &&
                model.attribute(lastStep).errors().length > 0) {
                count++;
            }
        }
        return count;
    };
    ABaseModel.prototype.setErrorsV2 = function (errors) {
        for (var errorsKey in errors) {
            if (errors.hasOwnProperty(errorsKey)) {
                this.setErrorsByPath.apply(this, __spread([errors[errorsKey].attribute], errors[errorsKey].messages));
            }
        }
        return this;
    };
    ABaseModel.prototype.getDataToSave = function () {
        var _this = this;
        var data = {};
        Object
            .keys(this.schema())
            .forEach(function (attrName) {
            if (_this.schema()[attrName].$s) {
                var value = _this.attribute(attrName).value();
                data[attrName] = (typeof value.getDataToSave === 'function' ? value.getDataToSave() : value);
            }
        });
        Object.keys(data).forEach(function (key) {
            if (typeof data[key] === 'object' && Object.keys(data[key]).length === 0) {
                delete data[key];
            }
            if (Array.isArray(data[key]) && data[key].length === 0) {
                delete data[key];
            }
            if (!data[key]) {
                delete data[key];
            }
        });
        return data;
    };
    ABaseModel.prototype.setScenario = function () {
    };
    ABaseModel.prototype.enum = function (name) {
        if (!this.enums.hasOwnProperty(name)) {
            throw new Error("enums have no property " + name);
        }
        return this.enums[name];
    };
    return ABaseModel;
}());

var EnumModel = /** @class */ (function () {
    function EnumModel(enumerable, textList) {
        var _this = this;
        this.vll = [];
        this.enumerable = enumerable;
        this.textList = textList;
        this.allValuesList = Object.keys(this.enumerable).map(function (key) { return _this.enum[key]; });
        this.allKeysList = Object.keys(this.enumerable);
    }
    Object.defineProperty(EnumModel.prototype, "enum", {
        get: function () {
            return this.enumerable;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EnumModel.prototype, "wordings", {
        get: function () {
            return this.textList;
        },
        enumerable: true,
        configurable: true
    });
    EnumModel.prototype.getAllValuesList = function () {
        return this.allValuesList;
    };
    EnumModel.prototype.getValueLabelList = function () {
        var _this = this;
        if (this.vll.length === 0) {
            this.allValuesList.forEach(function (val) {
                _this.vll.push({
                    value: val,
                    label: _this.wordings[val]
                });
            });
        }
        return this.vll;
    };
    EnumModel.prototype.getTextByValue = function (value) {
        if (!this.wordings[value]) {
            return 'Not in Enum!';
        }
        return this.wordings[value];
    };
    EnumModel.prototype.getTextByKey = function (key) {
        if (!this.wordings[this.enum[key]]) {
            return "Not wording for " + key + "! EnumModel: " + this + ".";
        }
        return this.wordings[this.enum[key]];
    };
    EnumModel.prototype.getValueByKey = function (key) {
        return this.enum[key];
    };
    return EnumModel;
}());

var AModel = /** @class */ (function (_super) {
    __extends(AModel, _super);
    function AModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @inheritDoc
     */
    AModel.prototype.dataProvider = function () {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    };
    /**
     * @inheritDoc
     */
    AModel.prototype.pk = function () {
        if (this.schema()['id'] === undefined) {
            throw new Error("Could not to find default primary key in model.\nYou must redeclare pk() in model or define 'id' field in schema.");
        }
        return 'id';
    };
    /**
     * @inheritDoc
     */
    AModel.prototype.requestFormatter = function () {
        return {};
    };
    AModel.prototype.getDataToSave = function () {
        return _super.prototype.getDataToSave.call(this);
    };
    return AModel;
}(ABaseModel$1));

var BaseQuery = /** @class */ (function () {
    function BaseQuery() {
        this._limit = 20;
        this._offset = 0;
    }
    BaseQuery.prototype.limit = function () {
        return this._limit;
    };
    BaseQuery.prototype.offset = function () {
        return this._offset;
    };
    BaseQuery.prototype.toJSON = function () {
        return {
            forGrid: true,
            limit: this.limit(),
            skip: this.offset()
        };
    };
    BaseQuery.prototype.setLimit = function (limit) {
        this._limit = limit;
        return this;
    };
    BaseQuery.prototype.setOffset = function (offset) {
        this._offset = offset;
        return this;
    };
    return BaseQuery;
}());

var Filter = /** @class */ (function () {
    function Filter(filterItems) {
        this.filterItems = filterItems;
    }
    Filter.prototype.toRequestJSON = function () {
        return this
            .getSettings()
            .map(function (i) { return i.toResponse(); })
            .filter(function (i) {
            return (i.val !== null && i.val !== undefined) ||
                (i.minVal !== null && i.minVal !== undefined) ||
                (i.maxVal !== null && i.maxVal !== undefined);
        })
            .filter(function (i) {
            return !(Array.isArray(i.val) && i.val.length === 0);
        })
            .filter(function (i) {
            return !(i.type === FilterType$1.ILIKE && !i.val);
        });
    };
    Filter.prototype.getSettings = function () {
        return this.filterItems;
    };
    return Filter;
}());

var ContentTypeInterceptor = /** @class */ (function () {
    function ContentTypeInterceptor() {
    }
    ContentTypeInterceptor.prototype.intercept = function (req, next) {
        if (req.body instanceof FormData) {
            return next.handle(req);
        }
        var authReq = req.clone({
            headers: req.headers
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
        });
        return next.handle(authReq);
    };
    ContentTypeInterceptor.ɵfac = function ContentTypeInterceptor_Factory(t) { return new (t || ContentTypeInterceptor)(); };
    ContentTypeInterceptor.ɵprov = ɵɵdefineInjectable({ token: ContentTypeInterceptor, factory: ContentTypeInterceptor.ɵfac });
    return ContentTypeInterceptor;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(ContentTypeInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();

var LangInterceptor = /** @class */ (function () {
    function LangInterceptor() {
        // private langService: LangService
    }
    LangInterceptor.prototype.intercept = function (req, next) {
        var authReq = req.clone({
        // headers: req.headers.set(this.langService.getLangHeader(), this.langService.getLangCode())
        });
        return next.handle(authReq);
    };
    LangInterceptor.ɵfac = function LangInterceptor_Factory(t) { return new (t || LangInterceptor)(); };
    LangInterceptor.ɵprov = ɵɵdefineInjectable({ token: LangInterceptor, factory: LangInterceptor.ɵfac });
    return LangInterceptor;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(LangInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();

var SystemErrorInterceptor = /** @class */ (function () {
    function SystemErrorInterceptor() {
    }
    SystemErrorInterceptor.prototype.intercept = function (req, next) {
        return next.handle(req)
            .pipe(catchError(function (response) {
            if (response instanceof HttpErrorResponse) {
                var error = response.error;
                var systemError = error && error.errors ? error.errors.find(function (item) { return item.attribute === 'system'; }) : null;
                if (systemError) {
                    // systemError.messages.forEach((message => this.toast.danger(message)));
                }
            }
            return of(response);
        }));
    };
    SystemErrorInterceptor.ɵfac = function SystemErrorInterceptor_Factory(t) { return new (t || SystemErrorInterceptor)(); };
    SystemErrorInterceptor.ɵprov = ɵɵdefineInjectable({ token: SystemErrorInterceptor, factory: SystemErrorInterceptor.ɵfac });
    return SystemErrorInterceptor;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(SystemErrorInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();

var ActionNames;
(function (ActionNames) {
    ActionNames["PLinkAction"] = "p-link-action";
    ActionNames["PSearchAction"] = "p-search-action";
    ActionNames["PModalAction"] = "p-modal-action";
    ActionNames["PModelBulkRemoveAction"] = "p-model-bulk-remove-action";
    ActionNames["PCollectionBulkRemoveAction"] = "p-collection-bulk-remove-action";
    ActionNames["PCollectionChooseItemsAction"] = "p-collection-choose-items-action";
})(ActionNames || (ActionNames = {}));

var SORT = {
    ASC: 'asc',
    DESC: 'desc'
};

var FilterType;
(function (FilterType) {
    FilterType["EQ"] = "eq";
    FilterType["NEQ"] = "neq";
    FilterType["IN"] = "in";
    FilterType["IN_ENUM"] = "in_enum";
    FilterType["ENUM"] = "enum";
    FilterType["GT"] = "gt";
    FilterType["LT"] = "lt";
    FilterType["GTE"] = "gte";
    FilterType["LTE"] = "lte";
    FilterType["ILIKE"] = "ilike";
    FilterType["RANGE"] = "range";
    FilterType["DATE"] = "date";
})(FilterType || (FilterType = {}));
var FilterAttribute = /** @class */ (function () {
    function FilterAttribute(lbl, initVal) {
        this.lbl = lbl;
        this.val = null;
        if (initVal !== undefined) {
            this.val = initVal;
        }
    }
    FilterAttribute.prototype.description = function () {
        return '';
    };
    FilterAttribute.prototype.errors = function () {
        return [];
    };
    FilterAttribute.prototype.is = function (val) {
        return val === this.val;
    };
    FilterAttribute.prototype.label = function () {
        return this.lbl;
    };
    FilterAttribute.prototype.setValue = function (value) {
        this.val = value;
        return this;
    };
    FilterAttribute.prototype.value = function () {
        return this.val;
    };
    return FilterAttribute;
}());
var FilterItem = /** @class */ (function () {
    function FilterItem(setting) {
        this.setting = setting;
        this.val = null;
        this.minVal = null;
        this.maxVal = null;
        this.enumVal = {};
    }
    FilterItem.prototype.getAlias = function () {
        try {
            return this.setting.alias;
        }
        catch (e) {
            return '';
        }
    };
    FilterItem.prototype.getName = function () {
        return this.setting.name;
    };
    FilterItem.prototype.getType = function () {
        return this.setting.type;
    };
    FilterItem.prototype.max = function () {
        if (this.maxVal === null) {
            this.maxVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === FilterType.DATE ? 0 : null);
        }
        return this.maxVal;
    };
    FilterItem.prototype.min = function () {
        if (this.minVal === null) {
            this.minVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === FilterType.DATE ? 0 : null);
        }
        return this.minVal;
    };
    FilterItem.prototype.vEnum = function (index) {
        if (!this.enumVal.hasOwnProperty(index)) {
            this.enumVal[index] = new FilterAttribute(this.setting.enum.getTextByKey(index), false);
        }
        return this.enumVal[index];
    };
    FilterItem.prototype.v = function () {
        if (this.val === null) {
            this.val = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name);
        }
        return this.val;
    };
    FilterItem.prototype.getEnum = function () {
        return this.setting.enum;
    };
    FilterItem.prototype.toResponse = function () {
        var _this = this;
        var result = {
            type: this.getType(),
            name: this.setting.name,
            alias: this.setting.alias,
        };
        switch (this.getType()) {
            case FilterType.IN_ENUM:
                result.val = Object
                    .keys(this.enumVal)
                    .map(function (key) {
                    return {
                        key: key,
                        enabled: !!_this.enumVal[key].value()
                    };
                })
                    .filter(function (i) {
                    return i.enabled === true;
                })
                    .map(function (i) {
                    console.log(i.key);
                    return _this.setting.enum.enum[i.key];
                });
                break;
            case FilterType.DATE:
                result.minVal = this.min().value();
                result.maxVal = this.max().value();
                if (result.minVal !== 0 && (result.minVal === result.maxVal)) {
                    result.maxVal += (3600 * 24) - 1; // adding one day minus last second
                }
                /* if (this.min().value() < this.max().value()) {
                   result.minVal = this.min().value();
                   result.maxVal = this.max().value();
                 } else {
                   result.minVal = this.max().value();
                   result.maxVal = this.min().value();
                 }*/
                break;
            default:
                result.val = this.v().value();
                break;
        }
        return result;
    };
    return FilterItem;
}());
var FilterSettingsFactory = /** @class */ (function () {
    function FilterSettingsFactory() {
    }
    FilterSettingsFactory.fromSettings = function () {
        var settings = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            settings[_i] = arguments[_i];
        }
        return settings.map(function (item) {
            return new FilterItem(item);
        });
    };
    FilterSettingsFactory.fromSettingsAndModel = function (model) {
        var settings = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            settings[_i - 1] = arguments[_i];
        }
        return settings.map(function (item) {
            item.label = model.attribute(item.name).label();
            return new FilterItem(item);
        });
    };
    return FilterSettingsFactory;
}());

var ServiceLocator = /** @class */ (function () {
    function ServiceLocator() {
    }
    ServiceLocator.http = function () {
        return ServiceLocator.injector.get(HttpClient);
    };
    ServiceLocator.colQuery = function () {
        return ServiceLocator.injector.get(CollectionQuery$1);
    };
    /**
     * Provide data provider factory
     */
    ServiceLocator.setDataProviderFactory = function (dpf) {
        ServiceLocator.dataProviderFactory = dpf;
    };
    /**
     * Provide instance of data provider
     */
    ServiceLocator.getDataProviderFactory = function () {
        return ServiceLocator.dataProviderFactory;
    };
    return ServiceLocator;
}());

var CollectionQuery = /** @class */ (function (_super) {
    __extends(CollectionQuery, _super);
    function CollectionQuery() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.s = {};
        _this.q = {};
        _this._sort = {};
        _this._query = {};
        return _this;
    }
    CollectionQuery.prototype.setTerm = function (term, fts) {
        if (fts === void 0) { fts = []; }
        try {
            this.q = JSON.parse(term);
            return this;
        }
        catch (e) {
        }
        if (fts.length === 0) {
            fts.push('name');
        }
        this.q = term ? fts.reduce(function (p, c) {
            var t = {};
            t[c] = { '$regex': term, $options: 'i' };
            p.$or.push(t);
            return p;
        }, { $or: [] }) : {};
        return this;
    };
    // toJSON(): any {
    //     return {
    //         forGrid: true,
    //         jsonQuery: JSON.stringify(this.q),
    //         limit: this.limit(),
    //         skip: this.offset()
    //     };
    // }
    CollectionQuery.prototype.toJSON = function () {
        var settings = [];
        try {
            settings = this.filterSettings.toRequestJSON();
        }
        catch (e) {
        }
        return {
            filter: settings,
            limit: this.limit(),
            skip: this.offset(),
            sort: this.sort(),
        };
    };
    CollectionQuery.prototype.setFilter = function (filter) {
        this.filterSettings = filter;
        return this;
    };
    CollectionQuery.prototype.filter = function () {
        return this.q;
    };
    CollectionQuery.prototype.sort = function () {
        return this.s;
    };
    /**
     *
     * @param {string} attributeName
     * @param {string} direction
     * @returns {this}
     */
    CollectionQuery.prototype.setSort = function (attributeName, direction) {
        var _this = this;
        if ([SORT$1.ASC, SORT$1.DESC].indexOf(direction) === -1) {
            return this;
        }
        Object.keys(this.s).forEach(function (n) {
            delete _this.s[n];
        });
        switch (direction) {
            case SORT$1.ASC:
                this.s[attributeName] = 1;
                break;
            case SORT$1.DESC:
                this.s[attributeName] = -1;
        }
        return this;
    };
    CollectionQuery.ɵfac = function CollectionQuery_Factory(t) { return ɵCollectionQuery_BaseFactory(t || CollectionQuery); };
    CollectionQuery.ɵprov = ɵɵdefineInjectable({ token: CollectionQuery, factory: CollectionQuery.ɵfac });
    return CollectionQuery;
}(BaseQuery$1));
var ɵCollectionQuery_BaseFactory = ɵɵgetInheritedFactory(CollectionQuery);
/*@__PURE__*/ (function () { ɵsetClassMetadata(CollectionQuery, [{
        type: Injectable
    }], null, null); })();

var ModelList = /** @class */ (function () {
    function ModelList() {
        this._list = [];
        this._em = new EventEmitter();
    }
    /**
     * @return EventEmitter<IModelListActionEvent>
     */
    ModelList.prototype.em = function () {
        return this._em;
    };
    /**
     * Provide reference for list
     * @return {IModel[]}
     */
    ModelList.prototype.list = function () {
        return this._list;
    };
    ModelList.ɵfac = function ModelList_Factory(t) { return new (t || ModelList)(); };
    ModelList.ɵprov = ɵɵdefineInjectable({ token: ModelList, factory: ModelList.ɵfac });
    return ModelList;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(ModelList, [{
        type: Injectable
    }], null, null); })();

/*
 * Public API Surface of core-ui
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ABaseModel, ACollection, AModel, ActionNames, Attribute, AutoAttribute, BaseCollection, BaseQuery, CollectionProviderDecorator, CollectionQuery, ContentTypeInterceptor, EnumModel, Filter, FilterItem, FilterSettingsFactory, FilterType, LangInterceptor, ModelList, ModelProviderDecorator, ReadOnlyAttribute, SORT, ServiceLocator, SimpleAttribute, SystemErrorInterceptor };
//# sourceMappingURL=core-ui.js.map
