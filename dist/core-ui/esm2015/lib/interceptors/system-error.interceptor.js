import { Injectable } from '@angular/core';
import { HttpErrorResponse, } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
export class SystemErrorInterceptor {
    constructor() {
    }
    intercept(req, next) {
        return next.handle(req)
            .pipe(catchError((response) => {
            if (response instanceof HttpErrorResponse) {
                const { error } = response;
                const systemError = error && error.errors ? error.errors.find(item => item.attribute === 'system') : null;
                if (systemError) {
                    // systemError.messages.forEach((message => this.toast.danger(message)));
                }
            }
            return of(response);
        }));
    }
}
SystemErrorInterceptor.ɵfac = function SystemErrorInterceptor_Factory(t) { return new (t || SystemErrorInterceptor)(); };
SystemErrorInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: SystemErrorInterceptor, factory: SystemErrorInterceptor.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SystemErrorInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLWVycm9yLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmNlcHRvcnMvc3lzdGVtLWVycm9yLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUNMLGlCQUFpQixHQUtsQixNQUFNLHNCQUFzQixDQUFDO0FBRTlCLE9BQU8sRUFBYSxFQUFFLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDcEMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztBQUcxQyxNQUFNLE9BQU8sc0JBQXNCO0lBQ2pDO0lBQ0EsQ0FBQztJQUVELFNBQVMsQ0FBQyxHQUFxQixFQUFFLElBQWlCO1FBRWhELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7YUFDcEIsSUFBSSxDQUNILFVBQVUsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzNCLElBQUksUUFBUSxZQUFZLGlCQUFpQixFQUFFO2dCQUN6QyxNQUFNLEVBQUMsS0FBSyxFQUFDLEdBQUcsUUFBUSxDQUFDO2dCQUV6QixNQUFNLFdBQVcsR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBRTFHLElBQUksV0FBVyxFQUFFO29CQUNmLHlFQUF5RTtpQkFDMUU7YUFDRjtZQUVELE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUNILENBQUM7SUFDTixDQUFDOzs0RkF0QlUsc0JBQXNCOzhEQUF0QixzQkFBc0IsV0FBdEIsc0JBQXNCO2tEQUF0QixzQkFBc0I7Y0FEbEMsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgSHR0cEVycm9yUmVzcG9uc2UsXHJcbiAgSHR0cEV2ZW50LFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBJbnRlcmNlcHRvcixcclxuICBIdHRwUmVxdWVzdCxcclxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcblxyXG5pbXBvcnQge09ic2VydmFibGUsIG9mfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHtjYXRjaEVycm9yfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTeXN0ZW1FcnJvckludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICB9XHJcblxyXG4gIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG5cclxuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXEpXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGNhdGNoRXJyb3IoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICAgIGlmIChyZXNwb25zZSBpbnN0YW5jZW9mIEh0dHBFcnJvclJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHtlcnJvcn0gPSByZXNwb25zZTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHN5c3RlbUVycm9yID0gZXJyb3IgJiYgZXJyb3IuZXJyb3JzID8gZXJyb3IuZXJyb3JzLmZpbmQoaXRlbSA9PiBpdGVtLmF0dHJpYnV0ZSA9PT0gJ3N5c3RlbScpIDogbnVsbDtcclxuXHJcbiAgICAgICAgICAgIGlmIChzeXN0ZW1FcnJvcikge1xyXG4gICAgICAgICAgICAgIC8vIHN5c3RlbUVycm9yLm1lc3NhZ2VzLmZvckVhY2goKG1lc3NhZ2UgPT4gdGhpcy50b2FzdC5kYW5nZXIobWVzc2FnZSkpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHJldHVybiBvZihyZXNwb25zZSk7XHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuICB9XHJcbn1cclxuIl19