import { BaseCollection } from 'projects/core-ui/src/lib/data-structures/collections/base.collection';
export class ACollection extends BaseCollection {
    /**
     * @inheritDoc
     */
    dataProvider() {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    }
    /**
     *
     * @returns {string[]}
     */
    fullTextSearchKeys() {
        return [];
    }
    /**
     * @inheritDoc
     */
    filter() {
        return [];
    }
    /**
     * Provide list of keys that able to be sorted
     * @returns {string[]}
     */
    sortKeys() {
        return [];
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL2NvbGxlY3Rpb25zL2NvbGxlY3Rpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLHNFQUFzRSxDQUFDO0FBS3BHLE1BQU0sT0FBZ0IsV0FBWSxTQUFRLGNBQWM7SUFDdEQ7O09BRUc7SUFDSCxZQUFZO1FBQ1YsSUFBSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLEVBQUU7WUFDbkMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEI7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVEOzs7T0FHRztJQUNJLGtCQUFrQjtRQUN2QixPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU07UUFDWCxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRDs7O09BR0c7SUFDSSxRQUFRO1FBQ2IsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0Jhc2VDb2xsZWN0aW9ufSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvZGF0YS1zdHJ1Y3R1cmVzL2NvbGxlY3Rpb25zL2Jhc2UuY29sbGVjdGlvbic7XHJcbmltcG9ydCB7SUNvbGxlY3Rpb259IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7SUNvbGxlY3Rpb25EYXRhUHJvdmlkZXJ9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtcHJvdmlkZXInO1xyXG5pbXBvcnQge0lGaWx0ZXJTZXR0aW5nc30gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL3F1ZXJpZXMnO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFDb2xsZWN0aW9uIGV4dGVuZHMgQmFzZUNvbGxlY3Rpb24gaW1wbGVtZW50cyBJQ29sbGVjdGlvbiB7XHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBkYXRhUHJvdmlkZXIoKTogSUNvbGxlY3Rpb25EYXRhUHJvdmlkZXIge1xyXG4gICAgaWYgKHR5cGVvZiB0aGlzWyckZHAnXSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgcmV0dXJuIHRoaXNbJyRkcCddO1xyXG4gICAgfVxyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdZb3Ugc2hvdWxkIHVzZSBhbnkgb2YgZGF0YSBwcm92aWRlciBkZWNvcmF0b3JzJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtzdHJpbmdbXX1cclxuICAgKi9cclxuICBwdWJsaWMgZnVsbFRleHRTZWFyY2hLZXlzKCk6IHN0cmluZ1tdIHtcclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgcHVibGljIGZpbHRlcigpOiBJRmlsdGVyU2V0dGluZ3NbXSB7XHJcbiAgICByZXR1cm4gW107XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBQcm92aWRlIGxpc3Qgb2Yga2V5cyB0aGF0IGFibGUgdG8gYmUgc29ydGVkXHJcbiAgICogQHJldHVybnMge3N0cmluZ1tdfVxyXG4gICAqL1xyXG4gIHB1YmxpYyBzb3J0S2V5cygpOiBzdHJpbmdbXSB7XHJcbiAgICByZXR1cm4gW107XHJcbiAgfVxyXG59XHJcbiJdfQ==