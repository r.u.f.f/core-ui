export class BaseCollection {
    constructor() {
        this._data = [];
    }
    /**
     * @inheritDoc
     */
    count() {
        return this._data.length;
    }
    /**
     * @inheritDoc
     */
    data() {
        return this._data;
    }
    /**
     * @inheritDoc
     */
    add(model) {
        this._data.push(model);
        return this;
    }
    /**
     * @inheritDoc
     */
    clear() {
        this._data.splice(0, this.count());
        return this;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5jb2xsZWN0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9kYXRhLXN0cnVjdHVyZXMvY29sbGVjdGlvbnMvYmFzZS5jb2xsZWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sT0FBZ0IsY0FBYztJQUFwQztRQUVVLFVBQUssR0FBc0IsRUFBRSxDQUFDO0lBcUN4QyxDQUFDO0lBbkNDOztPQUVHO0lBQ0gsS0FBSztRQUNILE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDM0IsQ0FBQztJQUdEOztPQUVHO0lBQ0gsSUFBSTtRQUNGLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBT0Q7O09BRUc7SUFDSCxHQUFHLENBQUMsS0FBaUI7UUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxLQUFLO1FBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ25DLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJQmFzZUNvbGxlY3Rpb259IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7SUJhc2VNb2RlbH0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscyc7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZUNvbGxlY3Rpb24gaW1wbGVtZW50cyBJQmFzZUNvbGxlY3Rpb24ge1xyXG5cclxuICBwcml2YXRlIF9kYXRhOiBBcnJheTxJQmFzZU1vZGVsPiA9IFtdO1xyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGNvdW50KCk6IG51bWJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5fZGF0YS5sZW5ndGg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBkYXRhKCk6IEFycmF5PElCYXNlTW9kZWw+IHtcclxuICAgIHJldHVybiB0aGlzLl9kYXRhO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBhYnN0cmFjdCBtb2RlbCgpOiBJQmFzZU1vZGVsO1xyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGFkZChtb2RlbDogSUJhc2VNb2RlbCk6IHRoaXMge1xyXG4gICAgdGhpcy5fZGF0YS5wdXNoKG1vZGVsKTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBjbGVhcigpOiB0aGlzIHtcclxuICAgIHRoaXMuX2RhdGEuc3BsaWNlKDAsIHRoaXMuY291bnQoKSk7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcbn1cclxuIl19