import { Attribute } from 'projects/core-ui/src/lib/data-structures/attributes/attribute';
export class AutoAttribute extends Attribute {
    constructor(labels, attributes, name, model) {
        super();
        this.labels = labels;
        this.attributes = attributes;
        this.name = name;
        this.model = model;
        if (!this.model.schema()[this.name]) {
            throw new Error(`Incorrect implementation. Model ${this.model.constructor.name} have no attribute [${this.name}]!`);
        }
    }
    setValue(value) {
        this.attributes[this.name] = value;
        this.model.onAttributesChanged().emit(this.name);
        return this;
    }
    value() {
        return this.attributes[this.name];
    }
    valueByKey(key) {
        return this.attributes[this.name][key];
    }
    label() {
        return this.labels[this.name];
    }
    errors() {
        return this.model.errors(this.name);
    }
    is(val) {
        return this.attributes[this.name] === val;
    }
    getName() {
        return this.name;
    }
    description() {
        return this.model.schema()[this.name].$d ? this.model.schema()[this.name].$d : '';
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0by1hdHRyaWJ1dGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLXVpLyIsInNvdXJjZXMiOlsibGliL2RhdGEtc3RydWN0dXJlcy9hdHRyaWJ1dGVzL2F1dG8tYXR0cmlidXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSwrREFBK0QsQ0FBQztBQUd4RixNQUFNLE9BQU8sYUFBYyxTQUFRLFNBQVM7SUFFMUMsWUFBb0IsTUFBNEIsRUFDNUIsVUFBZ0MsRUFDaEMsSUFBSSxFQUNKLEtBQWlCO1FBQ25DLEtBQUssRUFBRSxDQUFDO1FBSlUsV0FBTSxHQUFOLE1BQU0sQ0FBc0I7UUFDNUIsZUFBVSxHQUFWLFVBQVUsQ0FBc0I7UUFDaEMsU0FBSSxHQUFKLElBQUksQ0FBQTtRQUNKLFVBQUssR0FBTCxLQUFLLENBQVk7UUFHbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLE1BQU0sSUFBSSxLQUFLLENBQUMsbUNBQW1DLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksdUJBQXVCLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDO1NBQ3JIO0lBQ0gsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFVO1FBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxLQUFLO1FBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsVUFBVSxDQUFDLEdBQVc7UUFDcEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsS0FBSztRQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELE1BQU07UUFDSixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsRUFBRSxDQUFDLEdBQVE7UUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQztJQUM1QyxDQUFDO0lBRU0sT0FBTztRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDO0lBRU0sV0FBVztRQUNoQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDcEYsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBdHRyaWJ1dGV9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9kYXRhLXN0cnVjdHVyZXMvYXR0cmlidXRlcy9hdHRyaWJ1dGUnO1xyXG5pbXBvcnQge0lCYXNlTW9kZWx9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMvSUJhc2VNb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgQXV0b0F0dHJpYnV0ZSBleHRlbmRzIEF0dHJpYnV0ZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbGFiZWxzOiB7IFtzOiBzdHJpbmddOiBhbnkgfSxcclxuICAgICAgICAgICAgICBwcml2YXRlIGF0dHJpYnV0ZXM6IHsgW3M6IHN0cmluZ106IGFueSB9LFxyXG4gICAgICAgICAgICAgIHByaXZhdGUgbmFtZSxcclxuICAgICAgICAgICAgICBwcml2YXRlIG1vZGVsOiBJQmFzZU1vZGVsKSB7XHJcbiAgICBzdXBlcigpO1xyXG5cclxuICAgIGlmICghdGhpcy5tb2RlbC5zY2hlbWEoKVt0aGlzLm5hbWVdKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihgSW5jb3JyZWN0IGltcGxlbWVudGF0aW9uLiBNb2RlbCAke3RoaXMubW9kZWwuY29uc3RydWN0b3IubmFtZX0gaGF2ZSBubyBhdHRyaWJ1dGUgWyR7dGhpcy5uYW1lfV0hYCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRWYWx1ZSh2YWx1ZTogYW55KTogdGhpcyB7XHJcbiAgICB0aGlzLmF0dHJpYnV0ZXNbdGhpcy5uYW1lXSA9IHZhbHVlO1xyXG4gICAgdGhpcy5tb2RlbC5vbkF0dHJpYnV0ZXNDaGFuZ2VkKCkuZW1pdCh0aGlzLm5hbWUpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICB2YWx1ZSgpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuYXR0cmlidXRlc1t0aGlzLm5hbWVdO1xyXG4gIH1cclxuXHJcbiAgdmFsdWVCeUtleShrZXk6IHN0cmluZyk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5hdHRyaWJ1dGVzW3RoaXMubmFtZV1ba2V5XTtcclxuICB9XHJcblxyXG4gIGxhYmVsKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5sYWJlbHNbdGhpcy5uYW1lXTtcclxuICB9XHJcblxyXG4gIGVycm9ycygpOiBBcnJheTxzdHJpbmc+IHtcclxuICAgIHJldHVybiB0aGlzLm1vZGVsLmVycm9ycyh0aGlzLm5hbWUpO1xyXG4gIH1cclxuXHJcbiAgaXModmFsOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZXNbdGhpcy5uYW1lXSA9PT0gdmFsO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldE5hbWUoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZGVzY3JpcHRpb24oKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLm1vZGVsLnNjaGVtYSgpW3RoaXMubmFtZV0uJGQgPyB0aGlzLm1vZGVsLnNjaGVtYSgpW3RoaXMubmFtZV0uJGQgOiAnJztcclxuICB9XHJcbn1cclxuIl19