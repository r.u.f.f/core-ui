export class SimpleAttribute {
    constructor(_val, _label, _description = '') {
        this._val = _val;
        this._label = _label;
        this._description = _description;
    }
    value() {
        return this._val;
    }
    setValue(value) {
        this._val = value;
        return this;
    }
    label() {
        return this._label;
    }
    description() {
        return this._description;
    }
    errors() {
        return [];
    }
    is(val) {
        return this._val === val;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2ltcGxlLWF0dHJpYnV0ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMvc2ltcGxlLWF0dHJpYnV0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8sZUFBZTtJQUUxQixZQUFvQixJQUFTLEVBQ1QsTUFBYyxFQUNkLGVBQXVCLEVBQUU7UUFGekIsU0FBSSxHQUFKLElBQUksQ0FBSztRQUNULFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxpQkFBWSxHQUFaLFlBQVksQ0FBYTtJQUU3QyxDQUFDO0lBRUQsS0FBSztRQUNILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQVU7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFDbEIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsS0FBSztRQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBRUQsV0FBVztRQUNULE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDO0lBRUQsTUFBTTtRQUNKLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVELEVBQUUsQ0FBQyxHQUFRO1FBQ1QsT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQztJQUMzQixDQUFDO0NBRUYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lBdHRyaWJ1dGV9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9hdHRyaWJ1dGVzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBTaW1wbGVBdHRyaWJ1dGUgaW1wbGVtZW50cyBJQXR0cmlidXRlIHtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfdmFsOiBhbnksXHJcbiAgICAgICAgICAgICAgcHJpdmF0ZSBfbGFiZWw6IHN0cmluZyxcclxuICAgICAgICAgICAgICBwcml2YXRlIF9kZXNjcmlwdGlvbjogc3RyaW5nID0gJycpIHtcclxuXHJcbiAgfVxyXG5cclxuICB2YWx1ZSgpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuX3ZhbDtcclxuICB9XHJcblxyXG4gIHNldFZhbHVlKHZhbHVlOiBhbnkpOiB0aGlzIHtcclxuICAgIHRoaXMuX3ZhbCA9IHZhbHVlO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBsYWJlbCgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMuX2xhYmVsO1xyXG4gIH1cclxuXHJcbiAgZGVzY3JpcHRpb24oKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9kZXNjcmlwdGlvbjtcclxuICB9XHJcblxyXG4gIGVycm9ycygpOiBBcnJheTxzdHJpbmc+IHtcclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcblxyXG4gIGlzKHZhbDogYW55KTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fdmFsID09PSB2YWw7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=