export class ReadOnlyAttribute {
    constructor(lbl, val) {
        this.lbl = lbl;
        this.val = val;
        this.errs = [];
    }
    label() {
        return this.lbl;
    }
    setValue(value) {
        return this;
    }
    value() {
        return this.val;
    }
    errors() {
        return this.errs;
    }
    is(val) {
        return this.value() === val;
    }
    description() {
        return '';
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVhZC1vbmx5LWF0dHJpYnV0ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMvcmVhZC1vbmx5LWF0dHJpYnV0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8saUJBQWlCO0lBSTVCLFlBQXNCLEdBQVcsRUFBWSxHQUFRO1FBQS9CLFFBQUcsR0FBSCxHQUFHLENBQVE7UUFBWSxRQUFHLEdBQUgsR0FBRyxDQUFLO1FBRjdDLFNBQUksR0FBRyxFQUFFLENBQUM7SUFJbEIsQ0FBQztJQUVELEtBQUs7UUFDSCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDbEIsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELEtBQUs7UUFDSCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDbEIsQ0FBQztJQUVELE1BQU07UUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbkIsQ0FBQztJQUVELEVBQUUsQ0FBQyxHQUFRO1FBQ1QsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssR0FBRyxDQUFDO0lBQzlCLENBQUM7SUFFTSxXQUFXO1FBQ2hCLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztDQUVGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJQXR0cmlidXRlfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvYXR0cmlidXRlcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgUmVhZE9ubHlBdHRyaWJ1dGUgaW1wbGVtZW50cyBJQXR0cmlidXRlIHtcclxuXHJcbiAgcHJpdmF0ZSBlcnJzID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBsYmw6IHN0cmluZywgcHJvdGVjdGVkIHZhbDogYW55KSB7XHJcblxyXG4gIH1cclxuXHJcbiAgbGFiZWwoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLmxibDtcclxuICB9XHJcblxyXG4gIHNldFZhbHVlKHZhbHVlOiBhbnkpOiB0aGlzIHtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgdmFsdWUoKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLnZhbDtcclxuICB9XHJcblxyXG4gIGVycm9ycygpOiBBcnJheTxzdHJpbmc+IHtcclxuICAgIHJldHVybiB0aGlzLmVycnM7XHJcbiAgfVxyXG5cclxuICBpcyh2YWw6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMudmFsdWUoKSA9PT0gdmFsO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGRlc2NyaXB0aW9uKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gJyc7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=