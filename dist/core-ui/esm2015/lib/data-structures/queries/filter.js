import { FilterType } from 'projects/core-ui/src/lib/interfaces/data-structures/queries';
export class Filter {
    constructor(filterItems) {
        this.filterItems = filterItems;
    }
    toRequestJSON() {
        return this
            .getSettings()
            .map(i => i.toResponse())
            .filter((i) => {
            return (i.val !== null && i.val !== undefined) ||
                (i.minVal !== null && i.minVal !== undefined) ||
                (i.maxVal !== null && i.maxVal !== undefined);
        })
            .filter((i) => {
            return !(Array.isArray(i.val) && i.val.length === 0);
        })
            .filter((i) => {
            return !(i.type === FilterType.ILIKE && !i.val);
        });
    }
    getSettings() {
        return this.filterItems;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9kYXRhLXN0cnVjdHVyZXMvcXVlcmllcy9maWx0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFVBQVUsRUFHWCxNQUFNLDZEQUE2RCxDQUFDO0FBUXJFLE1BQU0sT0FBTyxNQUFNO0lBRWpCLFlBQW9CLFdBQTBCO1FBQTFCLGdCQUFXLEdBQVgsV0FBVyxDQUFlO0lBRTlDLENBQUM7SUFFTSxhQUFhO1FBQ2xCLE9BQU8sSUFBSTthQUNSLFdBQVcsRUFBRTthQUNiLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUN4QixNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNaLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFNBQVMsQ0FBQztnQkFDNUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLFNBQVMsQ0FBQztnQkFDN0MsQ0FBQyxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLFNBQVMsQ0FBQyxDQUFDO1FBQ2xELENBQUMsQ0FBQzthQUNELE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ1osT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDO2FBQ0QsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDWixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sV0FBVztRQUNoQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDMUIsQ0FBQztDQUVGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBGaWx0ZXJUeXBlLFxyXG4gIElGaWx0ZXJJdGVtLFxyXG4gIElSZXNwXHJcbn0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL3F1ZXJpZXMnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBJRmlsdGVyIHtcclxuICB0b1JlcXVlc3RKU09OKCk6IElSZXNwW107XHJcblxyXG4gIGdldFNldHRpbmdzKCk6IElGaWx0ZXJJdGVtW107XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXIgaW1wbGVtZW50cyBJRmlsdGVyIHtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmaWx0ZXJJdGVtczogSUZpbHRlckl0ZW1bXSkge1xyXG5cclxuICB9XHJcblxyXG4gIHB1YmxpYyB0b1JlcXVlc3RKU09OKCk6IElSZXNwW10ge1xyXG4gICAgcmV0dXJuIHRoaXNcclxuICAgICAgLmdldFNldHRpbmdzKClcclxuICAgICAgLm1hcChpID0+IGkudG9SZXNwb25zZSgpKVxyXG4gICAgICAuZmlsdGVyKChpKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIChpLnZhbCAhPT0gbnVsbCAmJiBpLnZhbCAhPT0gdW5kZWZpbmVkKSB8fFxyXG4gICAgICAgICAgKGkubWluVmFsICE9PSBudWxsICYmIGkubWluVmFsICE9PSB1bmRlZmluZWQpIHx8XHJcbiAgICAgICAgICAoaS5tYXhWYWwgIT09IG51bGwgJiYgaS5tYXhWYWwgIT09IHVuZGVmaW5lZCk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5maWx0ZXIoKGkpID0+IHtcclxuICAgICAgICByZXR1cm4gIShBcnJheS5pc0FycmF5KGkudmFsKSAmJiBpLnZhbC5sZW5ndGggPT09IDApO1xyXG4gICAgICB9KVxyXG4gICAgICAuZmlsdGVyKChpKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuICEoaS50eXBlID09PSBGaWx0ZXJUeXBlLklMSUtFICYmICFpLnZhbCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFNldHRpbmdzKCk6IElGaWx0ZXJJdGVtW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuZmlsdGVySXRlbXM7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=