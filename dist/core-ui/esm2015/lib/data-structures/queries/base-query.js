export class BaseQuery {
    constructor() {
        this._limit = 20;
        this._offset = 0;
    }
    limit() {
        return this._limit;
    }
    offset() {
        return this._offset;
    }
    toJSON() {
        return {
            forGrid: true,
            limit: this.limit(),
            skip: this.offset()
        };
    }
    setLimit(limit) {
        this._limit = limit;
        return this;
    }
    setOffset(offset) {
        this._offset = offset;
        return this;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1xdWVyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL3F1ZXJpZXMvYmFzZS1xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLE9BQU8sU0FBUztJQUF0QjtRQUNZLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFDWixZQUFPLEdBQUcsQ0FBQyxDQUFDO0lBNEJ4QixDQUFDO0lBMUJDLEtBQUs7UUFDSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDckIsQ0FBQztJQUVELE1BQU07UUFDSixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDdEIsQ0FBQztJQUVELE1BQU07UUFDSixPQUFPO1lBQ0wsT0FBTyxFQUFFLElBQUk7WUFDYixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRTtTQUNwQixDQUFDO0lBQ0osQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFhO1FBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELFNBQVMsQ0FBQyxNQUFjO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztDQUVGIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEJhc2VRdWVyeSB7XHJcbiAgcHJvdGVjdGVkIF9saW1pdCA9IDIwO1xyXG4gIHByb3RlY3RlZCBfb2Zmc2V0ID0gMDtcclxuXHJcbiAgbGltaXQoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9saW1pdDtcclxuICB9XHJcblxyXG4gIG9mZnNldCgpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRoaXMuX29mZnNldDtcclxuICB9XHJcblxyXG4gIHRvSlNPTigpOiBhbnkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZm9yR3JpZDogdHJ1ZSxcclxuICAgICAgbGltaXQ6IHRoaXMubGltaXQoKSxcclxuICAgICAgc2tpcDogdGhpcy5vZmZzZXQoKVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHNldExpbWl0KGxpbWl0OiBudW1iZXIpOiB0aGlzIHtcclxuICAgIHRoaXMuX2xpbWl0ID0gbGltaXQ7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIHNldE9mZnNldChvZmZzZXQ6IG51bWJlcik6IHRoaXMge1xyXG4gICAgdGhpcy5fb2Zmc2V0ID0gb2Zmc2V0O1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=