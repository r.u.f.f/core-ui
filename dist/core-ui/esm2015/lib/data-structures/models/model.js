import { ABaseModel } from 'projects/core-ui/src/lib/data-structures/models/base.model';
export class AModel extends ABaseModel {
    /**
     * @inheritDoc
     */
    dataProvider() {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    }
    /**
     * @inheritDoc
     */
    pk() {
        if (this.schema()['id'] === undefined) {
            throw new Error(`Could not to find default primary key in model.
You must redeclare pk() in model or define 'id' field in schema.`);
        }
        return 'id';
    }
    /**
     * @inheritDoc
     */
    requestFormatter() {
        return {};
    }
    getDataToSave() {
        return super.getDataToSave();
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLXVpLyIsInNvdXJjZXMiOlsibGliL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMvbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLDREQUE0RCxDQUFDO0FBSXRGLE1BQU0sT0FBZ0IsTUFBTyxTQUFRLFVBQVU7SUFDM0M7O09BRUc7SUFDSCxZQUFZO1FBQ1IsSUFBSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLEVBQUU7WUFDakMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEI7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVEOztPQUVHO0lBQ0gsRUFBRTtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNuQyxNQUFNLElBQUksS0FBSyxDQUNYO2lFQUNpRCxDQUNwRCxDQUFDO1NBQ0w7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxnQkFBZ0I7UUFDWixPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxhQUFhO1FBQ1QsT0FBTyxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDakMsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBQmFzZU1vZGVsfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscy9iYXNlLm1vZGVsJztcclxuaW1wb3J0IHtJTW9kZWx9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMvSU1vZGVsJztcclxuaW1wb3J0IHtJTW9kZWxEYXRhUHJvdmlkZXJ9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtcHJvdmlkZXIvSU1vZGVsRGF0YVByb3ZpZGVyJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBTW9kZWwgZXh0ZW5kcyBBQmFzZU1vZGVsIGltcGxlbWVudHMgSU1vZGVsIHtcclxuICAgIC8qKlxyXG4gICAgICogQGluaGVyaXREb2NcclxuICAgICAqL1xyXG4gICAgZGF0YVByb3ZpZGVyKCk6IElNb2RlbERhdGFQcm92aWRlciB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzWyckZHAnXSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXNbJyRkcCddO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBzaG91bGQgdXNlIGFueSBvZiBkYXRhIHByb3ZpZGVyIGRlY29yYXRvcnMnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBpbmhlcml0RG9jXHJcbiAgICAgKi9cclxuICAgIHBrKCk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2NoZW1hKClbJ2lkJ10gPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXHJcbiAgICAgICAgICAgICAgICBgQ291bGQgbm90IHRvIGZpbmQgZGVmYXVsdCBwcmltYXJ5IGtleSBpbiBtb2RlbC5cclxuWW91IG11c3QgcmVkZWNsYXJlIHBrKCkgaW4gbW9kZWwgb3IgZGVmaW5lICdpZCcgZmllbGQgaW4gc2NoZW1hLmBcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICdpZCc7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAaW5oZXJpdERvY1xyXG4gICAgICovXHJcbiAgICByZXF1ZXN0Rm9ybWF0dGVyKCk6IG9iamVjdCB7XHJcbiAgICAgICAgcmV0dXJuIHt9O1xyXG4gICAgfVxyXG5cclxuICAgIGdldERhdGFUb1NhdmUoKTogb2JqZWN0IHtcclxuICAgICAgICByZXR1cm4gc3VwZXIuZ2V0RGF0YVRvU2F2ZSgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==