import { EventEmitter } from '@angular/core';
import { validate } from 'validate.js';
import { AutoAttribute } from 'projects/core-ui/src/lib/data-structures/attributes';
export class ABaseModel {
    constructor() {
        this.ALLOW_NULL_VALUES = true;
        /**
         * Internal dictionary for keeping attributes
         * @private
         */
        this.attributes = {};
        this.enums = {};
        this.editMode = false;
        this.$services = {
            $onAttributesChanged: new EventEmitter(),
            $nullValuesMap: {
                string: '',
                number: 0,
                boolean: false,
                bool: false,
                array: [],
                date_int_ms: new Date(),
            },
            $errors: {},
            $scenario: {
                active: {},
                list: [],
            }
        };
        this.initEmptyAttributes();
    }
    startEdit() {
        this.editMode = true;
        return this;
    }
    finishEdit() {
        this.editMode = false;
        return this;
    }
    isEditModeEnabled() {
        return this.editMode;
    }
    /**
     * Detect null values for each type
     * @param {*} type
     * @return {*}
     */
    nullValueByType(type) {
        if (Array.isArray(type)) {
            return this.$services.$nullValuesMap.array;
        }
        if (typeof type === 'function') {
            const c = type;
            return new c();
        }
        if (this.$services.$nullValuesMap.hasOwnProperty(type)) {
            return this.$services.$nullValuesMap[type];
        }
        return undefined;
    }
    /**
     * Initi attributes after model construct
     * @return {IBaseModel}
     */
    initEmptyAttributes() {
        Object
            .keys(this.schema())
            .forEach(n => this.setAttributeBySchema(n, this.nullValueByType(this.schema()[n].$t)));
        return this;
    }
    /**
     * Defining each attribute by declared schema
     * @param {string} name
     * @param value
     * @return {IBaseModel}
     */
    setAttributeBySchema(name, value) {
        if (this.schema().hasOwnProperty(name)) {
            const cast = (name, value, type) => {
                /**@override IBaseModel this */
                const t = type ? type : this.schema()[name].$t;
                if (typeof t === 'function') { // we think that it is another model
                    const a = (new t());
                    if (value != null) {
                        a.setAttributes(value);
                    }
                    return a;
                }
                switch (t) {
                    case 'string':
                        return String(value);
                    case 'date_int_ms':
                        return new Date(value);
                    case 'number':
                        return Number(value);
                    case 'bool':
                    case 'boolean':
                        if (value === '0') {
                            value = false;
                        }
                        if (value === '1') {
                            value = true;
                        }
                        return Boolean(value);
                }
            };
            if (Array.isArray(this.schema()[name].$t)) {
                this.attributes[name] = [];
                if (this.schema()[name].$t.length !== 1) {
                    throw new Error(`Allowed only one type for array definitions for attribute "${name}"`);
                }
                if (!Array.isArray(value)) {
                    if (this.ALLOW_NULL_VALUES === true && value === null) {
                        value = [];
                    }
                    else {
                        throw new Error(`Value must be array according to scheme definition for attribute "${name}"`);
                    }
                }
                value.forEach((val) => {
                    this.attributes[name].push(cast(name, val, this.schema()[name].$t[0]));
                });
            }
            else {
                this.attributes[name] = cast(name, value);
            }
            this.fillAttributesCopies();
        }
        return this;
    }
    fillAttributesCopies() {
        Object
            .keys(this.schema())
            .forEach((attrName) => {
            if (this.schema()[attrName].$c &&
                this.attributes.hasOwnProperty(this.schema()[attrName].$c)) {
                this.attributes[attrName] = this.attributes[this.schema()[attrName].$c];
            }
        });
        return this;
    }
    /**
     * @inheritDoc
     */
    labels() {
        return Object
            .keys(this.schema())
            .reduce((p, c) => {
            if (this.schema()[c].$l) {
                p[c] = this.schema()[c].$l;
                return p;
            }
            p[c] = c
                .replace(/([A-Z])/g, ' $1')
                .replace(/^./, function (str) {
                return str.toUpperCase();
            });
            p[c] = c.split('_').map((word) => {
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join(' ');
            return p;
        }, {});
    }
    /**
     * @inheritDoc
     */
    descriptions() {
        return {};
    }
    /**
     * @inheritDoc
     */
    attribute(name) {
        return new AutoAttribute(this.labels(), this.attributes, name, this);
    }
    /**
     * Provide attribute value
     */
    value(name) {
        return this.attribute(name).value();
    }
    /**
     * Provide label by attribute name
     */
    label(name) {
        return this.attribute(name).label();
    }
    /**
     * @inheritDoc
     */
    setAttribute(name, value) {
        return this.setAttributeBySchema(name, value);
    }
    /**
     * @inheritDoc
     */
    setAttributes(attributes) {
        Object
            .keys(attributes)
            .forEach((name) => {
            this.setAttributeBySchema(name, attributes[name]);
        });
        return this;
    }
    /**
     * @inheritDoc
     */
    validate() {
        return new Promise((resolve, reject) => {
            this.clearErrors();
            this.$services.$errors = validate(this.attributes, Object
                .keys(this.schema())
                .reduce((p, c) => {
                if (this.schema()[c].$v) {
                    p[c] = this.schema()[c].$v;
                }
                return p;
            }, {}));
            if (this.$services.$errors === undefined) {
                this.$services.$errors = {};
                resolve();
            }
            else {
                reject();
            }
        });
    }
    /**
     * Represent this model as JSON object annotation
     * @return {*}
     */
    toJSON() {
        return Object.keys(this.schema()).reduce((p, c, i, a) => {
            if (this.schema()[c].$t === 'date_int_ms') {
                p[c] = this.attributes[c].getTime();
            }
            else {
                p[c] = this.attributes[c];
            }
            return p;
        }, {});
    }
    /**
     * @inheritDoc
     */
    errors(name) {
        return Array.isArray(this.$services.$errors[name]) ? this.$services.$errors[name] : [];
    }
    /**
     * @inheritDoc
     */
    isValid() {
        return Object.keys(this.$services.$errors).length === 0;
    }
    /**
     * @inheritDoc
     */
    clearErrors() {
        Object
            .keys(this.$services.$errors)
            .forEach((v) => {
            delete this.$services.$errors[v];
        });
        return this;
    }
    /**
     * @todo add ability to detect models
     * @param {Array<string>} names
     * @return {{[p: string]: any}}
     */
    copyRawAttributes(names) {
        try {
            const ser = JSON.stringify(Object.keys(this.schema()).reduce((p, c) => {
                if (Array.isArray(names)) {
                    if (names.indexOf(c) !== -1) {
                        p[c] = this.attribute(c).value();
                    }
                }
                else {
                    p[c] = this.attribute(c).value();
                }
                return p;
            }, {}));
            return JSON.parse(ser);
        }
        catch (e) {
            console.log(`model clone: could not to serialize / un serialize attributes!, message ${e.message}`, e);
        }
    }
    /**
     * @inheritDoc
     */
    clone() {
        const c = this.constructor, cloned = new c;
        cloned.setAttributes(this.copyRawAttributes());
        return cloned;
    }
    /**
     * @inheritDoc
     */
    onAttributesChanged() {
        return this.$services.$onAttributesChanged;
    }
    /**
     * @inheritDoc
     */
    setError(name, message) {
        if (this.schema().hasOwnProperty(name)) {
            if (!Array.isArray(this.$services.$errors[name])) {
                this.$services.$errors[name] = [];
            }
            this.$services.$errors[name].push(message);
        }
        return this;
    }
    /**
     * @inheritDoc
     */
    setErrors(errors) {
        Object.keys(errors).forEach((name) => {
            if (Array.isArray(errors[name])) {
                errors[name].forEach((message) => {
                    this.setError(name, message);
                });
            }
        });
        return this;
    }
    /**
     * @todo implement array path processing like items[3]name
     */
    setErrorsByPath(path, ...message) {
        const steps = path.split('.');
        let model = this;
        let lastStepW = '';
        while (steps.length) {
            const lastStep = steps.shift();
            if (lastStep && lastStep.length > 2 && lastStep[0] === '[' && lastStep[lastStep.length - 1] === ']') {
                const index = parseInt(lastStep.substr(1, lastStep.length - 1), 10);
                lastStepW = steps.shift();
                if (isNaN(index) === false && Array.isArray(model) && model.length - 1 >= index) {
                    model = model[index];
                }
                continue;
            }
            if (steps.length >= 1) {
                // code for checking items[3]name must be here
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            lastStepW = lastStep;
        }
        if (model !== null && typeof model === 'object' && lastStepW !== '') {
            const errs = {};
            errs[lastStepW] = message;
            model.setErrors(errs);
        }
        return this;
    }
    countErrors(...paths) {
        let count = 0;
        paths.forEach((path) => {
            count = this.countErrorsInModel(count, this, path.split('.'));
        });
        return count;
    }
    countErrorsInModel(count, model, steps) {
        let lastStep = '';
        while (steps.length) {
            lastStep = steps.shift();
            if (lastStep === '[*]' && Array.isArray(model)) {
                model.forEach((item) => {
                    count = this.countErrorsInModel(count, item, steps);
                });
            }
            if (steps.length >= 1 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object') {
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            if (steps.length === 0 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object' &&
                model.attribute(lastStep).errors().length > 0) {
                count++;
            }
        }
        return count;
    }
    setErrorsV2(errors) {
        for (const errorsKey in errors) {
            if (errors.hasOwnProperty(errorsKey)) {
                this.setErrorsByPath(errors[errorsKey].attribute, ...errors[errorsKey].messages);
            }
        }
        return this;
    }
    getDataToSave() {
        const data = {};
        Object
            .keys(this.schema())
            .forEach((attrName) => {
            if (this.schema()[attrName].$s) {
                const value = this.attribute(attrName).value();
                data[attrName] = (typeof value.getDataToSave === 'function' ? value.getDataToSave() : value);
            }
        });
        Object.keys(data).forEach((key) => {
            if (typeof data[key] === 'object' && Object.keys(data[key]).length === 0) {
                delete data[key];
            }
            if (Array.isArray(data[key]) && data[key].length === 0) {
                delete data[key];
            }
            if (!data[key]) {
                delete data[key];
            }
        });
        return data;
    }
    setScenario() {
    }
    enum(name) {
        if (!this.enums.hasOwnProperty(name)) {
            throw new Error(`enums have no property ${name}`);
        }
        return this.enums[name];
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscy9iYXNlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGFBQWEsQ0FBQztBQVVyQyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0scURBQXFELENBQUM7QUFHbEYsTUFBTSxPQUFnQixVQUFVO0lBOEI5QjtRQTVCUSxzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFFakM7OztXQUdHO1FBQ08sZUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNoQixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUVqQixjQUFTLEdBQUc7WUFDcEIsb0JBQW9CLEVBQUUsSUFBSSxZQUFZLEVBQVU7WUFDaEQsY0FBYyxFQUFFO2dCQUNkLE1BQU0sRUFBRSxFQUFFO2dCQUNWLE1BQU0sRUFBRSxDQUFDO2dCQUNULE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssRUFBRSxFQUFFO2dCQUNULFdBQVcsRUFBRSxJQUFJLElBQUksRUFBRTthQUN4QjtZQUNELE9BQU8sRUFBRSxFQUFFO1lBQ1gsU0FBUyxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLElBQUksRUFBRSxFQUFFO2FBQ1Q7U0FDRixDQUFDO1FBSUEsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVNLFNBQVM7UUFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUVyQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxVQUFVO1FBQ2YsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFFdEIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3ZCLENBQUM7SUFPRDs7OztPQUlHO0lBQ0ssZUFBZSxDQUFDLElBQVM7UUFFL0IsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxPQUFPLElBQUksS0FBSyxVQUFVLEVBQUU7WUFDOUIsTUFBTSxDQUFDLEdBQXFCLElBQUksQ0FBQztZQUNqQyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7U0FDaEI7UUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0RCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDbkIsQ0FBQztJQUVEOzs7T0FHRztJQUNLLG1CQUFtQjtRQUN6QixNQUFNO2FBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNuQixPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6RixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLG9CQUFvQixDQUFDLElBQVksRUFBRSxLQUFVO1FBQ25ELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUV0QyxNQUFNLElBQUksR0FBRyxDQUFDLElBQVksRUFBRSxLQUFVLEVBQUUsSUFBbUIsRUFBTyxFQUFFO2dCQUNsRSwrQkFBK0I7Z0JBQy9CLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUMvQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLFVBQVUsRUFBRSxFQUFFLG9DQUFvQztvQkFDakUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ3BCLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTt3QkFDakIsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDeEI7b0JBQ0QsT0FBTyxDQUFDLENBQUM7aUJBQ1Y7Z0JBQ0QsUUFBUSxDQUFDLEVBQUU7b0JBQ1QsS0FBSyxRQUFRO3dCQUNYLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN2QixLQUFLLGFBQWE7d0JBQ2hCLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3pCLEtBQUssUUFBUTt3QkFDWCxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDdkIsS0FBSyxNQUFNLENBQUM7b0JBQ1osS0FBSyxTQUFTO3dCQUNaLElBQUksS0FBSyxLQUFLLEdBQUcsRUFBRTs0QkFDakIsS0FBSyxHQUFHLEtBQUssQ0FBQzt5QkFDZjt3QkFDRCxJQUFJLEtBQUssS0FBSyxHQUFHLEVBQUU7NEJBQ2pCLEtBQUssR0FBRyxJQUFJLENBQUM7eUJBQ2Q7d0JBQ0QsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3pCO1lBQ0gsQ0FBQyxDQUFDO1lBRUYsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRTtnQkFDekMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQzNCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN2QyxNQUFNLElBQUksS0FBSyxDQUFDLDhEQUE4RCxJQUFJLEdBQUcsQ0FBQyxDQUFDO2lCQUN4RjtnQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDekIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssSUFBSSxJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7d0JBQ3JELEtBQUssR0FBRyxFQUFFLENBQUM7cUJBQ1o7eUJBQU07d0JBQ0wsTUFBTSxJQUFJLEtBQUssQ0FBQyxxRUFBcUUsSUFBSSxHQUFHLENBQUMsQ0FBQztxQkFDL0Y7aUJBQ0Y7Z0JBQ0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekUsQ0FBQyxDQUFDLENBQUM7YUFFSjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDM0M7WUFFRCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztTQUM3QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVPLG9CQUFvQjtRQUMxQixNQUFNO2FBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNuQixPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFO2dCQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQzVELElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDekU7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTTtRQUNKLE9BQU8sTUFBTTthQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDbkIsTUFBTSxDQUFDLENBQUMsQ0FBMEIsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3ZCLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUMzQixPQUFPLENBQUMsQ0FBQzthQUNWO1lBQ0QsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7aUJBQ0wsT0FBTyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUM7aUJBQzFCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBVSxHQUFHO2dCQUMxQixPQUFPLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUMzQixDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQVksRUFBRSxFQUFFO2dCQUN2QyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RCxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDYixPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRDs7T0FFRztJQUNILFlBQVk7UUFDVixPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRDs7T0FFRztJQUNILFNBQVMsQ0FBQyxJQUFZO1FBQ3BCLE9BQU8sSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRDs7T0FFRztJQUNILEtBQUssQ0FBQyxJQUFZO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxLQUFLLENBQUMsSUFBWTtRQUNoQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsWUFBWSxDQUFDLElBQVksRUFBRSxLQUFVO1FBQ25DLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxhQUFhLENBQUMsVUFBMEI7UUFDdEMsTUFBTTthQUNILElBQUksQ0FBQyxVQUFVLENBQUM7YUFDaEIsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDaEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsUUFBUTtRQUNOLE9BQU8sSUFBSSxPQUFPLENBQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU07aUJBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7aUJBQ25CLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDZixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ3ZCLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUM1QjtnQkFDRCxPQUFPLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1YsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUU7Z0JBQ3hDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztnQkFDNUIsT0FBTyxFQUFFLENBQUM7YUFDWDtpQkFBTTtnQkFDTCxNQUFNLEVBQUUsQ0FBQzthQUNWO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsTUFBTTtRQUNKLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0RCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssYUFBYSxFQUFFO2dCQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUNyQztpQkFBTTtnQkFDTCxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMzQjtZQUNELE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTSxDQUFDLElBQVk7UUFDakIsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDekYsQ0FBQztJQUVEOztPQUVHO0lBQ0gsT0FBTztRQUNMLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsV0FBVztRQUNULE1BQU07YUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUM7YUFDNUIsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDYixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDO1FBQ0wsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILGlCQUFpQixDQUFDLEtBQXFCO1FBQ3JDLElBQUk7WUFDRixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwRSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3hCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDM0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7cUJBQ2xDO2lCQUNGO3FCQUFNO29CQUNMLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUNsQztnQkFDRCxPQUFPLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3hCO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixPQUFPLENBQUMsR0FBRyxDQUFDLDJFQUEyRSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDeEc7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxLQUFLO1FBQ0gsTUFBTSxDQUFDLEdBQVEsSUFBSSxDQUFDLFdBQVcsRUFDN0IsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ2pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQztRQUMvQyxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxtQkFBbUI7UUFDakIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDO0lBQzdDLENBQUM7SUFFRDs7T0FFRztJQUNJLFFBQVEsQ0FBQyxJQUFZLEVBQUUsT0FBZTtRQUMzQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ25DO1lBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBR0Q7O09BRUc7SUFDSSxTQUFTLENBQUMsTUFBa0I7UUFDakMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFZLEVBQUUsRUFBRTtZQUMzQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFlLEVBQUUsRUFBRTtvQkFDdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQy9CLENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOztPQUVHO0lBQ0ssZUFBZSxDQUFDLElBQVksRUFBRSxHQUFHLE9BQWlCO1FBQ3hELE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUIsSUFBSSxLQUFLLEdBQWUsSUFBSSxDQUFDO1FBQzdCLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNuQixPQUFPLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDbkIsTUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQy9CLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO2dCQUNuRyxNQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDcEUsU0FBUyxHQUFHLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxFQUFFO29CQUMvRSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN0QjtnQkFDRCxTQUFTO2FBQ1Y7WUFFRCxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUNyQiw4Q0FBOEM7Z0JBQzlDLElBQUk7b0JBQ0YsS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQzNDO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO2lCQUVYO2FBQ0Y7WUFDRCxTQUFTLEdBQUcsUUFBUSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxLQUFLLEtBQUssSUFBSSxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxTQUFTLEtBQUssRUFBRSxFQUFFO1lBQ25FLE1BQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQzFCLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFHTSxXQUFXLENBQUMsR0FBRyxLQUFlO1FBQ25DLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNyQixLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRU0sa0JBQWtCLENBQUMsS0FBYSxFQUFFLEtBQWlCLEVBQUUsS0FBZTtRQUN6RSxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsT0FBTyxLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ25CLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDekIsSUFBSSxRQUFRLEtBQUssS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtvQkFDckIsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN0RCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7Z0JBQ25CLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ3JCLEtBQUssS0FBSyxJQUFJO2dCQUNkLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDM0IsSUFBSTtvQkFDRixLQUFLLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDM0M7Z0JBQUMsT0FBTyxDQUFDLEVBQUU7aUJBRVg7YUFDRjtZQUNELElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDO2dCQUNwQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUNyQixLQUFLLEtBQUssSUFBSTtnQkFDZCxPQUFPLEtBQUssS0FBSyxRQUFRO2dCQUN6QixLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQy9DLEtBQUssRUFBRSxDQUFDO2FBQ1Q7U0FDRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVNLFdBQVcsQ0FBQyxNQUFvQjtRQUNyQyxLQUFLLE1BQU0sU0FBUyxJQUFJLE1BQU0sRUFBRTtZQUM5QixJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNsRjtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBR00sYUFBYTtRQUNsQixNQUFNLElBQUksR0FBRyxFQUFFLENBQUM7UUFFaEIsTUFBTTthQUNILElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDbkIsT0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDcEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUM5QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEtBQUssQ0FBQyxhQUFhLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzlGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBRWhDLElBQUksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDeEUsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbEI7WUFFRCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3RELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDZCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsV0FBVztJQUVYLENBQUM7SUFFTSxJQUFJLENBQUMsSUFBSTtRQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNwQyxNQUFNLElBQUksS0FBSyxDQUFDLDBCQUEwQixJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQ25EO1FBRUQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RXZlbnRFbWl0dGVyfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHt2YWxpZGF0ZX0gZnJvbSAndmFsaWRhdGUuanMnO1xyXG5cclxuaW1wb3J0IHtcclxuICBBdHRyaWJ1dGVzVHlwZSxcclxuICBEZXNjcmlwdGlvbnNUeXBlLCBFcnJvcnNUeXBlLCBFcnJvcnNUeXBlVjIsXHJcbiAgSUJhc2VNb2RlbCxcclxuICBMYWJlbHNUeXBlLFxyXG4gIFNjaGVtYVR5cGVcclxufSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvbW9kZWxzJztcclxuaW1wb3J0IHtJQXR0cmlidXRlfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvYXR0cmlidXRlcyc7XHJcbmltcG9ydCB7QXV0b0F0dHJpYnV0ZX0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2RhdGEtc3RydWN0dXJlcy9hdHRyaWJ1dGVzJztcclxuaW1wb3J0IHtFbnVtTW9kZWx9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9kYXRhLXN0cnVjdHVyZXMvbW9kZWxzL2VudW0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFCYXNlTW9kZWwgaW1wbGVtZW50cyBJQmFzZU1vZGVsIHtcclxuXHJcbiAgcHJpdmF0ZSBBTExPV19OVUxMX1ZBTFVFUyA9IHRydWU7XHJcblxyXG4gIC8qKlxyXG4gICAqIEludGVybmFsIGRpY3Rpb25hcnkgZm9yIGtlZXBpbmcgYXR0cmlidXRlc1xyXG4gICAqIEBwcml2YXRlXHJcbiAgICovXHJcbiAgcHJvdGVjdGVkIGF0dHJpYnV0ZXMgPSB7fTtcclxuICBwcm90ZWN0ZWQgZW51bXMgPSB7fTtcclxuICBwcm90ZWN0ZWQgZWRpdE1vZGUgPSBmYWxzZTtcclxuXHJcbiAgcHJvdGVjdGVkICRzZXJ2aWNlcyA9IHtcclxuICAgICRvbkF0dHJpYnV0ZXNDaGFuZ2VkOiBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKSxcclxuICAgICRudWxsVmFsdWVzTWFwOiB7XHJcbiAgICAgIHN0cmluZzogJycsXHJcbiAgICAgIG51bWJlcjogMCxcclxuICAgICAgYm9vbGVhbjogZmFsc2UsXHJcbiAgICAgIGJvb2w6IGZhbHNlLFxyXG4gICAgICBhcnJheTogW10sXHJcbiAgICAgIGRhdGVfaW50X21zOiBuZXcgRGF0ZSgpLFxyXG4gICAgfSxcclxuICAgICRlcnJvcnM6IHt9LFxyXG4gICAgJHNjZW5hcmlvOiB7XHJcbiAgICAgIGFjdGl2ZToge30sXHJcbiAgICAgIGxpc3Q6IFtdLFxyXG4gICAgfVxyXG4gIH07XHJcblxyXG5cclxuICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLmluaXRFbXB0eUF0dHJpYnV0ZXMoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGFydEVkaXQoKTogdGhpcyB7XHJcbiAgICB0aGlzLmVkaXRNb2RlID0gdHJ1ZTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIHB1YmxpYyBmaW5pc2hFZGl0KCk6IHRoaXMge1xyXG4gICAgdGhpcy5lZGl0TW9kZSA9IGZhbHNlO1xyXG5cclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgaXNFZGl0TW9kZUVuYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5lZGl0TW9kZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgYWJzdHJhY3Qgc2NoZW1hKCk6IFNjaGVtYVR5cGU7XHJcblxyXG4gIC8qKlxyXG4gICAqIERldGVjdCBudWxsIHZhbHVlcyBmb3IgZWFjaCB0eXBlXHJcbiAgICogQHBhcmFtIHsqfSB0eXBlXHJcbiAgICogQHJldHVybiB7Kn1cclxuICAgKi9cclxuICBwcml2YXRlIG51bGxWYWx1ZUJ5VHlwZSh0eXBlOiBhbnkpOiBhbnkge1xyXG5cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHR5cGUpKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLiRzZXJ2aWNlcy4kbnVsbFZhbHVlc01hcC5hcnJheTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodHlwZW9mIHR5cGUgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgY29uc3QgYzogYW55IHwgSUJhc2VNb2RlbCA9IHR5cGU7XHJcbiAgICAgIHJldHVybiBuZXcgYygpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLiRzZXJ2aWNlcy4kbnVsbFZhbHVlc01hcC5oYXNPd25Qcm9wZXJ0eSh0eXBlKSkge1xyXG4gICAgICByZXR1cm4gdGhpcy4kc2VydmljZXMuJG51bGxWYWx1ZXNNYXBbdHlwZV07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogSW5pdGkgYXR0cmlidXRlcyBhZnRlciBtb2RlbCBjb25zdHJ1Y3RcclxuICAgKiBAcmV0dXJuIHtJQmFzZU1vZGVsfVxyXG4gICAqL1xyXG4gIHByaXZhdGUgaW5pdEVtcHR5QXR0cmlidXRlcygpOiBJQmFzZU1vZGVsIHtcclxuICAgIE9iamVjdFxyXG4gICAgICAua2V5cyh0aGlzLnNjaGVtYSgpKVxyXG4gICAgICAuZm9yRWFjaChuID0+IHRoaXMuc2V0QXR0cmlidXRlQnlTY2hlbWEobiwgdGhpcy5udWxsVmFsdWVCeVR5cGUodGhpcy5zY2hlbWEoKVtuXS4kdCkpKTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRGVmaW5pbmcgZWFjaCBhdHRyaWJ1dGUgYnkgZGVjbGFyZWQgc2NoZW1hXHJcbiAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcclxuICAgKiBAcGFyYW0gdmFsdWVcclxuICAgKiBAcmV0dXJuIHtJQmFzZU1vZGVsfVxyXG4gICAqL1xyXG4gIHByaXZhdGUgc2V0QXR0cmlidXRlQnlTY2hlbWEobmFtZTogc3RyaW5nLCB2YWx1ZTogYW55KTogdGhpcyB7XHJcbiAgICBpZiAodGhpcy5zY2hlbWEoKS5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xyXG5cclxuICAgICAgY29uc3QgY2FzdCA9IChuYW1lOiBzdHJpbmcsIHZhbHVlOiBhbnksIHR5cGU/OiBzdHJpbmcgfCBhbnkpOiBhbnkgPT4ge1xyXG4gICAgICAgIC8qKkBvdmVycmlkZSBJQmFzZU1vZGVsIHRoaXMgKi9cclxuICAgICAgICBjb25zdCB0ID0gdHlwZSA/IHR5cGUgOiB0aGlzLnNjaGVtYSgpW25hbWVdLiR0O1xyXG4gICAgICAgIGlmICh0eXBlb2YgdCA9PT0gJ2Z1bmN0aW9uJykgeyAvLyB3ZSB0aGluayB0aGF0IGl0IGlzIGFub3RoZXIgbW9kZWxcclxuICAgICAgICAgIGNvbnN0IGEgPSAobmV3IHQoKSk7XHJcbiAgICAgICAgICBpZiAodmFsdWUgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICBhLnNldEF0dHJpYnV0ZXModmFsdWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIGE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHN3aXRjaCAodCkge1xyXG4gICAgICAgICAgY2FzZSAnc3RyaW5nJzpcclxuICAgICAgICAgICAgcmV0dXJuIFN0cmluZyh2YWx1ZSk7XHJcbiAgICAgICAgICBjYXNlICdkYXRlX2ludF9tcyc6XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgRGF0ZSh2YWx1ZSk7XHJcbiAgICAgICAgICBjYXNlICdudW1iZXInOlxyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICAgIGNhc2UgJ2Jvb2wnOlxyXG4gICAgICAgICAgY2FzZSAnYm9vbGVhbic6XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gJzAnKSB7XHJcbiAgICAgICAgICAgICAgdmFsdWUgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodmFsdWUgPT09ICcxJykge1xyXG4gICAgICAgICAgICAgIHZhbHVlID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gQm9vbGVhbih2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5zY2hlbWEoKVtuYW1lXS4kdCkpIHtcclxuICAgICAgICB0aGlzLmF0dHJpYnV0ZXNbbmFtZV0gPSBbXTtcclxuICAgICAgICBpZiAodGhpcy5zY2hlbWEoKVtuYW1lXS4kdC5sZW5ndGggIT09IDEpIHtcclxuICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgQWxsb3dlZCBvbmx5IG9uZSB0eXBlIGZvciBhcnJheSBkZWZpbml0aW9ucyBmb3IgYXR0cmlidXRlIFwiJHtuYW1lfVwiYCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICAgIGlmICh0aGlzLkFMTE9XX05VTExfVkFMVUVTID09PSB0cnVlICYmIHZhbHVlID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gW107XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYFZhbHVlIG11c3QgYmUgYXJyYXkgYWNjb3JkaW5nIHRvIHNjaGVtZSBkZWZpbml0aW9uIGZvciBhdHRyaWJ1dGUgXCIke25hbWV9XCJgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdmFsdWUuZm9yRWFjaCgodmFsKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmF0dHJpYnV0ZXNbbmFtZV0ucHVzaChjYXN0KG5hbWUsIHZhbCwgdGhpcy5zY2hlbWEoKVtuYW1lXS4kdFswXSkpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmF0dHJpYnV0ZXNbbmFtZV0gPSBjYXN0KG5hbWUsIHZhbHVlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5maWxsQXR0cmlidXRlc0NvcGllcygpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGZpbGxBdHRyaWJ1dGVzQ29waWVzKCk6IHRoaXMge1xyXG4gICAgT2JqZWN0XHJcbiAgICAgIC5rZXlzKHRoaXMuc2NoZW1hKCkpXHJcbiAgICAgIC5mb3JFYWNoKChhdHRyTmFtZSkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnNjaGVtYSgpW2F0dHJOYW1lXS4kYyAmJlxyXG4gICAgICAgICAgdGhpcy5hdHRyaWJ1dGVzLmhhc093blByb3BlcnR5KHRoaXMuc2NoZW1hKClbYXR0ck5hbWVdLiRjKSkge1xyXG4gICAgICAgICAgdGhpcy5hdHRyaWJ1dGVzW2F0dHJOYW1lXSA9IHRoaXMuYXR0cmlidXRlc1t0aGlzLnNjaGVtYSgpW2F0dHJOYW1lXS4kY107XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBsYWJlbHMoKTogTGFiZWxzVHlwZSB7XHJcbiAgICByZXR1cm4gT2JqZWN0XHJcbiAgICAgIC5rZXlzKHRoaXMuc2NoZW1hKCkpXHJcbiAgICAgIC5yZWR1Y2UoKHA6IHsgW3M6IHN0cmluZ106IHN0cmluZyB9LCBjKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2NoZW1hKClbY10uJGwpIHtcclxuICAgICAgICAgIHBbY10gPSB0aGlzLnNjaGVtYSgpW2NdLiRsO1xyXG4gICAgICAgICAgcmV0dXJuIHA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHBbY10gPSBjXHJcbiAgICAgICAgICAucmVwbGFjZSgvKFtBLVpdKS9nLCAnICQxJylcclxuICAgICAgICAgIC5yZXBsYWNlKC9eLi8sIGZ1bmN0aW9uIChzdHIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHN0ci50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgcFtjXSA9IGMuc3BsaXQoJ18nKS5tYXAoKHdvcmQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHdvcmQuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyB3b3JkLnNsaWNlKDEpO1xyXG4gICAgICAgIH0pLmpvaW4oJyAnKTtcclxuICAgICAgICByZXR1cm4gcDtcclxuICAgICAgfSwge30pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBkZXNjcmlwdGlvbnMoKTogRGVzY3JpcHRpb25zVHlwZSB7XHJcbiAgICByZXR1cm4ge307XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGF0dHJpYnV0ZShuYW1lOiBzdHJpbmcpOiBJQXR0cmlidXRlIHtcclxuICAgIHJldHVybiBuZXcgQXV0b0F0dHJpYnV0ZSh0aGlzLmxhYmVscygpLCB0aGlzLmF0dHJpYnV0ZXMsIG5hbWUsIHRoaXMpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUHJvdmlkZSBhdHRyaWJ1dGUgdmFsdWVcclxuICAgKi9cclxuICB2YWx1ZShuYW1lOiBzdHJpbmcpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuYXR0cmlidXRlKG5hbWUpLnZhbHVlKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBQcm92aWRlIGxhYmVsIGJ5IGF0dHJpYnV0ZSBuYW1lXHJcbiAgICovXHJcbiAgbGFiZWwobmFtZTogc3RyaW5nKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZShuYW1lKS5sYWJlbCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBzZXRBdHRyaWJ1dGUobmFtZTogc3RyaW5nLCB2YWx1ZTogYW55KTogdGhpcyB7XHJcbiAgICByZXR1cm4gdGhpcy5zZXRBdHRyaWJ1dGVCeVNjaGVtYShuYW1lLCB2YWx1ZSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIHNldEF0dHJpYnV0ZXMoYXR0cmlidXRlczogQXR0cmlidXRlc1R5cGUpOiB0aGlzIHtcclxuICAgIE9iamVjdFxyXG4gICAgICAua2V5cyhhdHRyaWJ1dGVzKVxyXG4gICAgICAuZm9yRWFjaCgobmFtZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0QXR0cmlidXRlQnlTY2hlbWEobmFtZSwgYXR0cmlidXRlc1tuYW1lXSk7XHJcbiAgICAgIH0pO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIHZhbGlkYXRlKCk6IFByb21pc2U8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIHRoaXMuY2xlYXJFcnJvcnMoKTtcclxuICAgICAgdGhpcy4kc2VydmljZXMuJGVycm9ycyA9IHZhbGlkYXRlKHRoaXMuYXR0cmlidXRlcywgT2JqZWN0XHJcbiAgICAgICAgLmtleXModGhpcy5zY2hlbWEoKSlcclxuICAgICAgICAucmVkdWNlKChwLCBjKSA9PiB7XHJcbiAgICAgICAgICBpZiAodGhpcy5zY2hlbWEoKVtjXS4kdikge1xyXG4gICAgICAgICAgICBwW2NdID0gdGhpcy5zY2hlbWEoKVtjXS4kdjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBwO1xyXG4gICAgICAgIH0sIHt9KSk7XHJcbiAgICAgIGlmICh0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICB0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzID0ge307XHJcbiAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJlamVjdCgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlcHJlc2VudCB0aGlzIG1vZGVsIGFzIEpTT04gb2JqZWN0IGFubm90YXRpb25cclxuICAgKiBAcmV0dXJuIHsqfVxyXG4gICAqL1xyXG4gIHRvSlNPTigpIHtcclxuICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLnNjaGVtYSgpKS5yZWR1Y2UoKHAsIGMsIGksIGEpID0+IHtcclxuICAgICAgaWYgKHRoaXMuc2NoZW1hKClbY10uJHQgPT09ICdkYXRlX2ludF9tcycpIHtcclxuICAgICAgICBwW2NdID0gdGhpcy5hdHRyaWJ1dGVzW2NdLmdldFRpbWUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBwW2NdID0gdGhpcy5hdHRyaWJ1dGVzW2NdO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBwO1xyXG4gICAgfSwge30pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBlcnJvcnMobmFtZTogc3RyaW5nKTogQXJyYXk8c3RyaW5nPiB7XHJcbiAgICByZXR1cm4gQXJyYXkuaXNBcnJheSh0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzW25hbWVdKSA/IHRoaXMuJHNlcnZpY2VzLiRlcnJvcnNbbmFtZV0gOiBbXTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzKS5sZW5ndGggPT09IDA7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGNsZWFyRXJyb3JzKCk6IHRoaXMge1xyXG4gICAgT2JqZWN0XHJcbiAgICAgIC5rZXlzKHRoaXMuJHNlcnZpY2VzLiRlcnJvcnMpXHJcbiAgICAgIC5mb3JFYWNoKCh2KSA9PiB7XHJcbiAgICAgICAgZGVsZXRlIHRoaXMuJHNlcnZpY2VzLiRlcnJvcnNbdl07XHJcbiAgICAgIH0pO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAdG9kbyBhZGQgYWJpbGl0eSB0byBkZXRlY3QgbW9kZWxzXHJcbiAgICogQHBhcmFtIHtBcnJheTxzdHJpbmc+fSBuYW1lc1xyXG4gICAqIEByZXR1cm4ge3tbcDogc3RyaW5nXTogYW55fX1cclxuICAgKi9cclxuICBjb3B5UmF3QXR0cmlidXRlcyhuYW1lcz86IEFycmF5PHN0cmluZz4pOiB7IFtuYW1lOiBzdHJpbmddOiBhbnkgfSB7XHJcbiAgICB0cnkge1xyXG4gICAgICBjb25zdCBzZXIgPSBKU09OLnN0cmluZ2lmeShPYmplY3Qua2V5cyh0aGlzLnNjaGVtYSgpKS5yZWR1Y2UoKHAsIGMpID0+IHtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShuYW1lcykpIHtcclxuICAgICAgICAgIGlmIChuYW1lcy5pbmRleE9mKGMpICE9PSAtMSkge1xyXG4gICAgICAgICAgICBwW2NdID0gdGhpcy5hdHRyaWJ1dGUoYykudmFsdWUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcFtjXSA9IHRoaXMuYXR0cmlidXRlKGMpLnZhbHVlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBwO1xyXG4gICAgICB9LCB7fSkpO1xyXG4gICAgICByZXR1cm4gSlNPTi5wYXJzZShzZXIpO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICBjb25zb2xlLmxvZyhgbW9kZWwgY2xvbmU6IGNvdWxkIG5vdCB0byBzZXJpYWxpemUgLyB1biBzZXJpYWxpemUgYXR0cmlidXRlcyEsIG1lc3NhZ2UgJHtlLm1lc3NhZ2V9YCwgZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGNsb25lKCk6IHRoaXMge1xyXG4gICAgY29uc3QgYzogYW55ID0gdGhpcy5jb25zdHJ1Y3RvcixcclxuICAgICAgY2xvbmVkID0gbmV3IGM7XHJcbiAgICBjbG9uZWQuc2V0QXR0cmlidXRlcyh0aGlzLmNvcHlSYXdBdHRyaWJ1dGVzKCkpO1xyXG4gICAgcmV0dXJuIGNsb25lZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgb25BdHRyaWJ1dGVzQ2hhbmdlZCgpOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiB7XHJcbiAgICByZXR1cm4gdGhpcy4kc2VydmljZXMuJG9uQXR0cmlidXRlc0NoYW5nZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIHB1YmxpYyBzZXRFcnJvcihuYW1lOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZyk6IHRoaXMge1xyXG4gICAgaWYgKHRoaXMuc2NoZW1hKCkuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcclxuICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHRoaXMuJHNlcnZpY2VzLiRlcnJvcnNbbmFtZV0pKSB7XHJcbiAgICAgICAgdGhpcy4kc2VydmljZXMuJGVycm9yc1tuYW1lXSA9IFtdO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuJHNlcnZpY2VzLiRlcnJvcnNbbmFtZV0ucHVzaChtZXNzYWdlKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgcHVibGljIHNldEVycm9ycyhlcnJvcnM6IEVycm9yc1R5cGUpOiB0aGlzIHtcclxuICAgIE9iamVjdC5rZXlzKGVycm9ycykuZm9yRWFjaCgobmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KGVycm9yc1tuYW1lXSkpIHtcclxuICAgICAgICBlcnJvcnNbbmFtZV0uZm9yRWFjaCgobWVzc2FnZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNldEVycm9yKG5hbWUsIG1lc3NhZ2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEB0b2RvIGltcGxlbWVudCBhcnJheSBwYXRoIHByb2Nlc3NpbmcgbGlrZSBpdGVtc1szXW5hbWVcclxuICAgKi9cclxuICBwcml2YXRlIHNldEVycm9yc0J5UGF0aChwYXRoOiBzdHJpbmcsIC4uLm1lc3NhZ2U6IHN0cmluZ1tdKSB7XHJcbiAgICBjb25zdCBzdGVwcyA9IHBhdGguc3BsaXQoJy4nKTtcclxuICAgIGxldCBtb2RlbDogSUJhc2VNb2RlbCA9IHRoaXM7XHJcbiAgICBsZXQgbGFzdFN0ZXBXID0gJyc7XHJcbiAgICB3aGlsZSAoc3RlcHMubGVuZ3RoKSB7XHJcbiAgICAgIGNvbnN0IGxhc3RTdGVwID0gc3RlcHMuc2hpZnQoKTtcclxuICAgICAgaWYgKGxhc3RTdGVwICYmIGxhc3RTdGVwLmxlbmd0aCA+IDIgJiYgbGFzdFN0ZXBbMF0gPT09ICdbJyAmJiBsYXN0U3RlcFtsYXN0U3RlcC5sZW5ndGggLSAxXSA9PT0gJ10nKSB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPSBwYXJzZUludChsYXN0U3RlcC5zdWJzdHIoMSwgbGFzdFN0ZXAubGVuZ3RoIC0gMSksIDEwKTtcclxuICAgICAgICBsYXN0U3RlcFcgPSBzdGVwcy5zaGlmdCgpO1xyXG4gICAgICAgIGlmIChpc05hTihpbmRleCkgPT09IGZhbHNlICYmIEFycmF5LmlzQXJyYXkobW9kZWwpICYmIG1vZGVsLmxlbmd0aCAtIDEgPj0gaW5kZXgpIHtcclxuICAgICAgICAgIG1vZGVsID0gbW9kZWxbaW5kZXhdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb250aW51ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHN0ZXBzLmxlbmd0aCA+PSAxKSB7XHJcbiAgICAgICAgLy8gY29kZSBmb3IgY2hlY2tpbmcgaXRlbXNbM11uYW1lIG11c3QgYmUgaGVyZVxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBtb2RlbCA9IG1vZGVsLmF0dHJpYnV0ZShsYXN0U3RlcCkudmFsdWUoKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBsYXN0U3RlcFcgPSBsYXN0U3RlcDtcclxuICAgIH1cclxuICAgIGlmIChtb2RlbCAhPT0gbnVsbCAmJiB0eXBlb2YgbW9kZWwgPT09ICdvYmplY3QnICYmIGxhc3RTdGVwVyAhPT0gJycpIHtcclxuICAgICAgY29uc3QgZXJycyA9IHt9O1xyXG4gICAgICBlcnJzW2xhc3RTdGVwV10gPSBtZXNzYWdlO1xyXG4gICAgICBtb2RlbC5zZXRFcnJvcnMoZXJycyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG5cclxuICBwdWJsaWMgY291bnRFcnJvcnMoLi4ucGF0aHM6IHN0cmluZ1tdKTogbnVtYmVyIHtcclxuICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICBwYXRocy5mb3JFYWNoKChwYXRoKSA9PiB7XHJcbiAgICAgIGNvdW50ID0gdGhpcy5jb3VudEVycm9yc0luTW9kZWwoY291bnQsIHRoaXMsIHBhdGguc3BsaXQoJy4nKSk7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBjb3VudDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjb3VudEVycm9yc0luTW9kZWwoY291bnQ6IG51bWJlciwgbW9kZWw6IElCYXNlTW9kZWwsIHN0ZXBzOiBzdHJpbmdbXSk6IG51bWJlciB7XHJcbiAgICBsZXQgbGFzdFN0ZXAgPSAnJztcclxuICAgIHdoaWxlIChzdGVwcy5sZW5ndGgpIHtcclxuICAgICAgbGFzdFN0ZXAgPSBzdGVwcy5zaGlmdCgpO1xyXG4gICAgICBpZiAobGFzdFN0ZXAgPT09ICdbKl0nICYmIEFycmF5LmlzQXJyYXkobW9kZWwpKSB7XHJcbiAgICAgICAgbW9kZWwuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgY291bnQgPSB0aGlzLmNvdW50RXJyb3JzSW5Nb2RlbChjb3VudCwgaXRlbSwgc3RlcHMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChzdGVwcy5sZW5ndGggPj0gMSAmJlxyXG4gICAgICAgICFBcnJheS5pc0FycmF5KG1vZGVsKSAmJlxyXG4gICAgICAgIG1vZGVsICE9PSBudWxsICYmXHJcbiAgICAgICAgdHlwZW9mIG1vZGVsID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBtb2RlbCA9IG1vZGVsLmF0dHJpYnV0ZShsYXN0U3RlcCkudmFsdWUoKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoc3RlcHMubGVuZ3RoID09PSAwICYmXHJcbiAgICAgICAgIUFycmF5LmlzQXJyYXkobW9kZWwpICYmXHJcbiAgICAgICAgbW9kZWwgIT09IG51bGwgJiZcclxuICAgICAgICB0eXBlb2YgbW9kZWwgPT09ICdvYmplY3QnICYmXHJcbiAgICAgICAgbW9kZWwuYXR0cmlidXRlKGxhc3RTdGVwKS5lcnJvcnMoKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgY291bnQrKztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGNvdW50O1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHNldEVycm9yc1YyKGVycm9yczogRXJyb3JzVHlwZVYyKTogdGhpcyB7XHJcbiAgICBmb3IgKGNvbnN0IGVycm9yc0tleSBpbiBlcnJvcnMpIHtcclxuICAgICAgaWYgKGVycm9ycy5oYXNPd25Qcm9wZXJ0eShlcnJvcnNLZXkpKSB7XHJcbiAgICAgICAgdGhpcy5zZXRFcnJvcnNCeVBhdGgoZXJyb3JzW2Vycm9yc0tleV0uYXR0cmlidXRlLCAuLi5lcnJvcnNbZXJyb3JzS2V5XS5tZXNzYWdlcyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcblxyXG4gIHB1YmxpYyBnZXREYXRhVG9TYXZlKCk6IG9iamVjdCB7XHJcbiAgICBjb25zdCBkYXRhID0ge307XHJcblxyXG4gICAgT2JqZWN0XHJcbiAgICAgIC5rZXlzKHRoaXMuc2NoZW1hKCkpXHJcbiAgICAgIC5mb3JFYWNoKChhdHRyTmFtZSkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnNjaGVtYSgpW2F0dHJOYW1lXS4kcykge1xyXG4gICAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLmF0dHJpYnV0ZShhdHRyTmFtZSkudmFsdWUoKTtcclxuICAgICAgICAgIGRhdGFbYXR0ck5hbWVdID0gKHR5cGVvZiB2YWx1ZS5nZXREYXRhVG9TYXZlID09PSAnZnVuY3Rpb24nID8gdmFsdWUuZ2V0RGF0YVRvU2F2ZSgpIDogdmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgT2JqZWN0LmtleXMoZGF0YSkuZm9yRWFjaCgoa2V5KSA9PiB7XHJcblxyXG4gICAgICBpZiAodHlwZW9mIGRhdGFba2V5XSA9PT0gJ29iamVjdCcgJiYgT2JqZWN0LmtleXMoZGF0YVtrZXldKS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICBkZWxldGUgZGF0YVtrZXldO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheShkYXRhW2tleV0pICYmIGRhdGFba2V5XS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICBkZWxldGUgZGF0YVtrZXldO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoIWRhdGFba2V5XSkge1xyXG4gICAgICAgIGRlbGV0ZSBkYXRhW2tleV07XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBkYXRhO1xyXG4gIH1cclxuXHJcbiAgc2V0U2NlbmFyaW8oKSB7XHJcblxyXG4gIH1cclxuXHJcbiAgcHVibGljIGVudW0obmFtZSk6IEVudW1Nb2RlbCB7XHJcbiAgICBpZiAoIXRoaXMuZW51bXMuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGBlbnVtcyBoYXZlIG5vIHByb3BlcnR5ICR7bmFtZX1gKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcy5lbnVtc1tuYW1lXTtcclxuICB9XHJcbn1cclxuIl19