import { Injectable } from '@angular/core';
import { SORT } from 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery';
import { BaseQuery } from 'projects/core-ui/src/lib/data-structures/queries';
import * as i0 from "@angular/core";
export class CollectionQuery extends BaseQuery {
    constructor() {
        super(...arguments);
        this.s = {};
        this.q = {};
        this._sort = {};
        this._query = {};
    }
    setTerm(term, fts = []) {
        try {
            this.q = JSON.parse(term);
            return this;
        }
        catch (e) {
        }
        if (fts.length === 0) {
            fts.push('name');
        }
        this.q = term ? fts.reduce((p, c) => {
            const t = {};
            t[c] = { '$regex': term, $options: 'i' };
            p.$or.push(t);
            return p;
        }, { $or: [] }) : {};
        return this;
    }
    // toJSON(): any {
    //     return {
    //         forGrid: true,
    //         jsonQuery: JSON.stringify(this.q),
    //         limit: this.limit(),
    //         skip: this.offset()
    //     };
    // }
    toJSON() {
        let settings = [];
        try {
            settings = this.filterSettings.toRequestJSON();
        }
        catch (e) {
        }
        return {
            filter: settings,
            limit: this.limit(),
            skip: this.offset(),
            sort: this.sort(),
        };
    }
    setFilter(filter) {
        this.filterSettings = filter;
        return this;
    }
    filter() {
        return this.q;
    }
    sort() {
        return this.s;
    }
    /**
     *
     * @param {string} attributeName
     * @param {string} direction
     * @returns {this}
     */
    setSort(attributeName, direction) {
        if ([SORT.ASC, SORT.DESC].indexOf(direction) === -1) {
            return this;
        }
        Object.keys(this.s).forEach((n) => {
            delete this.s[n];
        });
        switch (direction) {
            case SORT.ASC:
                this.s[attributeName] = 1;
                break;
            case SORT.DESC:
                this.s[attributeName] = -1;
        }
        return this;
    }
}
CollectionQuery.ɵfac = function CollectionQuery_Factory(t) { return ɵCollectionQuery_BaseFactory(t || CollectionQuery); };
CollectionQuery.ɵprov = i0.ɵɵdefineInjectable({ token: CollectionQuery, factory: CollectionQuery.ɵfac });
const ɵCollectionQuery_BaseFactory = i0.ɵɵgetInheritedFactory(CollectionQuery);
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CollectionQuery, [{
        type: Injectable
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi1xdWVyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvcHJvdmlkZXJzL2NvbGxlY3Rpb24tcXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQW1CLElBQUksRUFBQyxNQUFNLDhFQUE4RSxDQUFDO0FBRXBILE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxrREFBa0QsQ0FBQzs7QUFHM0UsTUFBTSxPQUFPLGVBQWdCLFNBQVEsU0FBUztJQUQ5Qzs7UUFFVSxNQUFDLEdBQXdDLEVBQUUsQ0FBQztRQUU1QyxNQUFDLEdBQUcsRUFBRSxDQUFDO1FBSVAsVUFBSyxHQUF3QyxFQUFFLENBQUM7UUFFaEQsV0FBTSxHQUFHLEVBQUUsQ0FBQztLQW1GckI7SUFqRkMsT0FBTyxDQUFDLElBQVksRUFBRSxNQUFnQixFQUFFO1FBQ3RDLElBQUk7WUFDRixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1NBRVg7UUFFRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbEI7UUFDRCxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNsQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDYixDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNkLE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFFLEVBQUMsR0FBRyxFQUFFLEVBQUUsRUFBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNuQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6Qiw2Q0FBNkM7SUFDN0MsK0JBQStCO0lBQy9CLDhCQUE4QjtJQUM5QixTQUFTO0lBQ1QsSUFBSTtJQUNKLE1BQU07UUFFSixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSTtZQUNGLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ2hEO1FBQUMsT0FBTyxDQUFDLEVBQUU7U0FFWDtRQUNELE9BQU87WUFDTCxNQUFNLEVBQUUsUUFBUTtZQUNoQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRTtTQUNsQixDQUFDO0lBQ0osQ0FBQztJQUVNLFNBQVMsQ0FBQyxNQUFlO1FBQzlCLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDO1FBQzdCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVNLE1BQU07UUFDWCxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUVNLElBQUk7UUFDVCxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksT0FBTyxDQUFDLGFBQXFCLEVBQUUsU0FBaUI7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNuRCxPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBUyxFQUFFLEVBQUU7WUFDeEMsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxTQUFTLEVBQUU7WUFDakIsS0FBSyxJQUFJLENBQUMsR0FBRztnQkFDWCxJQUFJLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDMUIsTUFBTTtZQUNSLEtBQUssSUFBSSxDQUFDLElBQUk7Z0JBQ1osSUFBSSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUM5QjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7c0dBMUZVLGVBQWU7dURBQWYsZUFBZSxXQUFmLGVBQWU7OERBQWYsZUFBZTtrREFBZixlQUFlO2NBRDNCLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge0lDb2xsZWN0aW9uUXVlcnksIFNPUlR9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9xdWVyaWVzL0lDb2xsZWN0aW9uUXVlcnknO1xyXG5pbXBvcnQge0lGaWx0ZXJ9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9kYXRhLXN0cnVjdHVyZXMvcXVlcmllcy9maWx0ZXInO1xyXG5pbXBvcnQge0Jhc2VRdWVyeX0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2RhdGEtc3RydWN0dXJlcy9xdWVyaWVzJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENvbGxlY3Rpb25RdWVyeSBleHRlbmRzIEJhc2VRdWVyeSBpbXBsZW1lbnRzIElDb2xsZWN0aW9uUXVlcnkge1xyXG4gIHByaXZhdGUgczogeyBbYXR0cmlidXRlTmFtZTogc3RyaW5nXTogbnVtYmVyIH0gPSB7fTtcclxuXHJcbiAgcHJpdmF0ZSBxID0ge307XHJcblxyXG4gIHByaXZhdGUgZmlsdGVyU2V0dGluZ3M6IElGaWx0ZXI7XHJcblxyXG4gIHByaXZhdGUgX3NvcnQ6IHsgW2F0dHJpYnV0ZU5hbWU6IHN0cmluZ106IG51bWJlciB9ID0ge307XHJcblxyXG4gIHByaXZhdGUgX3F1ZXJ5ID0ge307XHJcblxyXG4gIHNldFRlcm0odGVybTogc3RyaW5nLCBmdHM6IHN0cmluZ1tdID0gW10pOiB0aGlzIHtcclxuICAgIHRyeSB7XHJcbiAgICAgIHRoaXMucSA9IEpTT04ucGFyc2UodGVybSk7XHJcbiAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBpZiAoZnRzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICBmdHMucHVzaCgnbmFtZScpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5xID0gdGVybSA/IGZ0cy5yZWR1Y2UoKHAsIGMpID0+IHtcclxuICAgICAgY29uc3QgdCA9IHt9O1xyXG4gICAgICB0W2NdID0geyckcmVnZXgnOiB0ZXJtLCAkb3B0aW9uczogJ2knfTtcclxuICAgICAgcC4kb3IucHVzaCh0KTtcclxuICAgICAgcmV0dXJuIHA7XHJcbiAgICB9LCB7JG9yOiBbXX0pIDoge307XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIC8vIHRvSlNPTigpOiBhbnkge1xyXG4gIC8vICAgICByZXR1cm4ge1xyXG4gIC8vICAgICAgICAgZm9yR3JpZDogdHJ1ZSxcclxuICAvLyAgICAgICAgIGpzb25RdWVyeTogSlNPTi5zdHJpbmdpZnkodGhpcy5xKSxcclxuICAvLyAgICAgICAgIGxpbWl0OiB0aGlzLmxpbWl0KCksXHJcbiAgLy8gICAgICAgICBza2lwOiB0aGlzLm9mZnNldCgpXHJcbiAgLy8gICAgIH07XHJcbiAgLy8gfVxyXG4gIHRvSlNPTigpOiBhbnkge1xyXG5cclxuICAgIGxldCBzZXR0aW5ncyA9IFtdO1xyXG4gICAgdHJ5IHtcclxuICAgICAgc2V0dGluZ3MgPSB0aGlzLmZpbHRlclNldHRpbmdzLnRvUmVxdWVzdEpTT04oKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuXHJcbiAgICB9XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmaWx0ZXI6IHNldHRpbmdzLFxyXG4gICAgICBsaW1pdDogdGhpcy5saW1pdCgpLFxyXG4gICAgICBza2lwOiB0aGlzLm9mZnNldCgpLFxyXG4gICAgICBzb3J0OiB0aGlzLnNvcnQoKSxcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2V0RmlsdGVyKGZpbHRlcjogSUZpbHRlcik6IHRoaXMge1xyXG4gICAgdGhpcy5maWx0ZXJTZXR0aW5ncyA9IGZpbHRlcjtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGZpbHRlcigpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMucTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzb3J0KCk6IHsgW3A6IHN0cmluZ106IG51bWJlciB9IHtcclxuICAgIHJldHVybiB0aGlzLnM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBhdHRyaWJ1dGVOYW1lXHJcbiAgICogQHBhcmFtIHtzdHJpbmd9IGRpcmVjdGlvblxyXG4gICAqIEByZXR1cm5zIHt0aGlzfVxyXG4gICAqL1xyXG4gIHB1YmxpYyBzZXRTb3J0KGF0dHJpYnV0ZU5hbWU6IHN0cmluZywgZGlyZWN0aW9uOiBzdHJpbmcpOiB0aGlzIHtcclxuICAgIGlmIChbU09SVC5BU0MsIFNPUlQuREVTQ10uaW5kZXhPZihkaXJlY3Rpb24pID09PSAtMSkge1xyXG4gICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuICAgIE9iamVjdC5rZXlzKHRoaXMucykuZm9yRWFjaCgobjogc3RyaW5nKSA9PiB7XHJcbiAgICAgIGRlbGV0ZSB0aGlzLnNbbl07XHJcbiAgICB9KTtcclxuICAgIHN3aXRjaCAoZGlyZWN0aW9uKSB7XHJcbiAgICAgIGNhc2UgU09SVC5BU0M6XHJcbiAgICAgICAgdGhpcy5zW2F0dHJpYnV0ZU5hbWVdID0gMTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBTT1JULkRFU0M6XHJcbiAgICAgICAgdGhpcy5zW2F0dHJpYnV0ZU5hbWVdID0gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=