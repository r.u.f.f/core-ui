import { EventEmitter, Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class ModelList {
    constructor() {
        this._list = [];
        this._em = new EventEmitter();
    }
    /**
     * @return EventEmitter<IModelListActionEvent>
     */
    em() {
        return this._em;
    }
    /**
     * Provide reference for list
     * @return {IModel[]}
     */
    list() {
        return this._list;
    }
}
ModelList.ɵfac = function ModelList_Factory(t) { return new (t || ModelList)(); };
ModelList.ɵprov = i0.ɵɵdefineInjectable({ token: ModelList, factory: ModelList.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ModelList, [{
        type: Injectable
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwtbGlzdC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvcHJvdmlkZXJzL21vZGVsLWxpc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFlBQVksRUFBRSxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7O0FBU3ZELE1BQU0sT0FBTyxTQUFTO0lBRHRCO1FBR1UsVUFBSyxHQUFhLEVBQUUsQ0FBQztRQUNyQixRQUFHLEdBQXdDLElBQUksWUFBWSxFQUFFLENBQUM7S0FnQnZFO0lBZEM7O09BRUc7SUFDSSxFQUFFO1FBQ1AsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxJQUFJO1FBQ1QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3BCLENBQUM7O2tFQWxCVSxTQUFTO2lEQUFULFNBQVMsV0FBVCxTQUFTO2tEQUFULFNBQVM7Y0FEckIsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RXZlbnRFbWl0dGVyLCBJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtJTW9kZWx9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBJTW9kZWxMaXN0QWN0aW9uRXZlbnQge1xyXG4gIG5hbWU6IHN0cmluZztcclxuICByZXN1bHQ6IGJvb2xlYW47XHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1vZGVsTGlzdCB7XHJcblxyXG4gIHByaXZhdGUgX2xpc3Q6IElNb2RlbFtdID0gW107XHJcbiAgcHJpdmF0ZSBfZW06IEV2ZW50RW1pdHRlcjxJTW9kZWxMaXN0QWN0aW9uRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAvKipcclxuICAgKiBAcmV0dXJuIEV2ZW50RW1pdHRlcjxJTW9kZWxMaXN0QWN0aW9uRXZlbnQ+XHJcbiAgICovXHJcbiAgcHVibGljIGVtKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2VtO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUHJvdmlkZSByZWZlcmVuY2UgZm9yIGxpc3RcclxuICAgKiBAcmV0dXJuIHtJTW9kZWxbXX1cclxuICAgKi9cclxuICBwdWJsaWMgbGlzdCgpOiBJTW9kZWxbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5fbGlzdDtcclxuICB9XHJcbn1cclxuIl19