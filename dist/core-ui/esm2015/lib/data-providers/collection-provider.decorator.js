import { ServiceLocator } from 'projects/core-ui/src/lib/locators/service.locator';
export function CollectionProviderDecorator(options) {
    return function (constructor) {
        return class extends constructor {
            constructor() {
                super(...arguments);
                this.$dp = ServiceLocator.getDataProviderFactory().collection(options.endpoint, this);
            }
        };
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi1wcm92aWRlci5kZWNvcmF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLXVpLyIsInNvdXJjZXMiOlsibGliL2RhdGEtcHJvdmlkZXJzL2NvbGxlY3Rpb24tcHJvdmlkZXIuZGVjb3JhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxtREFBbUQsQ0FBQztBQUVqRixNQUFNLFVBQVUsMkJBQTJCLENBQUMsT0FHM0M7SUFDQyxPQUFPLFVBQWlELFdBQWM7UUFDcEUsT0FBTyxLQUFNLFNBQVEsV0FBVztZQUF6Qjs7Z0JBQ0wsUUFBRyxHQUFHLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ25GLENBQUM7U0FBQSxDQUFDO0lBQ0osQ0FBQyxDQUFDO0FBQ0osQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SUVuZHBvaW50fSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9lbmRwb2ludC9JRW5kcG9pbnQnO1xyXG5pbXBvcnQge1NlcnZpY2VMb2NhdG9yfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvbG9jYXRvcnMvc2VydmljZS5sb2NhdG9yJztcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBDb2xsZWN0aW9uUHJvdmlkZXJEZWNvcmF0b3Iob3B0aW9uczoge1xyXG4gIGVuZHBvaW50OiBJRW5kcG9pbnQsXHJcbiAgYXBpVXJsPzogc3RyaW5nXHJcbn0pIHtcclxuICByZXR1cm4gZnVuY3Rpb24gPFQgZXh0ZW5kcyB7IG5ldyguLi5hcmdzOiBhbnlbXSk6IHt9IH0+KGNvbnN0cnVjdG9yOiBUKSB7XHJcbiAgICByZXR1cm4gY2xhc3MgZXh0ZW5kcyBjb25zdHJ1Y3RvciB7XHJcbiAgICAgICRkcCA9IFNlcnZpY2VMb2NhdG9yLmdldERhdGFQcm92aWRlckZhY3RvcnkoKS5jb2xsZWN0aW9uKG9wdGlvbnMuZW5kcG9pbnQsIHRoaXMpO1xyXG4gICAgfTtcclxuICB9O1xyXG59XHJcbiJdfQ==