import { ServiceLocator as ServiceLocator$1 } from 'projects/core-ui/src/lib/locators/service.locator';
import { Attribute as Attribute$1 } from 'projects/core-ui/src/lib/data-structures/attributes/attribute';
import { BaseCollection as BaseCollection$1 } from 'projects/core-ui/src/lib/data-structures/collections/base.collection';
import { EventEmitter, ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵgetInheritedFactory } from '@angular/core';
import { validate } from 'validate.js';
import { AutoAttribute as AutoAttribute$1 } from 'projects/core-ui/src/lib/data-structures/attributes';
import { ABaseModel as ABaseModel$1 } from 'projects/core-ui/src/lib/data-structures/models/base.model';
import { FilterType as FilterType$1 } from 'projects/core-ui/src/lib/interfaces/data-structures/queries';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CollectionQuery as CollectionQuery$1 } from 'projects/core-ui/src/lib/providers/collection-query';
import { SORT as SORT$1 } from 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery';
import { BaseQuery as BaseQuery$1 } from 'projects/core-ui/src/lib/data-structures/queries';

function ModelProviderDecorator(options) {
    return function (constructor) {
        return class extends constructor {
            constructor() {
                super(...arguments);
                this.$dp = ServiceLocator$1.getDataProviderFactory().model(options.endpoint, this);
            }
        };
    };
}

function CollectionProviderDecorator(options) {
    return function (constructor) {
        return class extends constructor {
            constructor() {
                super(...arguments);
                this.$dp = ServiceLocator$1.getDataProviderFactory().collection(options.endpoint, this);
            }
        };
    };
}

class Attribute {
}

class AutoAttribute extends Attribute$1 {
    constructor(labels, attributes, name, model) {
        super();
        this.labels = labels;
        this.attributes = attributes;
        this.name = name;
        this.model = model;
        if (!this.model.schema()[this.name]) {
            throw new Error(`Incorrect implementation. Model ${this.model.constructor.name} have no attribute [${this.name}]!`);
        }
    }
    setValue(value) {
        this.attributes[this.name] = value;
        this.model.onAttributesChanged().emit(this.name);
        return this;
    }
    value() {
        return this.attributes[this.name];
    }
    valueByKey(key) {
        return this.attributes[this.name][key];
    }
    label() {
        return this.labels[this.name];
    }
    errors() {
        return this.model.errors(this.name);
    }
    is(val) {
        return this.attributes[this.name] === val;
    }
    getName() {
        return this.name;
    }
    description() {
        return this.model.schema()[this.name].$d ? this.model.schema()[this.name].$d : '';
    }
}

class ReadOnlyAttribute {
    constructor(lbl, val) {
        this.lbl = lbl;
        this.val = val;
        this.errs = [];
    }
    label() {
        return this.lbl;
    }
    setValue(value) {
        return this;
    }
    value() {
        return this.val;
    }
    errors() {
        return this.errs;
    }
    is(val) {
        return this.value() === val;
    }
    description() {
        return '';
    }
}

class SimpleAttribute {
    constructor(_val, _label, _description = '') {
        this._val = _val;
        this._label = _label;
        this._description = _description;
    }
    value() {
        return this._val;
    }
    setValue(value) {
        this._val = value;
        return this;
    }
    label() {
        return this._label;
    }
    description() {
        return this._description;
    }
    errors() {
        return [];
    }
    is(val) {
        return this._val === val;
    }
}

class ACollection extends BaseCollection$1 {
    /**
     * @inheritDoc
     */
    dataProvider() {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    }
    /**
     *
     * @returns {string[]}
     */
    fullTextSearchKeys() {
        return [];
    }
    /**
     * @inheritDoc
     */
    filter() {
        return [];
    }
    /**
     * Provide list of keys that able to be sorted
     * @returns {string[]}
     */
    sortKeys() {
        return [];
    }
}

class BaseCollection {
    constructor() {
        this._data = [];
    }
    /**
     * @inheritDoc
     */
    count() {
        return this._data.length;
    }
    /**
     * @inheritDoc
     */
    data() {
        return this._data;
    }
    /**
     * @inheritDoc
     */
    add(model) {
        this._data.push(model);
        return this;
    }
    /**
     * @inheritDoc
     */
    clear() {
        this._data.splice(0, this.count());
        return this;
    }
}

class ABaseModel {
    constructor() {
        this.ALLOW_NULL_VALUES = true;
        /**
         * Internal dictionary for keeping attributes
         * @private
         */
        this.attributes = {};
        this.enums = {};
        this.editMode = false;
        this.$services = {
            $onAttributesChanged: new EventEmitter(),
            $nullValuesMap: {
                string: '',
                number: 0,
                boolean: false,
                bool: false,
                array: [],
                date_int_ms: new Date(),
            },
            $errors: {},
            $scenario: {
                active: {},
                list: [],
            }
        };
        this.initEmptyAttributes();
    }
    startEdit() {
        this.editMode = true;
        return this;
    }
    finishEdit() {
        this.editMode = false;
        return this;
    }
    isEditModeEnabled() {
        return this.editMode;
    }
    /**
     * Detect null values for each type
     * @param {*} type
     * @return {*}
     */
    nullValueByType(type) {
        if (Array.isArray(type)) {
            return this.$services.$nullValuesMap.array;
        }
        if (typeof type === 'function') {
            const c = type;
            return new c();
        }
        if (this.$services.$nullValuesMap.hasOwnProperty(type)) {
            return this.$services.$nullValuesMap[type];
        }
        return undefined;
    }
    /**
     * Initi attributes after model construct
     * @return {IBaseModel}
     */
    initEmptyAttributes() {
        Object
            .keys(this.schema())
            .forEach(n => this.setAttributeBySchema(n, this.nullValueByType(this.schema()[n].$t)));
        return this;
    }
    /**
     * Defining each attribute by declared schema
     * @param {string} name
     * @param value
     * @return {IBaseModel}
     */
    setAttributeBySchema(name, value) {
        if (this.schema().hasOwnProperty(name)) {
            const cast = (name, value, type) => {
                /**@override IBaseModel this */
                const t = type ? type : this.schema()[name].$t;
                if (typeof t === 'function') { // we think that it is another model
                    const a = (new t());
                    if (value != null) {
                        a.setAttributes(value);
                    }
                    return a;
                }
                switch (t) {
                    case 'string':
                        return String(value);
                    case 'date_int_ms':
                        return new Date(value);
                    case 'number':
                        return Number(value);
                    case 'bool':
                    case 'boolean':
                        if (value === '0') {
                            value = false;
                        }
                        if (value === '1') {
                            value = true;
                        }
                        return Boolean(value);
                }
            };
            if (Array.isArray(this.schema()[name].$t)) {
                this.attributes[name] = [];
                if (this.schema()[name].$t.length !== 1) {
                    throw new Error(`Allowed only one type for array definitions for attribute "${name}"`);
                }
                if (!Array.isArray(value)) {
                    if (this.ALLOW_NULL_VALUES === true && value === null) {
                        value = [];
                    }
                    else {
                        throw new Error(`Value must be array according to scheme definition for attribute "${name}"`);
                    }
                }
                value.forEach((val) => {
                    this.attributes[name].push(cast(name, val, this.schema()[name].$t[0]));
                });
            }
            else {
                this.attributes[name] = cast(name, value);
            }
            this.fillAttributesCopies();
        }
        return this;
    }
    fillAttributesCopies() {
        Object
            .keys(this.schema())
            .forEach((attrName) => {
            if (this.schema()[attrName].$c &&
                this.attributes.hasOwnProperty(this.schema()[attrName].$c)) {
                this.attributes[attrName] = this.attributes[this.schema()[attrName].$c];
            }
        });
        return this;
    }
    /**
     * @inheritDoc
     */
    labels() {
        return Object
            .keys(this.schema())
            .reduce((p, c) => {
            if (this.schema()[c].$l) {
                p[c] = this.schema()[c].$l;
                return p;
            }
            p[c] = c
                .replace(/([A-Z])/g, ' $1')
                .replace(/^./, function (str) {
                return str.toUpperCase();
            });
            p[c] = c.split('_').map((word) => {
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join(' ');
            return p;
        }, {});
    }
    /**
     * @inheritDoc
     */
    descriptions() {
        return {};
    }
    /**
     * @inheritDoc
     */
    attribute(name) {
        return new AutoAttribute$1(this.labels(), this.attributes, name, this);
    }
    /**
     * Provide attribute value
     */
    value(name) {
        return this.attribute(name).value();
    }
    /**
     * Provide label by attribute name
     */
    label(name) {
        return this.attribute(name).label();
    }
    /**
     * @inheritDoc
     */
    setAttribute(name, value) {
        return this.setAttributeBySchema(name, value);
    }
    /**
     * @inheritDoc
     */
    setAttributes(attributes) {
        Object
            .keys(attributes)
            .forEach((name) => {
            this.setAttributeBySchema(name, attributes[name]);
        });
        return this;
    }
    /**
     * @inheritDoc
     */
    validate() {
        return new Promise((resolve, reject) => {
            this.clearErrors();
            this.$services.$errors = validate(this.attributes, Object
                .keys(this.schema())
                .reduce((p, c) => {
                if (this.schema()[c].$v) {
                    p[c] = this.schema()[c].$v;
                }
                return p;
            }, {}));
            if (this.$services.$errors === undefined) {
                this.$services.$errors = {};
                resolve();
            }
            else {
                reject();
            }
        });
    }
    /**
     * Represent this model as JSON object annotation
     * @return {*}
     */
    toJSON() {
        return Object.keys(this.schema()).reduce((p, c, i, a) => {
            if (this.schema()[c].$t === 'date_int_ms') {
                p[c] = this.attributes[c].getTime();
            }
            else {
                p[c] = this.attributes[c];
            }
            return p;
        }, {});
    }
    /**
     * @inheritDoc
     */
    errors(name) {
        return Array.isArray(this.$services.$errors[name]) ? this.$services.$errors[name] : [];
    }
    /**
     * @inheritDoc
     */
    isValid() {
        return Object.keys(this.$services.$errors).length === 0;
    }
    /**
     * @inheritDoc
     */
    clearErrors() {
        Object
            .keys(this.$services.$errors)
            .forEach((v) => {
            delete this.$services.$errors[v];
        });
        return this;
    }
    /**
     * @todo add ability to detect models
     * @param {Array<string>} names
     * @return {{[p: string]: any}}
     */
    copyRawAttributes(names) {
        try {
            const ser = JSON.stringify(Object.keys(this.schema()).reduce((p, c) => {
                if (Array.isArray(names)) {
                    if (names.indexOf(c) !== -1) {
                        p[c] = this.attribute(c).value();
                    }
                }
                else {
                    p[c] = this.attribute(c).value();
                }
                return p;
            }, {}));
            return JSON.parse(ser);
        }
        catch (e) {
            console.log(`model clone: could not to serialize / un serialize attributes!, message ${e.message}`, e);
        }
    }
    /**
     * @inheritDoc
     */
    clone() {
        const c = this.constructor, cloned = new c;
        cloned.setAttributes(this.copyRawAttributes());
        return cloned;
    }
    /**
     * @inheritDoc
     */
    onAttributesChanged() {
        return this.$services.$onAttributesChanged;
    }
    /**
     * @inheritDoc
     */
    setError(name, message) {
        if (this.schema().hasOwnProperty(name)) {
            if (!Array.isArray(this.$services.$errors[name])) {
                this.$services.$errors[name] = [];
            }
            this.$services.$errors[name].push(message);
        }
        return this;
    }
    /**
     * @inheritDoc
     */
    setErrors(errors) {
        Object.keys(errors).forEach((name) => {
            if (Array.isArray(errors[name])) {
                errors[name].forEach((message) => {
                    this.setError(name, message);
                });
            }
        });
        return this;
    }
    /**
     * @todo implement array path processing like items[3]name
     */
    setErrorsByPath(path, ...message) {
        const steps = path.split('.');
        let model = this;
        let lastStepW = '';
        while (steps.length) {
            const lastStep = steps.shift();
            if (lastStep && lastStep.length > 2 && lastStep[0] === '[' && lastStep[lastStep.length - 1] === ']') {
                const index = parseInt(lastStep.substr(1, lastStep.length - 1), 10);
                lastStepW = steps.shift();
                if (isNaN(index) === false && Array.isArray(model) && model.length - 1 >= index) {
                    model = model[index];
                }
                continue;
            }
            if (steps.length >= 1) {
                // code for checking items[3]name must be here
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            lastStepW = lastStep;
        }
        if (model !== null && typeof model === 'object' && lastStepW !== '') {
            const errs = {};
            errs[lastStepW] = message;
            model.setErrors(errs);
        }
        return this;
    }
    countErrors(...paths) {
        let count = 0;
        paths.forEach((path) => {
            count = this.countErrorsInModel(count, this, path.split('.'));
        });
        return count;
    }
    countErrorsInModel(count, model, steps) {
        let lastStep = '';
        while (steps.length) {
            lastStep = steps.shift();
            if (lastStep === '[*]' && Array.isArray(model)) {
                model.forEach((item) => {
                    count = this.countErrorsInModel(count, item, steps);
                });
            }
            if (steps.length >= 1 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object') {
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            if (steps.length === 0 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object' &&
                model.attribute(lastStep).errors().length > 0) {
                count++;
            }
        }
        return count;
    }
    setErrorsV2(errors) {
        for (const errorsKey in errors) {
            if (errors.hasOwnProperty(errorsKey)) {
                this.setErrorsByPath(errors[errorsKey].attribute, ...errors[errorsKey].messages);
            }
        }
        return this;
    }
    getDataToSave() {
        const data = {};
        Object
            .keys(this.schema())
            .forEach((attrName) => {
            if (this.schema()[attrName].$s) {
                const value = this.attribute(attrName).value();
                data[attrName] = (typeof value.getDataToSave === 'function' ? value.getDataToSave() : value);
            }
        });
        Object.keys(data).forEach((key) => {
            if (typeof data[key] === 'object' && Object.keys(data[key]).length === 0) {
                delete data[key];
            }
            if (Array.isArray(data[key]) && data[key].length === 0) {
                delete data[key];
            }
            if (!data[key]) {
                delete data[key];
            }
        });
        return data;
    }
    setScenario() {
    }
    enum(name) {
        if (!this.enums.hasOwnProperty(name)) {
            throw new Error(`enums have no property ${name}`);
        }
        return this.enums[name];
    }
}

class EnumModel {
    constructor(enumerable, textList) {
        this.vll = [];
        this.enumerable = enumerable;
        this.textList = textList;
        this.allValuesList = Object.keys(this.enumerable).map((key) => this.enum[key]);
        this.allKeysList = Object.keys(this.enumerable);
    }
    get enum() {
        return this.enumerable;
    }
    get wordings() {
        return this.textList;
    }
    getAllValuesList() {
        return this.allValuesList;
    }
    getValueLabelList() {
        if (this.vll.length === 0) {
            this.allValuesList.forEach(val => {
                this.vll.push({
                    value: val,
                    label: this.wordings[val]
                });
            });
        }
        return this.vll;
    }
    getTextByValue(value) {
        if (!this.wordings[value]) {
            return 'Not in Enum!';
        }
        return this.wordings[value];
    }
    getTextByKey(key) {
        if (!this.wordings[this.enum[key]]) {
            return `Not wording for ${key}! EnumModel: ${this}.`;
        }
        return this.wordings[this.enum[key]];
    }
    getValueByKey(key) {
        return this.enum[key];
    }
}

class AModel extends ABaseModel$1 {
    /**
     * @inheritDoc
     */
    dataProvider() {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    }
    /**
     * @inheritDoc
     */
    pk() {
        if (this.schema()['id'] === undefined) {
            throw new Error(`Could not to find default primary key in model.
You must redeclare pk() in model or define 'id' field in schema.`);
        }
        return 'id';
    }
    /**
     * @inheritDoc
     */
    requestFormatter() {
        return {};
    }
    getDataToSave() {
        return super.getDataToSave();
    }
}

class BaseQuery {
    constructor() {
        this._limit = 20;
        this._offset = 0;
    }
    limit() {
        return this._limit;
    }
    offset() {
        return this._offset;
    }
    toJSON() {
        return {
            forGrid: true,
            limit: this.limit(),
            skip: this.offset()
        };
    }
    setLimit(limit) {
        this._limit = limit;
        return this;
    }
    setOffset(offset) {
        this._offset = offset;
        return this;
    }
}

class Filter {
    constructor(filterItems) {
        this.filterItems = filterItems;
    }
    toRequestJSON() {
        return this
            .getSettings()
            .map(i => i.toResponse())
            .filter((i) => {
            return (i.val !== null && i.val !== undefined) ||
                (i.minVal !== null && i.minVal !== undefined) ||
                (i.maxVal !== null && i.maxVal !== undefined);
        })
            .filter((i) => {
            return !(Array.isArray(i.val) && i.val.length === 0);
        })
            .filter((i) => {
            return !(i.type === FilterType$1.ILIKE && !i.val);
        });
    }
    getSettings() {
        return this.filterItems;
    }
}

class ContentTypeInterceptor {
    constructor() {
    }
    intercept(req, next) {
        if (req.body instanceof FormData) {
            return next.handle(req);
        }
        const authReq = req.clone({
            headers: req.headers
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
        });
        return next.handle(authReq);
    }
}
ContentTypeInterceptor.ɵfac = function ContentTypeInterceptor_Factory(t) { return new (t || ContentTypeInterceptor)(); };
ContentTypeInterceptor.ɵprov = ɵɵdefineInjectable({ token: ContentTypeInterceptor, factory: ContentTypeInterceptor.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(ContentTypeInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();

class LangInterceptor {
    constructor() {
        // private langService: LangService
    }
    intercept(req, next) {
        const authReq = req.clone({
        // headers: req.headers.set(this.langService.getLangHeader(), this.langService.getLangCode())
        });
        return next.handle(authReq);
    }
}
LangInterceptor.ɵfac = function LangInterceptor_Factory(t) { return new (t || LangInterceptor)(); };
LangInterceptor.ɵprov = ɵɵdefineInjectable({ token: LangInterceptor, factory: LangInterceptor.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(LangInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();

class SystemErrorInterceptor {
    constructor() {
    }
    intercept(req, next) {
        return next.handle(req)
            .pipe(catchError((response) => {
            if (response instanceof HttpErrorResponse) {
                const { error } = response;
                const systemError = error && error.errors ? error.errors.find(item => item.attribute === 'system') : null;
                if (systemError) {
                    // systemError.messages.forEach((message => this.toast.danger(message)));
                }
            }
            return of(response);
        }));
    }
}
SystemErrorInterceptor.ɵfac = function SystemErrorInterceptor_Factory(t) { return new (t || SystemErrorInterceptor)(); };
SystemErrorInterceptor.ɵprov = ɵɵdefineInjectable({ token: SystemErrorInterceptor, factory: SystemErrorInterceptor.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(SystemErrorInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();

var ActionNames;
(function (ActionNames) {
    ActionNames["PLinkAction"] = "p-link-action";
    ActionNames["PSearchAction"] = "p-search-action";
    ActionNames["PModalAction"] = "p-modal-action";
    ActionNames["PModelBulkRemoveAction"] = "p-model-bulk-remove-action";
    ActionNames["PCollectionBulkRemoveAction"] = "p-collection-bulk-remove-action";
    ActionNames["PCollectionChooseItemsAction"] = "p-collection-choose-items-action";
})(ActionNames || (ActionNames = {}));

const SORT = {
    ASC: 'asc',
    DESC: 'desc'
};

var FilterType;
(function (FilterType) {
    FilterType["EQ"] = "eq";
    FilterType["NEQ"] = "neq";
    FilterType["IN"] = "in";
    FilterType["IN_ENUM"] = "in_enum";
    FilterType["ENUM"] = "enum";
    FilterType["GT"] = "gt";
    FilterType["LT"] = "lt";
    FilterType["GTE"] = "gte";
    FilterType["LTE"] = "lte";
    FilterType["ILIKE"] = "ilike";
    FilterType["RANGE"] = "range";
    FilterType["DATE"] = "date";
})(FilterType || (FilterType = {}));
class FilterAttribute {
    constructor(lbl, initVal) {
        this.lbl = lbl;
        this.val = null;
        if (initVal !== undefined) {
            this.val = initVal;
        }
    }
    description() {
        return '';
    }
    errors() {
        return [];
    }
    is(val) {
        return val === this.val;
    }
    label() {
        return this.lbl;
    }
    setValue(value) {
        this.val = value;
        return this;
    }
    value() {
        return this.val;
    }
}
class FilterItem {
    constructor(setting) {
        this.setting = setting;
        this.val = null;
        this.minVal = null;
        this.maxVal = null;
        this.enumVal = {};
    }
    getAlias() {
        try {
            return this.setting.alias;
        }
        catch (e) {
            return '';
        }
    }
    getName() {
        return this.setting.name;
    }
    getType() {
        return this.setting.type;
    }
    max() {
        if (this.maxVal === null) {
            this.maxVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === FilterType.DATE ? 0 : null);
        }
        return this.maxVal;
    }
    min() {
        if (this.minVal === null) {
            this.minVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === FilterType.DATE ? 0 : null);
        }
        return this.minVal;
    }
    vEnum(index) {
        if (!this.enumVal.hasOwnProperty(index)) {
            this.enumVal[index] = new FilterAttribute(this.setting.enum.getTextByKey(index), false);
        }
        return this.enumVal[index];
    }
    v() {
        if (this.val === null) {
            this.val = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name);
        }
        return this.val;
    }
    getEnum() {
        return this.setting.enum;
    }
    toResponse() {
        const result = {
            type: this.getType(),
            name: this.setting.name,
            alias: this.setting.alias,
        };
        switch (this.getType()) {
            case FilterType.IN_ENUM:
                result.val = Object
                    .keys(this.enumVal)
                    .map((key) => {
                    return {
                        key,
                        enabled: !!this.enumVal[key].value()
                    };
                })
                    .filter((i) => {
                    return i.enabled === true;
                })
                    .map((i) => {
                    console.log(i.key);
                    return this.setting.enum.enum[i.key];
                });
                break;
            case FilterType.DATE:
                result.minVal = this.min().value();
                result.maxVal = this.max().value();
                if (result.minVal !== 0 && (result.minVal === result.maxVal)) {
                    result.maxVal += (3600 * 24) - 1; // adding one day minus last second
                }
                /* if (this.min().value() < this.max().value()) {
                   result.minVal = this.min().value();
                   result.maxVal = this.max().value();
                 } else {
                   result.minVal = this.max().value();
                   result.maxVal = this.min().value();
                 }*/
                break;
            default:
                result.val = this.v().value();
                break;
        }
        return result;
    }
}
class FilterSettingsFactory {
    static fromSettings(...settings) {
        return settings.map((item) => {
            return new FilterItem(item);
        });
    }
    static fromSettingsAndModel(model, ...settings) {
        return settings.map((item) => {
            item.label = model.attribute(item.name).label();
            return new FilterItem(item);
        });
    }
}

class ServiceLocator {
    static http() {
        return ServiceLocator.injector.get(HttpClient);
    }
    static colQuery() {
        return ServiceLocator.injector.get(CollectionQuery$1);
    }
    /**
     * Provide data provider factory
     */
    static setDataProviderFactory(dpf) {
        ServiceLocator.dataProviderFactory = dpf;
    }
    /**
     * Provide instance of data provider
     */
    static getDataProviderFactory() {
        return ServiceLocator.dataProviderFactory;
    }
}

class CollectionQuery extends BaseQuery$1 {
    constructor() {
        super(...arguments);
        this.s = {};
        this.q = {};
        this._sort = {};
        this._query = {};
    }
    setTerm(term, fts = []) {
        try {
            this.q = JSON.parse(term);
            return this;
        }
        catch (e) {
        }
        if (fts.length === 0) {
            fts.push('name');
        }
        this.q = term ? fts.reduce((p, c) => {
            const t = {};
            t[c] = { '$regex': term, $options: 'i' };
            p.$or.push(t);
            return p;
        }, { $or: [] }) : {};
        return this;
    }
    // toJSON(): any {
    //     return {
    //         forGrid: true,
    //         jsonQuery: JSON.stringify(this.q),
    //         limit: this.limit(),
    //         skip: this.offset()
    //     };
    // }
    toJSON() {
        let settings = [];
        try {
            settings = this.filterSettings.toRequestJSON();
        }
        catch (e) {
        }
        return {
            filter: settings,
            limit: this.limit(),
            skip: this.offset(),
            sort: this.sort(),
        };
    }
    setFilter(filter) {
        this.filterSettings = filter;
        return this;
    }
    filter() {
        return this.q;
    }
    sort() {
        return this.s;
    }
    /**
     *
     * @param {string} attributeName
     * @param {string} direction
     * @returns {this}
     */
    setSort(attributeName, direction) {
        if ([SORT$1.ASC, SORT$1.DESC].indexOf(direction) === -1) {
            return this;
        }
        Object.keys(this.s).forEach((n) => {
            delete this.s[n];
        });
        switch (direction) {
            case SORT$1.ASC:
                this.s[attributeName] = 1;
                break;
            case SORT$1.DESC:
                this.s[attributeName] = -1;
        }
        return this;
    }
}
CollectionQuery.ɵfac = function CollectionQuery_Factory(t) { return ɵCollectionQuery_BaseFactory(t || CollectionQuery); };
CollectionQuery.ɵprov = ɵɵdefineInjectable({ token: CollectionQuery, factory: CollectionQuery.ɵfac });
const ɵCollectionQuery_BaseFactory = ɵɵgetInheritedFactory(CollectionQuery);
/*@__PURE__*/ (function () { ɵsetClassMetadata(CollectionQuery, [{
        type: Injectable
    }], null, null); })();

class ModelList {
    constructor() {
        this._list = [];
        this._em = new EventEmitter();
    }
    /**
     * @return EventEmitter<IModelListActionEvent>
     */
    em() {
        return this._em;
    }
    /**
     * Provide reference for list
     * @return {IModel[]}
     */
    list() {
        return this._list;
    }
}
ModelList.ɵfac = function ModelList_Factory(t) { return new (t || ModelList)(); };
ModelList.ɵprov = ɵɵdefineInjectable({ token: ModelList, factory: ModelList.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(ModelList, [{
        type: Injectable
    }], null, null); })();

/*
 * Public API Surface of core-ui
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ABaseModel, ACollection, AModel, ActionNames, Attribute, AutoAttribute, BaseCollection, BaseQuery, CollectionProviderDecorator, CollectionQuery, ContentTypeInterceptor, EnumModel, Filter, FilterItem, FilterSettingsFactory, FilterType, LangInterceptor, ModelList, ModelProviderDecorator, ReadOnlyAttribute, SORT, ServiceLocator, SimpleAttribute, SystemErrorInterceptor };
//# sourceMappingURL=core-ui.js.map
