import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var ContentTypeInterceptor = /** @class */ (function () {
    function ContentTypeInterceptor() {
    }
    ContentTypeInterceptor.prototype.intercept = function (req, next) {
        if (req.body instanceof FormData) {
            return next.handle(req);
        }
        var authReq = req.clone({
            headers: req.headers
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
        });
        return next.handle(authReq);
    };
    ContentTypeInterceptor.ɵfac = function ContentTypeInterceptor_Factory(t) { return new (t || ContentTypeInterceptor)(); };
    ContentTypeInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: ContentTypeInterceptor, factory: ContentTypeInterceptor.ɵfac });
    return ContentTypeInterceptor;
}());
export { ContentTypeInterceptor };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ContentTypeInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC10eXBlLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmNlcHRvcnMvY29udGVudC10eXBlLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7O0FBRXpDO0lBR0U7SUFDQSxDQUFDO0lBRUQsMENBQVMsR0FBVCxVQUFVLEdBQXFCLEVBQUUsSUFBaUI7UUFDaEQsSUFBSSxHQUFHLENBQUMsSUFBSSxZQUFZLFFBQVEsRUFBRTtZQUNoQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDekI7UUFDRCxJQUFNLE9BQU8sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3hCLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTztpQkFDakIsR0FBRyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQztpQkFDdkMsR0FBRyxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQztTQUNyQyxDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDOUIsQ0FBQztnR0FoQlUsc0JBQXNCO2tFQUF0QixzQkFBc0IsV0FBdEIsc0JBQXNCO2lDQU5uQztDQXdCQyxBQW5CRCxJQW1CQztTQWxCWSxzQkFBc0I7a0RBQXRCLHNCQUFzQjtjQURsQyxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtIdHRwRXZlbnQsIEh0dHBIYW5kbGVyLCBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXF1ZXN0fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQ29udGVudFR5cGVJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICBpZiAocmVxLmJvZHkgaW5zdGFuY2VvZiBGb3JtRGF0YSkge1xyXG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxKTtcclxuICAgIH1cclxuICAgIGNvbnN0IGF1dGhSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICBoZWFkZXJzOiByZXEuaGVhZGVyc1xyXG4gICAgICAgIC5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi9qc29uJylcclxuICAgICAgICAuc2V0KCdBY2NlcHQnLCAnYXBwbGljYXRpb24vanNvbicpXHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUoYXV0aFJlcSk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=