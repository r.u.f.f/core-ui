import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var LangInterceptor = /** @class */ (function () {
    function LangInterceptor() {
        // private langService: LangService
    }
    LangInterceptor.prototype.intercept = function (req, next) {
        var authReq = req.clone({
        // headers: req.headers.set(this.langService.getLangHeader(), this.langService.getLangCode())
        });
        return next.handle(authReq);
    };
    LangInterceptor.ɵfac = function LangInterceptor_Factory(t) { return new (t || LangInterceptor)(); };
    LangInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: LangInterceptor, factory: LangInterceptor.ɵfac });
    return LangInterceptor;
}());
export { LangInterceptor };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LangInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFuZy5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvaW50ZXJjZXB0b3JzL2xhbmcuaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0EsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFFekM7SUFHRTtRQUNFLG1DQUFtQztJQUNyQyxDQUFDO0lBRUQsbUNBQVMsR0FBVCxVQUFVLEdBQXFCLEVBQUUsSUFBaUI7UUFFaEQsSUFBTSxPQUFPLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztRQUN4Qiw2RkFBNkY7U0FDOUYsQ0FBQyxDQUFDO1FBRUgsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzlCLENBQUM7a0ZBYlUsZUFBZTsyREFBZixlQUFlLFdBQWYsZUFBZTswQkFONUI7Q0FxQkMsQUFoQkQsSUFnQkM7U0FmWSxlQUFlO2tEQUFmLGVBQWU7Y0FEM0IsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SHR0cEV2ZW50LCBIdHRwSGFuZGxlciwgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVxdWVzdH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIExhbmdJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgLy8gcHJpdmF0ZSBsYW5nU2VydmljZTogTGFuZ1NlcnZpY2VcclxuICB9XHJcblxyXG4gIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG5cclxuICAgIGNvbnN0IGF1dGhSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICAvLyBoZWFkZXJzOiByZXEuaGVhZGVycy5zZXQodGhpcy5sYW5nU2VydmljZS5nZXRMYW5nSGVhZGVyKCksIHRoaXMubGFuZ1NlcnZpY2UuZ2V0TGFuZ0NvZGUoKSlcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBuZXh0LmhhbmRsZShhdXRoUmVxKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==