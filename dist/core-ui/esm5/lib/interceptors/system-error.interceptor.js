import { Injectable } from '@angular/core';
import { HttpErrorResponse, } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
var SystemErrorInterceptor = /** @class */ (function () {
    function SystemErrorInterceptor() {
    }
    SystemErrorInterceptor.prototype.intercept = function (req, next) {
        return next.handle(req)
            .pipe(catchError(function (response) {
            if (response instanceof HttpErrorResponse) {
                var error = response.error;
                var systemError = error && error.errors ? error.errors.find(function (item) { return item.attribute === 'system'; }) : null;
                if (systemError) {
                    // systemError.messages.forEach((message => this.toast.danger(message)));
                }
            }
            return of(response);
        }));
    };
    SystemErrorInterceptor.ɵfac = function SystemErrorInterceptor_Factory(t) { return new (t || SystemErrorInterceptor)(); };
    SystemErrorInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: SystemErrorInterceptor, factory: SystemErrorInterceptor.ɵfac });
    return SystemErrorInterceptor;
}());
export { SystemErrorInterceptor };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SystemErrorInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLWVycm9yLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmNlcHRvcnMvc3lzdGVtLWVycm9yLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUNMLGlCQUFpQixHQUtsQixNQUFNLHNCQUFzQixDQUFDO0FBRTlCLE9BQU8sRUFBYSxFQUFFLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDcEMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztBQUUxQztJQUVFO0lBQ0EsQ0FBQztJQUVELDBDQUFTLEdBQVQsVUFBVSxHQUFxQixFQUFFLElBQWlCO1FBRWhELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7YUFDcEIsSUFBSSxDQUNILFVBQVUsQ0FBQyxVQUFDLFFBQWE7WUFDdkIsSUFBSSxRQUFRLFlBQVksaUJBQWlCLEVBQUU7Z0JBQ2xDLElBQUEsc0JBQUssQ0FBYTtnQkFFekIsSUFBTSxXQUFXLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFNBQVMsS0FBSyxRQUFRLEVBQTNCLENBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUUxRyxJQUFJLFdBQVcsRUFBRTtvQkFDZix5RUFBeUU7aUJBQzFFO2FBQ0Y7WUFFRCxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FDSCxDQUFDO0lBQ04sQ0FBQztnR0F0QlUsc0JBQXNCO2tFQUF0QixzQkFBc0IsV0FBdEIsc0JBQXNCO2lDQWJuQztDQW9DQyxBQXhCRCxJQXdCQztTQXZCWSxzQkFBc0I7a0RBQXRCLHNCQUFzQjtjQURsQyxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBIdHRwRXJyb3JSZXNwb25zZSxcclxuICBIdHRwRXZlbnQsXHJcbiAgSHR0cEhhbmRsZXIsXHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuXHJcbmltcG9ydCB7T2JzZXJ2YWJsZSwgb2Z9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQge2NhdGNoRXJyb3J9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN5c3RlbUVycm9ySW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcblxyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgY2F0Y2hFcnJvcigocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgSHR0cEVycm9yUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgY29uc3Qge2Vycm9yfSA9IHJlc3BvbnNlO1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgc3lzdGVtRXJyb3IgPSBlcnJvciAmJiBlcnJvci5lcnJvcnMgPyBlcnJvci5lcnJvcnMuZmluZChpdGVtID0+IGl0ZW0uYXR0cmlidXRlID09PSAnc3lzdGVtJykgOiBudWxsO1xyXG5cclxuICAgICAgICAgICAgaWYgKHN5c3RlbUVycm9yKSB7XHJcbiAgICAgICAgICAgICAgLy8gc3lzdGVtRXJyb3IubWVzc2FnZXMuZm9yRWFjaCgobWVzc2FnZSA9PiB0aGlzLnRvYXN0LmRhbmdlcihtZXNzYWdlKSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgcmV0dXJuIG9mKHJlc3BvbnNlKTtcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gIH1cclxufVxyXG4iXX0=