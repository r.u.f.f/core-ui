import { EventEmitter, Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var ModelList = /** @class */ (function () {
    function ModelList() {
        this._list = [];
        this._em = new EventEmitter();
    }
    /**
     * @return EventEmitter<IModelListActionEvent>
     */
    ModelList.prototype.em = function () {
        return this._em;
    };
    /**
     * Provide reference for list
     * @return {IModel[]}
     */
    ModelList.prototype.list = function () {
        return this._list;
    };
    ModelList.ɵfac = function ModelList_Factory(t) { return new (t || ModelList)(); };
    ModelList.ɵprov = i0.ɵɵdefineInjectable({ token: ModelList, factory: ModelList.ɵfac });
    return ModelList;
}());
export { ModelList };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ModelList, [{
        type: Injectable
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwtbGlzdC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvcHJvdmlkZXJzL21vZGVsLWxpc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFlBQVksRUFBRSxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7O0FBUXZEO0lBQUE7UUFHVSxVQUFLLEdBQWEsRUFBRSxDQUFDO1FBQ3JCLFFBQUcsR0FBd0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztLQWdCdkU7SUFkQzs7T0FFRztJQUNJLHNCQUFFLEdBQVQ7UUFDRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDbEIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHdCQUFJLEdBQVg7UUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEIsQ0FBQztzRUFsQlUsU0FBUztxREFBVCxTQUFTLFdBQVQsU0FBUztvQkFUdEI7Q0E0QkMsQUFwQkQsSUFvQkM7U0FuQlksU0FBUztrREFBVCxTQUFTO2NBRHJCLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0V2ZW50RW1pdHRlciwgSW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SU1vZGVsfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvbW9kZWxzJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSU1vZGVsTGlzdEFjdGlvbkV2ZW50IHtcclxuICBuYW1lOiBzdHJpbmc7XHJcbiAgcmVzdWx0OiBib29sZWFuO1xyXG59XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBNb2RlbExpc3Qge1xyXG5cclxuICBwcml2YXRlIF9saXN0OiBJTW9kZWxbXSA9IFtdO1xyXG4gIHByaXZhdGUgX2VtOiBFdmVudEVtaXR0ZXI8SU1vZGVsTGlzdEFjdGlvbkV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQHJldHVybiBFdmVudEVtaXR0ZXI8SU1vZGVsTGlzdEFjdGlvbkV2ZW50PlxyXG4gICAqL1xyXG4gIHB1YmxpYyBlbSgpIHtcclxuICAgIHJldHVybiB0aGlzLl9lbTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFByb3ZpZGUgcmVmZXJlbmNlIGZvciBsaXN0XHJcbiAgICogQHJldHVybiB7SU1vZGVsW119XHJcbiAgICovXHJcbiAgcHVibGljIGxpc3QoKTogSU1vZGVsW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2xpc3Q7XHJcbiAgfVxyXG59XHJcbiJdfQ==