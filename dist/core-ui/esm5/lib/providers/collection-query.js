import { __extends } from "tslib";
import { Injectable } from '@angular/core';
import { SORT } from 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery';
import { BaseQuery } from 'projects/core-ui/src/lib/data-structures/queries';
import * as i0 from "@angular/core";
var CollectionQuery = /** @class */ (function (_super) {
    __extends(CollectionQuery, _super);
    function CollectionQuery() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.s = {};
        _this.q = {};
        _this._sort = {};
        _this._query = {};
        return _this;
    }
    CollectionQuery.prototype.setTerm = function (term, fts) {
        if (fts === void 0) { fts = []; }
        try {
            this.q = JSON.parse(term);
            return this;
        }
        catch (e) {
        }
        if (fts.length === 0) {
            fts.push('name');
        }
        this.q = term ? fts.reduce(function (p, c) {
            var t = {};
            t[c] = { '$regex': term, $options: 'i' };
            p.$or.push(t);
            return p;
        }, { $or: [] }) : {};
        return this;
    };
    // toJSON(): any {
    //     return {
    //         forGrid: true,
    //         jsonQuery: JSON.stringify(this.q),
    //         limit: this.limit(),
    //         skip: this.offset()
    //     };
    // }
    CollectionQuery.prototype.toJSON = function () {
        var settings = [];
        try {
            settings = this.filterSettings.toRequestJSON();
        }
        catch (e) {
        }
        return {
            filter: settings,
            limit: this.limit(),
            skip: this.offset(),
            sort: this.sort(),
        };
    };
    CollectionQuery.prototype.setFilter = function (filter) {
        this.filterSettings = filter;
        return this;
    };
    CollectionQuery.prototype.filter = function () {
        return this.q;
    };
    CollectionQuery.prototype.sort = function () {
        return this.s;
    };
    /**
     *
     * @param {string} attributeName
     * @param {string} direction
     * @returns {this}
     */
    CollectionQuery.prototype.setSort = function (attributeName, direction) {
        var _this = this;
        if ([SORT.ASC, SORT.DESC].indexOf(direction) === -1) {
            return this;
        }
        Object.keys(this.s).forEach(function (n) {
            delete _this.s[n];
        });
        switch (direction) {
            case SORT.ASC:
                this.s[attributeName] = 1;
                break;
            case SORT.DESC:
                this.s[attributeName] = -1;
        }
        return this;
    };
    CollectionQuery.ɵfac = function CollectionQuery_Factory(t) { return ɵCollectionQuery_BaseFactory(t || CollectionQuery); };
    CollectionQuery.ɵprov = i0.ɵɵdefineInjectable({ token: CollectionQuery, factory: CollectionQuery.ɵfac });
    return CollectionQuery;
}(BaseQuery));
export { CollectionQuery };
var ɵCollectionQuery_BaseFactory = i0.ɵɵgetInheritedFactory(CollectionQuery);
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CollectionQuery, [{
        type: Injectable
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi1xdWVyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvcHJvdmlkZXJzL2NvbGxlY3Rpb24tcXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFtQixJQUFJLEVBQUMsTUFBTSw4RUFBOEUsQ0FBQztBQUVwSCxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sa0RBQWtELENBQUM7O0FBRTNFO0lBQ3FDLG1DQUFTO0lBRDlDO1FBQUEscUVBNkZDO1FBM0ZTLE9BQUMsR0FBd0MsRUFBRSxDQUFDO1FBRTVDLE9BQUMsR0FBRyxFQUFFLENBQUM7UUFJUCxXQUFLLEdBQXdDLEVBQUUsQ0FBQztRQUVoRCxZQUFNLEdBQUcsRUFBRSxDQUFDOztLQW1GckI7SUFqRkMsaUNBQU8sR0FBUCxVQUFRLElBQVksRUFBRSxHQUFrQjtRQUFsQixvQkFBQSxFQUFBLFFBQWtCO1FBQ3RDLElBQUk7WUFDRixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1NBRVg7UUFFRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbEI7UUFDRCxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO1lBQzlCLElBQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNiLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2QsT0FBTyxDQUFDLENBQUM7UUFDWCxDQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ25CLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLDZDQUE2QztJQUM3QywrQkFBK0I7SUFDL0IsOEJBQThCO0lBQzlCLFNBQVM7SUFDVCxJQUFJO0lBQ0osZ0NBQU0sR0FBTjtRQUVFLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFJO1lBQ0YsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDaEQ7UUFBQyxPQUFPLENBQUMsRUFBRTtTQUVYO1FBQ0QsT0FBTztZQUNMLE1BQU0sRUFBRSxRQUFRO1lBQ2hCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFO1NBQ2xCLENBQUM7SUFDSixDQUFDO0lBRU0sbUNBQVMsR0FBaEIsVUFBaUIsTUFBZTtRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQztRQUM3QixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxnQ0FBTSxHQUFiO1FBQ0UsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFFTSw4QkFBSSxHQUFYO1FBQ0UsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLGlDQUFPLEdBQWQsVUFBZSxhQUFxQixFQUFFLFNBQWlCO1FBQXZELGlCQWdCQztRQWZDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDbkQsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQVM7WUFDcEMsT0FBTyxLQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxTQUFTLEVBQUU7WUFDakIsS0FBSyxJQUFJLENBQUMsR0FBRztnQkFDWCxJQUFJLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDMUIsTUFBTTtZQUNSLEtBQUssSUFBSSxDQUFDLElBQUk7Z0JBQ1osSUFBSSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUM5QjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzswR0ExRlUsZUFBZTsyREFBZixlQUFlLFdBQWYsZUFBZTswQkFONUI7Q0FrR0MsQUE3RkQsQ0FDcUMsU0FBUyxHQTRGN0M7U0E1RlksZUFBZTs0REFBZixlQUFlO2tEQUFmLGVBQWU7Y0FEM0IsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SUNvbGxlY3Rpb25RdWVyeSwgU09SVH0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL3F1ZXJpZXMvSUNvbGxlY3Rpb25RdWVyeSc7XHJcbmltcG9ydCB7SUZpbHRlcn0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2RhdGEtc3RydWN0dXJlcy9xdWVyaWVzL2ZpbHRlcic7XHJcbmltcG9ydCB7QmFzZVF1ZXJ5fSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvZGF0YS1zdHJ1Y3R1cmVzL3F1ZXJpZXMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQ29sbGVjdGlvblF1ZXJ5IGV4dGVuZHMgQmFzZVF1ZXJ5IGltcGxlbWVudHMgSUNvbGxlY3Rpb25RdWVyeSB7XHJcbiAgcHJpdmF0ZSBzOiB7IFthdHRyaWJ1dGVOYW1lOiBzdHJpbmddOiBudW1iZXIgfSA9IHt9O1xyXG5cclxuICBwcml2YXRlIHEgPSB7fTtcclxuXHJcbiAgcHJpdmF0ZSBmaWx0ZXJTZXR0aW5nczogSUZpbHRlcjtcclxuXHJcbiAgcHJpdmF0ZSBfc29ydDogeyBbYXR0cmlidXRlTmFtZTogc3RyaW5nXTogbnVtYmVyIH0gPSB7fTtcclxuXHJcbiAgcHJpdmF0ZSBfcXVlcnkgPSB7fTtcclxuXHJcbiAgc2V0VGVybSh0ZXJtOiBzdHJpbmcsIGZ0czogc3RyaW5nW10gPSBbXSk6IHRoaXMge1xyXG4gICAgdHJ5IHtcclxuICAgICAgdGhpcy5xID0gSlNPTi5wYXJzZSh0ZXJtKTtcclxuICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGlmIChmdHMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgIGZ0cy5wdXNoKCduYW1lJyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnEgPSB0ZXJtID8gZnRzLnJlZHVjZSgocCwgYykgPT4ge1xyXG4gICAgICBjb25zdCB0ID0ge307XHJcbiAgICAgIHRbY10gPSB7JyRyZWdleCc6IHRlcm0sICRvcHRpb25zOiAnaSd9O1xyXG4gICAgICBwLiRvci5wdXNoKHQpO1xyXG4gICAgICByZXR1cm4gcDtcclxuICAgIH0sIHskb3I6IFtdfSkgOiB7fTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLy8gdG9KU09OKCk6IGFueSB7XHJcbiAgLy8gICAgIHJldHVybiB7XHJcbiAgLy8gICAgICAgICBmb3JHcmlkOiB0cnVlLFxyXG4gIC8vICAgICAgICAganNvblF1ZXJ5OiBKU09OLnN0cmluZ2lmeSh0aGlzLnEpLFxyXG4gIC8vICAgICAgICAgbGltaXQ6IHRoaXMubGltaXQoKSxcclxuICAvLyAgICAgICAgIHNraXA6IHRoaXMub2Zmc2V0KClcclxuICAvLyAgICAgfTtcclxuICAvLyB9XHJcbiAgdG9KU09OKCk6IGFueSB7XHJcblxyXG4gICAgbGV0IHNldHRpbmdzID0gW107XHJcbiAgICB0cnkge1xyXG4gICAgICBzZXR0aW5ncyA9IHRoaXMuZmlsdGVyU2V0dGluZ3MudG9SZXF1ZXN0SlNPTigpO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG5cclxuICAgIH1cclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlcjogc2V0dGluZ3MsXHJcbiAgICAgIGxpbWl0OiB0aGlzLmxpbWl0KCksXHJcbiAgICAgIHNraXA6IHRoaXMub2Zmc2V0KCksXHJcbiAgICAgIHNvcnQ6IHRoaXMuc29ydCgpLFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRGaWx0ZXIoZmlsdGVyOiBJRmlsdGVyKTogdGhpcyB7XHJcbiAgICB0aGlzLmZpbHRlclNldHRpbmdzID0gZmlsdGVyO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZmlsdGVyKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5xO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHNvcnQoKTogeyBbcDogc3RyaW5nXTogbnVtYmVyIH0ge1xyXG4gICAgcmV0dXJuIHRoaXMucztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtzdHJpbmd9IGF0dHJpYnV0ZU5hbWVcclxuICAgKiBAcGFyYW0ge3N0cmluZ30gZGlyZWN0aW9uXHJcbiAgICogQHJldHVybnMge3RoaXN9XHJcbiAgICovXHJcbiAgcHVibGljIHNldFNvcnQoYXR0cmlidXRlTmFtZTogc3RyaW5nLCBkaXJlY3Rpb246IHN0cmluZyk6IHRoaXMge1xyXG4gICAgaWYgKFtTT1JULkFTQywgU09SVC5ERVNDXS5pbmRleE9mKGRpcmVjdGlvbikgPT09IC0xKSB7XHJcbiAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG4gICAgT2JqZWN0LmtleXModGhpcy5zKS5mb3JFYWNoKChuOiBzdHJpbmcpID0+IHtcclxuICAgICAgZGVsZXRlIHRoaXMuc1tuXTtcclxuICAgIH0pO1xyXG4gICAgc3dpdGNoIChkaXJlY3Rpb24pIHtcclxuICAgICAgY2FzZSBTT1JULkFTQzpcclxuICAgICAgICB0aGlzLnNbYXR0cmlidXRlTmFtZV0gPSAxO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIFNPUlQuREVTQzpcclxuICAgICAgICB0aGlzLnNbYXR0cmlidXRlTmFtZV0gPSAtMTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG59XHJcbiJdfQ==