var SimpleAttribute = /** @class */ (function () {
    function SimpleAttribute(_val, _label, _description) {
        if (_description === void 0) { _description = ''; }
        this._val = _val;
        this._label = _label;
        this._description = _description;
    }
    SimpleAttribute.prototype.value = function () {
        return this._val;
    };
    SimpleAttribute.prototype.setValue = function (value) {
        this._val = value;
        return this;
    };
    SimpleAttribute.prototype.label = function () {
        return this._label;
    };
    SimpleAttribute.prototype.description = function () {
        return this._description;
    };
    SimpleAttribute.prototype.errors = function () {
        return [];
    };
    SimpleAttribute.prototype.is = function (val) {
        return this._val === val;
    };
    return SimpleAttribute;
}());
export { SimpleAttribute };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2ltcGxlLWF0dHJpYnV0ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMvc2ltcGxlLWF0dHJpYnV0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtJQUVFLHlCQUFvQixJQUFTLEVBQ1QsTUFBYyxFQUNkLFlBQXlCO1FBQXpCLDZCQUFBLEVBQUEsaUJBQXlCO1FBRnpCLFNBQUksR0FBSixJQUFJLENBQUs7UUFDVCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsaUJBQVksR0FBWixZQUFZLENBQWE7SUFFN0MsQ0FBQztJQUVELCtCQUFLLEdBQUw7UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbkIsQ0FBQztJQUVELGtDQUFRLEdBQVIsVUFBUyxLQUFVO1FBQ2pCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELCtCQUFLLEdBQUw7UUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDckIsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQztJQUVELGdDQUFNLEdBQU47UUFDRSxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCw0QkFBRSxHQUFGLFVBQUcsR0FBUTtRQUNULE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxHQUFHLENBQUM7SUFDM0IsQ0FBQztJQUVILHNCQUFDO0FBQUQsQ0FBQyxBQWpDRCxJQWlDQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SUF0dHJpYnV0ZX0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFNpbXBsZUF0dHJpYnV0ZSBpbXBsZW1lbnRzIElBdHRyaWJ1dGUge1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF92YWw6IGFueSxcclxuICAgICAgICAgICAgICBwcml2YXRlIF9sYWJlbDogc3RyaW5nLFxyXG4gICAgICAgICAgICAgIHByaXZhdGUgX2Rlc2NyaXB0aW9uOiBzdHJpbmcgPSAnJykge1xyXG5cclxuICB9XHJcblxyXG4gIHZhbHVlKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5fdmFsO1xyXG4gIH1cclxuXHJcbiAgc2V0VmFsdWUodmFsdWU6IGFueSk6IHRoaXMge1xyXG4gICAgdGhpcy5fdmFsID0gdmFsdWU7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIGxhYmVsKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fbGFiZWw7XHJcbiAgfVxyXG5cclxuICBkZXNjcmlwdGlvbigpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMuX2Rlc2NyaXB0aW9uO1xyXG4gIH1cclxuXHJcbiAgZXJyb3JzKCk6IEFycmF5PHN0cmluZz4ge1xyXG4gICAgcmV0dXJuIFtdO1xyXG4gIH1cclxuXHJcbiAgaXModmFsOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl92YWwgPT09IHZhbDtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==