var ReadOnlyAttribute = /** @class */ (function () {
    function ReadOnlyAttribute(lbl, val) {
        this.lbl = lbl;
        this.val = val;
        this.errs = [];
    }
    ReadOnlyAttribute.prototype.label = function () {
        return this.lbl;
    };
    ReadOnlyAttribute.prototype.setValue = function (value) {
        return this;
    };
    ReadOnlyAttribute.prototype.value = function () {
        return this.val;
    };
    ReadOnlyAttribute.prototype.errors = function () {
        return this.errs;
    };
    ReadOnlyAttribute.prototype.is = function (val) {
        return this.value() === val;
    };
    ReadOnlyAttribute.prototype.description = function () {
        return '';
    };
    return ReadOnlyAttribute;
}());
export { ReadOnlyAttribute };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVhZC1vbmx5LWF0dHJpYnV0ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMvcmVhZC1vbmx5LWF0dHJpYnV0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtJQUlFLDJCQUFzQixHQUFXLEVBQVksR0FBUTtRQUEvQixRQUFHLEdBQUgsR0FBRyxDQUFRO1FBQVksUUFBRyxHQUFILEdBQUcsQ0FBSztRQUY3QyxTQUFJLEdBQUcsRUFBRSxDQUFDO0lBSWxCLENBQUM7SUFFRCxpQ0FBSyxHQUFMO1FBQ0UsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xCLENBQUM7SUFFRCxvQ0FBUSxHQUFSLFVBQVMsS0FBVTtRQUNqQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxpQ0FBSyxHQUFMO1FBQ0UsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xCLENBQUM7SUFFRCxrQ0FBTSxHQUFOO1FBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ25CLENBQUM7SUFFRCw4QkFBRSxHQUFGLFVBQUcsR0FBUTtRQUNULE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEdBQUcsQ0FBQztJQUM5QixDQUFDO0lBRU0sdUNBQVcsR0FBbEI7UUFDRSxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFSCx3QkFBQztBQUFELENBQUMsQUFoQ0QsSUFnQ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lBdHRyaWJ1dGV9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9hdHRyaWJ1dGVzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBSZWFkT25seUF0dHJpYnV0ZSBpbXBsZW1lbnRzIElBdHRyaWJ1dGUge1xyXG5cclxuICBwcml2YXRlIGVycnMgPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGxibDogc3RyaW5nLCBwcm90ZWN0ZWQgdmFsOiBhbnkpIHtcclxuXHJcbiAgfVxyXG5cclxuICBsYWJlbCgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMubGJsO1xyXG4gIH1cclxuXHJcbiAgc2V0VmFsdWUodmFsdWU6IGFueSk6IHRoaXMge1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICB2YWx1ZSgpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMudmFsO1xyXG4gIH1cclxuXHJcbiAgZXJyb3JzKCk6IEFycmF5PHN0cmluZz4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZXJycztcclxuICB9XHJcblxyXG4gIGlzKHZhbDogYW55KTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy52YWx1ZSgpID09PSB2YWw7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZGVzY3JpcHRpb24oKTogc3RyaW5nIHtcclxuICAgIHJldHVybiAnJztcclxuICB9XHJcblxyXG59XHJcbiJdfQ==