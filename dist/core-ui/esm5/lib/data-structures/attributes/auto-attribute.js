import { __extends } from "tslib";
import { Attribute } from 'projects/core-ui/src/lib/data-structures/attributes/attribute';
var AutoAttribute = /** @class */ (function (_super) {
    __extends(AutoAttribute, _super);
    function AutoAttribute(labels, attributes, name, model) {
        var _this = _super.call(this) || this;
        _this.labels = labels;
        _this.attributes = attributes;
        _this.name = name;
        _this.model = model;
        if (!_this.model.schema()[_this.name]) {
            throw new Error("Incorrect implementation. Model " + _this.model.constructor.name + " have no attribute [" + _this.name + "]!");
        }
        return _this;
    }
    AutoAttribute.prototype.setValue = function (value) {
        this.attributes[this.name] = value;
        this.model.onAttributesChanged().emit(this.name);
        return this;
    };
    AutoAttribute.prototype.value = function () {
        return this.attributes[this.name];
    };
    AutoAttribute.prototype.valueByKey = function (key) {
        return this.attributes[this.name][key];
    };
    AutoAttribute.prototype.label = function () {
        return this.labels[this.name];
    };
    AutoAttribute.prototype.errors = function () {
        return this.model.errors(this.name);
    };
    AutoAttribute.prototype.is = function (val) {
        return this.attributes[this.name] === val;
    };
    AutoAttribute.prototype.getName = function () {
        return this.name;
    };
    AutoAttribute.prototype.description = function () {
        return this.model.schema()[this.name].$d ? this.model.schema()[this.name].$d : '';
    };
    return AutoAttribute;
}(Attribute));
export { AutoAttribute };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0by1hdHRyaWJ1dGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLXVpLyIsInNvdXJjZXMiOlsibGliL2RhdGEtc3RydWN0dXJlcy9hdHRyaWJ1dGVzL2F1dG8tYXR0cmlidXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sK0RBQStELENBQUM7QUFHeEY7SUFBbUMsaUNBQVM7SUFFMUMsdUJBQW9CLE1BQTRCLEVBQzVCLFVBQWdDLEVBQ2hDLElBQUksRUFDSixLQUFpQjtRQUhyQyxZQUlFLGlCQUFPLFNBS1I7UUFUbUIsWUFBTSxHQUFOLE1BQU0sQ0FBc0I7UUFDNUIsZ0JBQVUsR0FBVixVQUFVLENBQXNCO1FBQ2hDLFVBQUksR0FBSixJQUFJLENBQUE7UUFDSixXQUFLLEdBQUwsS0FBSyxDQUFZO1FBR25DLElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNuQyxNQUFNLElBQUksS0FBSyxDQUFDLHFDQUFtQyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLDRCQUF1QixLQUFJLENBQUMsSUFBSSxPQUFJLENBQUMsQ0FBQztTQUNySDs7SUFDSCxDQUFDO0lBRUQsZ0NBQVEsR0FBUixVQUFTLEtBQVU7UUFDakIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELDZCQUFLLEdBQUw7UUFDRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxrQ0FBVSxHQUFWLFVBQVcsR0FBVztRQUNwQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCw2QkFBSyxHQUFMO1FBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsOEJBQU0sR0FBTjtRQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFRCwwQkFBRSxHQUFGLFVBQUcsR0FBUTtRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDO0lBQzVDLENBQUM7SUFFTSwrQkFBTyxHQUFkO1FBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ25CLENBQUM7SUFFTSxtQ0FBVyxHQUFsQjtRQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUNwRixDQUFDO0lBQ0gsb0JBQUM7QUFBRCxDQUFDLEFBOUNELENBQW1DLFNBQVMsR0E4QzNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBdHRyaWJ1dGV9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9kYXRhLXN0cnVjdHVyZXMvYXR0cmlidXRlcy9hdHRyaWJ1dGUnO1xyXG5pbXBvcnQge0lCYXNlTW9kZWx9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMvSUJhc2VNb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgQXV0b0F0dHJpYnV0ZSBleHRlbmRzIEF0dHJpYnV0ZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbGFiZWxzOiB7IFtzOiBzdHJpbmddOiBhbnkgfSxcclxuICAgICAgICAgICAgICBwcml2YXRlIGF0dHJpYnV0ZXM6IHsgW3M6IHN0cmluZ106IGFueSB9LFxyXG4gICAgICAgICAgICAgIHByaXZhdGUgbmFtZSxcclxuICAgICAgICAgICAgICBwcml2YXRlIG1vZGVsOiBJQmFzZU1vZGVsKSB7XHJcbiAgICBzdXBlcigpO1xyXG5cclxuICAgIGlmICghdGhpcy5tb2RlbC5zY2hlbWEoKVt0aGlzLm5hbWVdKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihgSW5jb3JyZWN0IGltcGxlbWVudGF0aW9uLiBNb2RlbCAke3RoaXMubW9kZWwuY29uc3RydWN0b3IubmFtZX0gaGF2ZSBubyBhdHRyaWJ1dGUgWyR7dGhpcy5uYW1lfV0hYCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRWYWx1ZSh2YWx1ZTogYW55KTogdGhpcyB7XHJcbiAgICB0aGlzLmF0dHJpYnV0ZXNbdGhpcy5uYW1lXSA9IHZhbHVlO1xyXG4gICAgdGhpcy5tb2RlbC5vbkF0dHJpYnV0ZXNDaGFuZ2VkKCkuZW1pdCh0aGlzLm5hbWUpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICB2YWx1ZSgpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuYXR0cmlidXRlc1t0aGlzLm5hbWVdO1xyXG4gIH1cclxuXHJcbiAgdmFsdWVCeUtleShrZXk6IHN0cmluZyk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5hdHRyaWJ1dGVzW3RoaXMubmFtZV1ba2V5XTtcclxuICB9XHJcblxyXG4gIGxhYmVsKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5sYWJlbHNbdGhpcy5uYW1lXTtcclxuICB9XHJcblxyXG4gIGVycm9ycygpOiBBcnJheTxzdHJpbmc+IHtcclxuICAgIHJldHVybiB0aGlzLm1vZGVsLmVycm9ycyh0aGlzLm5hbWUpO1xyXG4gIH1cclxuXHJcbiAgaXModmFsOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZXNbdGhpcy5uYW1lXSA9PT0gdmFsO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldE5hbWUoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZGVzY3JpcHRpb24oKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLm1vZGVsLnNjaGVtYSgpW3RoaXMubmFtZV0uJGQgPyB0aGlzLm1vZGVsLnNjaGVtYSgpW3RoaXMubmFtZV0uJGQgOiAnJztcclxuICB9XHJcbn1cclxuIl19