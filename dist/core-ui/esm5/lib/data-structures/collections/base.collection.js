var BaseCollection = /** @class */ (function () {
    function BaseCollection() {
        this._data = [];
    }
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.count = function () {
        return this._data.length;
    };
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.data = function () {
        return this._data;
    };
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.add = function (model) {
        this._data.push(model);
        return this;
    };
    /**
     * @inheritDoc
     */
    BaseCollection.prototype.clear = function () {
        this._data.splice(0, this.count());
        return this;
    };
    return BaseCollection;
}());
export { BaseCollection };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5jb2xsZWN0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9kYXRhLXN0cnVjdHVyZXMvY29sbGVjdGlvbnMvYmFzZS5jb2xsZWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0lBQUE7UUFFVSxVQUFLLEdBQXNCLEVBQUUsQ0FBQztJQXFDeEMsQ0FBQztJQW5DQzs7T0FFRztJQUNILDhCQUFLLEdBQUw7UUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzNCLENBQUM7SUFHRDs7T0FFRztJQUNILDZCQUFJLEdBQUo7UUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEIsQ0FBQztJQU9EOztPQUVHO0lBQ0gsNEJBQUcsR0FBSCxVQUFJLEtBQWlCO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsOEJBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUNuQyxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFDSCxxQkFBQztBQUFELENBQUMsQUF2Q0QsSUF1Q0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lCYXNlQ29sbGVjdGlvbn0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHtJQmFzZU1vZGVsfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvbW9kZWxzJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlQ29sbGVjdGlvbiBpbXBsZW1lbnRzIElCYXNlQ29sbGVjdGlvbiB7XHJcblxyXG4gIHByaXZhdGUgX2RhdGE6IEFycmF5PElCYXNlTW9kZWw+ID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgY291bnQoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9kYXRhLmxlbmd0aDtcclxuICB9XHJcblxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGRhdGEoKTogQXJyYXk8SUJhc2VNb2RlbD4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2RhdGE7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGFic3RyYWN0IG1vZGVsKCk6IElCYXNlTW9kZWw7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgYWRkKG1vZGVsOiBJQmFzZU1vZGVsKTogdGhpcyB7XHJcbiAgICB0aGlzLl9kYXRhLnB1c2gobW9kZWwpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGNsZWFyKCk6IHRoaXMge1xyXG4gICAgdGhpcy5fZGF0YS5zcGxpY2UoMCwgdGhpcy5jb3VudCgpKTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxufVxyXG4iXX0=