import { __extends } from "tslib";
import { BaseCollection } from 'projects/core-ui/src/lib/data-structures/collections/base.collection';
var ACollection = /** @class */ (function (_super) {
    __extends(ACollection, _super);
    function ACollection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @inheritDoc
     */
    ACollection.prototype.dataProvider = function () {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    };
    /**
     *
     * @returns {string[]}
     */
    ACollection.prototype.fullTextSearchKeys = function () {
        return [];
    };
    /**
     * @inheritDoc
     */
    ACollection.prototype.filter = function () {
        return [];
    };
    /**
     * Provide list of keys that able to be sorted
     * @returns {string[]}
     */
    ACollection.prototype.sortKeys = function () {
        return [];
    };
    return ACollection;
}(BaseCollection));
export { ACollection };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL2NvbGxlY3Rpb25zL2NvbGxlY3Rpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxzRUFBc0UsQ0FBQztBQUtwRztJQUEwQywrQkFBYztJQUF4RDs7SUFpQ0EsQ0FBQztJQWhDQzs7T0FFRztJQUNILGtDQUFZLEdBQVo7UUFDRSxJQUFJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLFFBQVEsRUFBRTtZQUNuQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksd0NBQWtCLEdBQXpCO1FBQ0UsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRUQ7O09BRUc7SUFDSSw0QkFBTSxHQUFiO1FBQ0UsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksOEJBQVEsR0FBZjtRQUNFLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUNILGtCQUFDO0FBQUQsQ0FBQyxBQWpDRCxDQUEwQyxjQUFjLEdBaUN2RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QmFzZUNvbGxlY3Rpb259IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9kYXRhLXN0cnVjdHVyZXMvY29sbGVjdGlvbnMvYmFzZS5jb2xsZWN0aW9uJztcclxuaW1wb3J0IHtJQ29sbGVjdGlvbn0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHtJQ29sbGVjdGlvbkRhdGFQcm92aWRlcn0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1wcm92aWRlcic7XHJcbmltcG9ydCB7SUZpbHRlclNldHRpbmdzfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvcXVlcmllcyc7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQUNvbGxlY3Rpb24gZXh0ZW5kcyBCYXNlQ29sbGVjdGlvbiBpbXBsZW1lbnRzIElDb2xsZWN0aW9uIHtcclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGRhdGFQcm92aWRlcigpOiBJQ29sbGVjdGlvbkRhdGFQcm92aWRlciB7XHJcbiAgICBpZiAodHlwZW9mIHRoaXNbJyRkcCddID09PSAnb2JqZWN0Jykge1xyXG4gICAgICByZXR1cm4gdGhpc1snJGRwJ107XHJcbiAgICB9XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBzaG91bGQgdXNlIGFueSBvZiBkYXRhIHByb3ZpZGVyIGRlY29yYXRvcnMnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqXHJcbiAgICogQHJldHVybnMge3N0cmluZ1tdfVxyXG4gICAqL1xyXG4gIHB1YmxpYyBmdWxsVGV4dFNlYXJjaEtleXMoKTogc3RyaW5nW10ge1xyXG4gICAgcmV0dXJuIFtdO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBwdWJsaWMgZmlsdGVyKCk6IElGaWx0ZXJTZXR0aW5nc1tdIHtcclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFByb3ZpZGUgbGlzdCBvZiBrZXlzIHRoYXQgYWJsZSB0byBiZSBzb3J0ZWRcclxuICAgKiBAcmV0dXJucyB7c3RyaW5nW119XHJcbiAgICovXHJcbiAgcHVibGljIHNvcnRLZXlzKCk6IHN0cmluZ1tdIHtcclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcbn1cclxuIl19