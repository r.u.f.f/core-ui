var BaseQuery = /** @class */ (function () {
    function BaseQuery() {
        this._limit = 20;
        this._offset = 0;
    }
    BaseQuery.prototype.limit = function () {
        return this._limit;
    };
    BaseQuery.prototype.offset = function () {
        return this._offset;
    };
    BaseQuery.prototype.toJSON = function () {
        return {
            forGrid: true,
            limit: this.limit(),
            skip: this.offset()
        };
    };
    BaseQuery.prototype.setLimit = function (limit) {
        this._limit = limit;
        return this;
    };
    BaseQuery.prototype.setOffset = function (offset) {
        this._offset = offset;
        return this;
    };
    return BaseQuery;
}());
export { BaseQuery };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1xdWVyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL3F1ZXJpZXMvYmFzZS1xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUFBO1FBQ1ksV0FBTSxHQUFHLEVBQUUsQ0FBQztRQUNaLFlBQU8sR0FBRyxDQUFDLENBQUM7SUE0QnhCLENBQUM7SUExQkMseUJBQUssR0FBTDtRQUNFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBRUQsMEJBQU0sR0FBTjtRQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN0QixDQUFDO0lBRUQsMEJBQU0sR0FBTjtRQUNFLE9BQU87WUFDTCxPQUFPLEVBQUUsSUFBSTtZQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFO1NBQ3BCLENBQUM7SUFDSixDQUFDO0lBRUQsNEJBQVEsR0FBUixVQUFTLEtBQWE7UUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsNkJBQVMsR0FBVCxVQUFVLE1BQWM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUgsZ0JBQUM7QUFBRCxDQUFDLEFBOUJELElBOEJDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEJhc2VRdWVyeSB7XHJcbiAgcHJvdGVjdGVkIF9saW1pdCA9IDIwO1xyXG4gIHByb3RlY3RlZCBfb2Zmc2V0ID0gMDtcclxuXHJcbiAgbGltaXQoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9saW1pdDtcclxuICB9XHJcblxyXG4gIG9mZnNldCgpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRoaXMuX29mZnNldDtcclxuICB9XHJcblxyXG4gIHRvSlNPTigpOiBhbnkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZm9yR3JpZDogdHJ1ZSxcclxuICAgICAgbGltaXQ6IHRoaXMubGltaXQoKSxcclxuICAgICAgc2tpcDogdGhpcy5vZmZzZXQoKVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHNldExpbWl0KGxpbWl0OiBudW1iZXIpOiB0aGlzIHtcclxuICAgIHRoaXMuX2xpbWl0ID0gbGltaXQ7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIHNldE9mZnNldChvZmZzZXQ6IG51bWJlcik6IHRoaXMge1xyXG4gICAgdGhpcy5fb2Zmc2V0ID0gb2Zmc2V0O1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=