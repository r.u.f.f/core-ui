import { __read, __spread } from "tslib";
import { EventEmitter } from '@angular/core';
import { validate } from 'validate.js';
import { AutoAttribute } from 'projects/core-ui/src/lib/data-structures/attributes';
var ABaseModel = /** @class */ (function () {
    function ABaseModel() {
        this.ALLOW_NULL_VALUES = true;
        /**
         * Internal dictionary for keeping attributes
         * @private
         */
        this.attributes = {};
        this.enums = {};
        this.editMode = false;
        this.$services = {
            $onAttributesChanged: new EventEmitter(),
            $nullValuesMap: {
                string: '',
                number: 0,
                boolean: false,
                bool: false,
                array: [],
                date_int_ms: new Date(),
            },
            $errors: {},
            $scenario: {
                active: {},
                list: [],
            }
        };
        this.initEmptyAttributes();
    }
    ABaseModel.prototype.startEdit = function () {
        this.editMode = true;
        return this;
    };
    ABaseModel.prototype.finishEdit = function () {
        this.editMode = false;
        return this;
    };
    ABaseModel.prototype.isEditModeEnabled = function () {
        return this.editMode;
    };
    /**
     * Detect null values for each type
     * @param {*} type
     * @return {*}
     */
    ABaseModel.prototype.nullValueByType = function (type) {
        if (Array.isArray(type)) {
            return this.$services.$nullValuesMap.array;
        }
        if (typeof type === 'function') {
            var c = type;
            return new c();
        }
        if (this.$services.$nullValuesMap.hasOwnProperty(type)) {
            return this.$services.$nullValuesMap[type];
        }
        return undefined;
    };
    /**
     * Initi attributes after model construct
     * @return {IBaseModel}
     */
    ABaseModel.prototype.initEmptyAttributes = function () {
        var _this = this;
        Object
            .keys(this.schema())
            .forEach(function (n) { return _this.setAttributeBySchema(n, _this.nullValueByType(_this.schema()[n].$t)); });
        return this;
    };
    /**
     * Defining each attribute by declared schema
     * @param {string} name
     * @param value
     * @return {IBaseModel}
     */
    ABaseModel.prototype.setAttributeBySchema = function (name, value) {
        var _this = this;
        if (this.schema().hasOwnProperty(name)) {
            var cast_1 = function (name, value, type) {
                /**@override IBaseModel this */
                var t = type ? type : _this.schema()[name].$t;
                if (typeof t === 'function') { // we think that it is another model
                    var a = (new t());
                    if (value != null) {
                        a.setAttributes(value);
                    }
                    return a;
                }
                switch (t) {
                    case 'string':
                        return String(value);
                    case 'date_int_ms':
                        return new Date(value);
                    case 'number':
                        return Number(value);
                    case 'bool':
                    case 'boolean':
                        if (value === '0') {
                            value = false;
                        }
                        if (value === '1') {
                            value = true;
                        }
                        return Boolean(value);
                }
            };
            if (Array.isArray(this.schema()[name].$t)) {
                this.attributes[name] = [];
                if (this.schema()[name].$t.length !== 1) {
                    throw new Error("Allowed only one type for array definitions for attribute \"" + name + "\"");
                }
                if (!Array.isArray(value)) {
                    if (this.ALLOW_NULL_VALUES === true && value === null) {
                        value = [];
                    }
                    else {
                        throw new Error("Value must be array according to scheme definition for attribute \"" + name + "\"");
                    }
                }
                value.forEach(function (val) {
                    _this.attributes[name].push(cast_1(name, val, _this.schema()[name].$t[0]));
                });
            }
            else {
                this.attributes[name] = cast_1(name, value);
            }
            this.fillAttributesCopies();
        }
        return this;
    };
    ABaseModel.prototype.fillAttributesCopies = function () {
        var _this = this;
        Object
            .keys(this.schema())
            .forEach(function (attrName) {
            if (_this.schema()[attrName].$c &&
                _this.attributes.hasOwnProperty(_this.schema()[attrName].$c)) {
                _this.attributes[attrName] = _this.attributes[_this.schema()[attrName].$c];
            }
        });
        return this;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.labels = function () {
        var _this = this;
        return Object
            .keys(this.schema())
            .reduce(function (p, c) {
            if (_this.schema()[c].$l) {
                p[c] = _this.schema()[c].$l;
                return p;
            }
            p[c] = c
                .replace(/([A-Z])/g, ' $1')
                .replace(/^./, function (str) {
                return str.toUpperCase();
            });
            p[c] = c.split('_').map(function (word) {
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join(' ');
            return p;
        }, {});
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.descriptions = function () {
        return {};
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.attribute = function (name) {
        return new AutoAttribute(this.labels(), this.attributes, name, this);
    };
    /**
     * Provide attribute value
     */
    ABaseModel.prototype.value = function (name) {
        return this.attribute(name).value();
    };
    /**
     * Provide label by attribute name
     */
    ABaseModel.prototype.label = function (name) {
        return this.attribute(name).label();
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setAttribute = function (name, value) {
        return this.setAttributeBySchema(name, value);
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setAttributes = function (attributes) {
        var _this = this;
        Object
            .keys(attributes)
            .forEach(function (name) {
            _this.setAttributeBySchema(name, attributes[name]);
        });
        return this;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.validate = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.clearErrors();
            _this.$services.$errors = validate(_this.attributes, Object
                .keys(_this.schema())
                .reduce(function (p, c) {
                if (_this.schema()[c].$v) {
                    p[c] = _this.schema()[c].$v;
                }
                return p;
            }, {}));
            if (_this.$services.$errors === undefined) {
                _this.$services.$errors = {};
                resolve();
            }
            else {
                reject();
            }
        });
    };
    /**
     * Represent this model as JSON object annotation
     * @return {*}
     */
    ABaseModel.prototype.toJSON = function () {
        var _this = this;
        return Object.keys(this.schema()).reduce(function (p, c, i, a) {
            if (_this.schema()[c].$t === 'date_int_ms') {
                p[c] = _this.attributes[c].getTime();
            }
            else {
                p[c] = _this.attributes[c];
            }
            return p;
        }, {});
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.errors = function (name) {
        return Array.isArray(this.$services.$errors[name]) ? this.$services.$errors[name] : [];
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.isValid = function () {
        return Object.keys(this.$services.$errors).length === 0;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.clearErrors = function () {
        var _this = this;
        Object
            .keys(this.$services.$errors)
            .forEach(function (v) {
            delete _this.$services.$errors[v];
        });
        return this;
    };
    /**
     * @todo add ability to detect models
     * @param {Array<string>} names
     * @return {{[p: string]: any}}
     */
    ABaseModel.prototype.copyRawAttributes = function (names) {
        var _this = this;
        try {
            var ser = JSON.stringify(Object.keys(this.schema()).reduce(function (p, c) {
                if (Array.isArray(names)) {
                    if (names.indexOf(c) !== -1) {
                        p[c] = _this.attribute(c).value();
                    }
                }
                else {
                    p[c] = _this.attribute(c).value();
                }
                return p;
            }, {}));
            return JSON.parse(ser);
        }
        catch (e) {
            console.log("model clone: could not to serialize / un serialize attributes!, message " + e.message, e);
        }
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.clone = function () {
        var c = this.constructor, cloned = new c;
        cloned.setAttributes(this.copyRawAttributes());
        return cloned;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.onAttributesChanged = function () {
        return this.$services.$onAttributesChanged;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setError = function (name, message) {
        if (this.schema().hasOwnProperty(name)) {
            if (!Array.isArray(this.$services.$errors[name])) {
                this.$services.$errors[name] = [];
            }
            this.$services.$errors[name].push(message);
        }
        return this;
    };
    /**
     * @inheritDoc
     */
    ABaseModel.prototype.setErrors = function (errors) {
        var _this = this;
        Object.keys(errors).forEach(function (name) {
            if (Array.isArray(errors[name])) {
                errors[name].forEach(function (message) {
                    _this.setError(name, message);
                });
            }
        });
        return this;
    };
    /**
     * @todo implement array path processing like items[3]name
     */
    ABaseModel.prototype.setErrorsByPath = function (path) {
        var message = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            message[_i - 1] = arguments[_i];
        }
        var steps = path.split('.');
        var model = this;
        var lastStepW = '';
        while (steps.length) {
            var lastStep = steps.shift();
            if (lastStep && lastStep.length > 2 && lastStep[0] === '[' && lastStep[lastStep.length - 1] === ']') {
                var index = parseInt(lastStep.substr(1, lastStep.length - 1), 10);
                lastStepW = steps.shift();
                if (isNaN(index) === false && Array.isArray(model) && model.length - 1 >= index) {
                    model = model[index];
                }
                continue;
            }
            if (steps.length >= 1) {
                // code for checking items[3]name must be here
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            lastStepW = lastStep;
        }
        if (model !== null && typeof model === 'object' && lastStepW !== '') {
            var errs = {};
            errs[lastStepW] = message;
            model.setErrors(errs);
        }
        return this;
    };
    ABaseModel.prototype.countErrors = function () {
        var _this = this;
        var paths = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            paths[_i] = arguments[_i];
        }
        var count = 0;
        paths.forEach(function (path) {
            count = _this.countErrorsInModel(count, _this, path.split('.'));
        });
        return count;
    };
    ABaseModel.prototype.countErrorsInModel = function (count, model, steps) {
        var _this = this;
        var lastStep = '';
        while (steps.length) {
            lastStep = steps.shift();
            if (lastStep === '[*]' && Array.isArray(model)) {
                model.forEach(function (item) {
                    count = _this.countErrorsInModel(count, item, steps);
                });
            }
            if (steps.length >= 1 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object') {
                try {
                    model = model.attribute(lastStep).value();
                }
                catch (e) {
                }
            }
            if (steps.length === 0 &&
                !Array.isArray(model) &&
                model !== null &&
                typeof model === 'object' &&
                model.attribute(lastStep).errors().length > 0) {
                count++;
            }
        }
        return count;
    };
    ABaseModel.prototype.setErrorsV2 = function (errors) {
        for (var errorsKey in errors) {
            if (errors.hasOwnProperty(errorsKey)) {
                this.setErrorsByPath.apply(this, __spread([errors[errorsKey].attribute], errors[errorsKey].messages));
            }
        }
        return this;
    };
    ABaseModel.prototype.getDataToSave = function () {
        var _this = this;
        var data = {};
        Object
            .keys(this.schema())
            .forEach(function (attrName) {
            if (_this.schema()[attrName].$s) {
                var value = _this.attribute(attrName).value();
                data[attrName] = (typeof value.getDataToSave === 'function' ? value.getDataToSave() : value);
            }
        });
        Object.keys(data).forEach(function (key) {
            if (typeof data[key] === 'object' && Object.keys(data[key]).length === 0) {
                delete data[key];
            }
            if (Array.isArray(data[key]) && data[key].length === 0) {
                delete data[key];
            }
            if (!data[key]) {
                delete data[key];
            }
        });
        return data;
    };
    ABaseModel.prototype.setScenario = function () {
    };
    ABaseModel.prototype.enum = function (name) {
        if (!this.enums.hasOwnProperty(name)) {
            throw new Error("enums have no property " + name);
        }
        return this.enums[name];
    };
    return ABaseModel;
}());
export { ABaseModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscy9iYXNlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFVckMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHFEQUFxRCxDQUFDO0FBR2xGO0lBOEJFO1FBNUJRLHNCQUFpQixHQUFHLElBQUksQ0FBQztRQUVqQzs7O1dBR0c7UUFDTyxlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBRWpCLGNBQVMsR0FBRztZQUNwQixvQkFBb0IsRUFBRSxJQUFJLFlBQVksRUFBVTtZQUNoRCxjQUFjLEVBQUU7Z0JBQ2QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsV0FBVyxFQUFFLElBQUksSUFBSSxFQUFFO2FBQ3hCO1lBQ0QsT0FBTyxFQUFFLEVBQUU7WUFDWCxTQUFTLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLEVBQUU7YUFDVDtTQUNGLENBQUM7UUFJQSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU0sOEJBQVMsR0FBaEI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUVyQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSwrQkFBVSxHQUFqQjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBRXRCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELHNDQUFpQixHQUFqQjtRQUNFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN2QixDQUFDO0lBT0Q7Ozs7T0FJRztJQUNLLG9DQUFlLEdBQXZCLFVBQXdCLElBQVM7UUFFL0IsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxPQUFPLElBQUksS0FBSyxVQUFVLEVBQUU7WUFDOUIsSUFBTSxDQUFDLEdBQXFCLElBQUksQ0FBQztZQUNqQyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7U0FDaEI7UUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0RCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDbkIsQ0FBQztJQUVEOzs7T0FHRztJQUNLLHdDQUFtQixHQUEzQjtRQUFBLGlCQUtDO1FBSkMsTUFBTTthQUNILElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDbkIsT0FBTyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsS0FBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUF2RSxDQUF1RSxDQUFDLENBQUM7UUFDekYsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSyx5Q0FBb0IsR0FBNUIsVUFBNkIsSUFBWSxFQUFFLEtBQVU7UUFBckQsaUJBdURDO1FBdERDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUV0QyxJQUFNLE1BQUksR0FBRyxVQUFDLElBQVksRUFBRSxLQUFVLEVBQUUsSUFBbUI7Z0JBQ3pELCtCQUErQjtnQkFDL0IsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQy9DLElBQUksT0FBTyxDQUFDLEtBQUssVUFBVSxFQUFFLEVBQUUsb0NBQW9DO29CQUNqRSxJQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDcEIsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO3dCQUNqQixDQUFDLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN4QjtvQkFDRCxPQUFPLENBQUMsQ0FBQztpQkFDVjtnQkFDRCxRQUFRLENBQUMsRUFBRTtvQkFDVCxLQUFLLFFBQVE7d0JBQ1gsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3ZCLEtBQUssYUFBYTt3QkFDaEIsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDekIsS0FBSyxRQUFRO3dCQUNYLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN2QixLQUFLLE1BQU0sQ0FBQztvQkFDWixLQUFLLFNBQVM7d0JBQ1osSUFBSSxLQUFLLEtBQUssR0FBRyxFQUFFOzRCQUNqQixLQUFLLEdBQUcsS0FBSyxDQUFDO3lCQUNmO3dCQUNELElBQUksS0FBSyxLQUFLLEdBQUcsRUFBRTs0QkFDakIsS0FBSyxHQUFHLElBQUksQ0FBQzt5QkFDZDt3QkFDRCxPQUFPLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDekI7WUFDSCxDQUFDLENBQUM7WUFFRixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQ3ZDLE1BQU0sSUFBSSxLQUFLLENBQUMsaUVBQThELElBQUksT0FBRyxDQUFDLENBQUM7aUJBQ3hGO2dCQUNELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUN6QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxJQUFJLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTt3QkFDckQsS0FBSyxHQUFHLEVBQUUsQ0FBQztxQkFDWjt5QkFBTTt3QkFDTCxNQUFNLElBQUksS0FBSyxDQUFDLHdFQUFxRSxJQUFJLE9BQUcsQ0FBQyxDQUFDO3FCQUMvRjtpQkFDRjtnQkFDRCxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRztvQkFDaEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pFLENBQUMsQ0FBQyxDQUFDO2FBRUo7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQzNDO1lBRUQsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FDN0I7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTyx5Q0FBb0IsR0FBNUI7UUFBQSxpQkFVQztRQVRDLE1BQU07YUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ25CLE9BQU8sQ0FBQyxVQUFDLFFBQVE7WUFDaEIsSUFBSSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRTtnQkFDNUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUM1RCxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3pFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRDs7T0FFRztJQUNILDJCQUFNLEdBQU47UUFBQSxpQkFrQkM7UUFqQkMsT0FBTyxNQUFNO2FBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNuQixNQUFNLENBQUMsVUFBQyxDQUEwQixFQUFFLENBQUM7WUFDcEMsSUFBSSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUN2QixDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDM0IsT0FBTyxDQUFDLENBQUM7YUFDVjtZQUNELENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO2lCQUNMLE9BQU8sQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDO2lCQUMxQixPQUFPLENBQUMsSUFBSSxFQUFFLFVBQVUsR0FBRztnQkFDMUIsT0FBTyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFZO2dCQUNuQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RCxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDYixPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRDs7T0FFRztJQUNILGlDQUFZLEdBQVo7UUFDRSxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRDs7T0FFRztJQUNILDhCQUFTLEdBQVQsVUFBVSxJQUFZO1FBQ3BCLE9BQU8sSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRDs7T0FFRztJQUNILDBCQUFLLEdBQUwsVUFBTSxJQUFZO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCwwQkFBSyxHQUFMLFVBQU0sSUFBWTtRQUNoQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsaUNBQVksR0FBWixVQUFhLElBQVksRUFBRSxLQUFVO1FBQ25DLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxrQ0FBYSxHQUFiLFVBQWMsVUFBMEI7UUFBeEMsaUJBT0M7UUFOQyxNQUFNO2FBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQzthQUNoQixPQUFPLENBQUMsVUFBQyxJQUFJO1lBQ1osS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsNkJBQVEsR0FBUjtRQUFBLGlCQWtCQztRQWpCQyxPQUFPLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDdEMsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFFLE1BQU07aUJBQ3RELElBQUksQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7aUJBQ25CLE1BQU0sQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNYLElBQUksS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDdkIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzVCO2dCQUNELE9BQU8sQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDVixJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxLQUFLLFNBQVMsRUFBRTtnQkFDeEMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO2dCQUM1QixPQUFPLEVBQUUsQ0FBQzthQUNYO2lCQUFNO2dCQUNMLE1BQU0sRUFBRSxDQUFDO2FBQ1Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSCwyQkFBTSxHQUFOO1FBQUEsaUJBU0M7UUFSQyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNsRCxJQUFJLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssYUFBYSxFQUFFO2dCQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUNyQztpQkFBTTtnQkFDTCxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMzQjtZQUNELE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsMkJBQU0sR0FBTixVQUFPLElBQVk7UUFDakIsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDekYsQ0FBQztJQUVEOztPQUVHO0lBQ0gsNEJBQU8sR0FBUDtRQUNFLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZ0NBQVcsR0FBWDtRQUFBLGlCQU9DO1FBTkMsTUFBTTthQUNILElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQzthQUM1QixPQUFPLENBQUMsVUFBQyxDQUFDO1lBQ1QsT0FBTyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxzQ0FBaUIsR0FBakIsVUFBa0IsS0FBcUI7UUFBdkMsaUJBZ0JDO1FBZkMsSUFBSTtZQUNGLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQztnQkFDaEUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUN4QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQzNCLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUNsQztpQkFDRjtxQkFBTTtvQkFDTCxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDbEM7Z0JBQ0QsT0FBTyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNSLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN4QjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2RUFBMkUsQ0FBQyxDQUFDLE9BQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN4RztJQUNILENBQUM7SUFFRDs7T0FFRztJQUNILDBCQUFLLEdBQUw7UUFDRSxJQUFNLENBQUMsR0FBUSxJQUFJLENBQUMsV0FBVyxFQUM3QixNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDakIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO1FBQy9DLE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNILHdDQUFtQixHQUFuQjtRQUNFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQztJQUM3QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSw2QkFBUSxHQUFmLFVBQWdCLElBQVksRUFBRSxPQUFlO1FBQzNDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0QyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUNoRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDbkM7WUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDNUM7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFHRDs7T0FFRztJQUNJLDhCQUFTLEdBQWhCLFVBQWlCLE1BQWtCO1FBQW5DLGlCQVVDO1FBVEMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFZO1lBQ3ZDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFDL0IsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQWU7b0JBQ25DLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUMvQixDQUFDLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRDs7T0FFRztJQUNLLG9DQUFlLEdBQXZCLFVBQXdCLElBQVk7UUFBRSxpQkFBb0I7YUFBcEIsVUFBb0IsRUFBcEIscUJBQW9CLEVBQXBCLElBQW9CO1lBQXBCLGdDQUFvQjs7UUFDeEQsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QixJQUFJLEtBQUssR0FBZSxJQUFJLENBQUM7UUFDN0IsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ25CLE9BQU8sS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNuQixJQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDL0IsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Z0JBQ25HLElBQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNwRSxTQUFTLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUMxQixJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLEVBQUU7b0JBQy9FLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RCO2dCQUNELFNBQVM7YUFDVjtZQUVELElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3JCLDhDQUE4QztnQkFDOUMsSUFBSTtvQkFDRixLQUFLLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDM0M7Z0JBQUMsT0FBTyxDQUFDLEVBQUU7aUJBRVg7YUFDRjtZQUNELFNBQVMsR0FBRyxRQUFRLENBQUM7U0FDdEI7UUFDRCxJQUFJLEtBQUssS0FBSyxJQUFJLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLFNBQVMsS0FBSyxFQUFFLEVBQUU7WUFDbkUsSUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxPQUFPLENBQUM7WUFDMUIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUdNLGdDQUFXLEdBQWxCO1FBQUEsaUJBTUM7UUFOa0IsZUFBa0I7YUFBbEIsVUFBa0IsRUFBbEIscUJBQWtCLEVBQWxCLElBQWtCO1lBQWxCLDBCQUFrQjs7UUFDbkMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDakIsS0FBSyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsS0FBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNoRSxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVNLHVDQUFrQixHQUF6QixVQUEwQixLQUFhLEVBQUUsS0FBaUIsRUFBRSxLQUFlO1FBQTNFLGlCQTRCQztRQTNCQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsT0FBTyxLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ25CLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDekIsSUFBSSxRQUFRLEtBQUssS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO29CQUNqQixLQUFLLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3RELENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFDRCxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDbkIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDckIsS0FBSyxLQUFLLElBQUk7Z0JBQ2QsT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO2dCQUMzQixJQUFJO29CQUNGLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUMzQztnQkFBQyxPQUFPLENBQUMsRUFBRTtpQkFFWDthQUNGO1lBQ0QsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUM7Z0JBQ3BCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ3JCLEtBQUssS0FBSyxJQUFJO2dCQUNkLE9BQU8sS0FBSyxLQUFLLFFBQVE7Z0JBQ3pCLEtBQUssQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDL0MsS0FBSyxFQUFFLENBQUM7YUFDVDtTQUNGO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRU0sZ0NBQVcsR0FBbEIsVUFBbUIsTUFBb0I7UUFDckMsS0FBSyxJQUFNLFNBQVMsSUFBSSxNQUFNLEVBQUU7WUFDOUIsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUNwQyxJQUFJLENBQUMsZUFBZSxPQUFwQixJQUFJLFlBQWlCLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLEdBQUssTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsR0FBRTthQUNsRjtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBR00sa0NBQWEsR0FBcEI7UUFBQSxpQkE0QkM7UUEzQkMsSUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBRWhCLE1BQU07YUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ25CLE9BQU8sQ0FBQyxVQUFDLFFBQVE7WUFDaEIsSUFBSSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUM5QixJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEtBQUssQ0FBQyxhQUFhLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzlGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEdBQUc7WUFFNUIsSUFBSSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUN4RSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQjtZQUVELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDdEQsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbEI7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNkLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxnQ0FBVyxHQUFYO0lBRUEsQ0FBQztJQUVNLHlCQUFJLEdBQVgsVUFBWSxJQUFJO1FBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3BDLE1BQU0sSUFBSSxLQUFLLENBQUMsNEJBQTBCLElBQU0sQ0FBQyxDQUFDO1NBQ25EO1FBRUQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFDSCxpQkFBQztBQUFELENBQUMsQUE1ZUQsSUE0ZUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0V2ZW50RW1pdHRlcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7dmFsaWRhdGV9IGZyb20gJ3ZhbGlkYXRlLmpzJztcclxuXHJcbmltcG9ydCB7XHJcbiAgQXR0cmlidXRlc1R5cGUsXHJcbiAgRGVzY3JpcHRpb25zVHlwZSwgRXJyb3JzVHlwZSwgRXJyb3JzVHlwZVYyLFxyXG4gIElCYXNlTW9kZWwsXHJcbiAgTGFiZWxzVHlwZSxcclxuICBTY2hlbWFUeXBlXHJcbn0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscyc7XHJcbmltcG9ydCB7SUF0dHJpYnV0ZX0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMnO1xyXG5pbXBvcnQge0F1dG9BdHRyaWJ1dGV9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9kYXRhLXN0cnVjdHVyZXMvYXR0cmlidXRlcyc7XHJcbmltcG9ydCB7RW51bU1vZGVsfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscy9lbnVtLm1vZGVsJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBQmFzZU1vZGVsIGltcGxlbWVudHMgSUJhc2VNb2RlbCB7XHJcblxyXG4gIHByaXZhdGUgQUxMT1dfTlVMTF9WQUxVRVMgPSB0cnVlO1xyXG5cclxuICAvKipcclxuICAgKiBJbnRlcm5hbCBkaWN0aW9uYXJ5IGZvciBrZWVwaW5nIGF0dHJpYnV0ZXNcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIHByb3RlY3RlZCBhdHRyaWJ1dGVzID0ge307XHJcbiAgcHJvdGVjdGVkIGVudW1zID0ge307XHJcbiAgcHJvdGVjdGVkIGVkaXRNb2RlID0gZmFsc2U7XHJcblxyXG4gIHByb3RlY3RlZCAkc2VydmljZXMgPSB7XHJcbiAgICAkb25BdHRyaWJ1dGVzQ2hhbmdlZDogbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCksXHJcbiAgICAkbnVsbFZhbHVlc01hcDoge1xyXG4gICAgICBzdHJpbmc6ICcnLFxyXG4gICAgICBudW1iZXI6IDAsXHJcbiAgICAgIGJvb2xlYW46IGZhbHNlLFxyXG4gICAgICBib29sOiBmYWxzZSxcclxuICAgICAgYXJyYXk6IFtdLFxyXG4gICAgICBkYXRlX2ludF9tczogbmV3IERhdGUoKSxcclxuICAgIH0sXHJcbiAgICAkZXJyb3JzOiB7fSxcclxuICAgICRzY2VuYXJpbzoge1xyXG4gICAgICBhY3RpdmU6IHt9LFxyXG4gICAgICBsaXN0OiBbXSxcclxuICAgIH1cclxuICB9O1xyXG5cclxuXHJcbiAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5pbml0RW1wdHlBdHRyaWJ1dGVzKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhcnRFZGl0KCk6IHRoaXMge1xyXG4gICAgdGhpcy5lZGl0TW9kZSA9IHRydWU7XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZmluaXNoRWRpdCgpOiB0aGlzIHtcclxuICAgIHRoaXMuZWRpdE1vZGUgPSBmYWxzZTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIGlzRWRpdE1vZGVFbmFibGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZWRpdE1vZGU7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGFic3RyYWN0IHNjaGVtYSgpOiBTY2hlbWFUeXBlO1xyXG5cclxuICAvKipcclxuICAgKiBEZXRlY3QgbnVsbCB2YWx1ZXMgZm9yIGVhY2ggdHlwZVxyXG4gICAqIEBwYXJhbSB7Kn0gdHlwZVxyXG4gICAqIEByZXR1cm4geyp9XHJcbiAgICovXHJcbiAgcHJpdmF0ZSBudWxsVmFsdWVCeVR5cGUodHlwZTogYW55KTogYW55IHtcclxuXHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh0eXBlKSkge1xyXG4gICAgICByZXR1cm4gdGhpcy4kc2VydmljZXMuJG51bGxWYWx1ZXNNYXAuYXJyYXk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHR5cGVvZiB0eXBlID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgIGNvbnN0IGM6IGFueSB8IElCYXNlTW9kZWwgPSB0eXBlO1xyXG4gICAgICByZXR1cm4gbmV3IGMoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy4kc2VydmljZXMuJG51bGxWYWx1ZXNNYXAuaGFzT3duUHJvcGVydHkodHlwZSkpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuJHNlcnZpY2VzLiRudWxsVmFsdWVzTWFwW3R5cGVdO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEluaXRpIGF0dHJpYnV0ZXMgYWZ0ZXIgbW9kZWwgY29uc3RydWN0XHJcbiAgICogQHJldHVybiB7SUJhc2VNb2RlbH1cclxuICAgKi9cclxuICBwcml2YXRlIGluaXRFbXB0eUF0dHJpYnV0ZXMoKTogSUJhc2VNb2RlbCB7XHJcbiAgICBPYmplY3RcclxuICAgICAgLmtleXModGhpcy5zY2hlbWEoKSlcclxuICAgICAgLmZvckVhY2gobiA9PiB0aGlzLnNldEF0dHJpYnV0ZUJ5U2NoZW1hKG4sIHRoaXMubnVsbFZhbHVlQnlUeXBlKHRoaXMuc2NoZW1hKClbbl0uJHQpKSk7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIERlZmluaW5nIGVhY2ggYXR0cmlidXRlIGJ5IGRlY2xhcmVkIHNjaGVtYVxyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXHJcbiAgICogQHBhcmFtIHZhbHVlXHJcbiAgICogQHJldHVybiB7SUJhc2VNb2RlbH1cclxuICAgKi9cclxuICBwcml2YXRlIHNldEF0dHJpYnV0ZUJ5U2NoZW1hKG5hbWU6IHN0cmluZywgdmFsdWU6IGFueSk6IHRoaXMge1xyXG4gICAgaWYgKHRoaXMuc2NoZW1hKCkuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcclxuXHJcbiAgICAgIGNvbnN0IGNhc3QgPSAobmFtZTogc3RyaW5nLCB2YWx1ZTogYW55LCB0eXBlPzogc3RyaW5nIHwgYW55KTogYW55ID0+IHtcclxuICAgICAgICAvKipAb3ZlcnJpZGUgSUJhc2VNb2RlbCB0aGlzICovXHJcbiAgICAgICAgY29uc3QgdCA9IHR5cGUgPyB0eXBlIDogdGhpcy5zY2hlbWEoKVtuYW1lXS4kdDtcclxuICAgICAgICBpZiAodHlwZW9mIHQgPT09ICdmdW5jdGlvbicpIHsgLy8gd2UgdGhpbmsgdGhhdCBpdCBpcyBhbm90aGVyIG1vZGVsXHJcbiAgICAgICAgICBjb25zdCBhID0gKG5ldyB0KCkpO1xyXG4gICAgICAgICAgaWYgKHZhbHVlICE9IG51bGwpIHtcclxuICAgICAgICAgICAgYS5zZXRBdHRyaWJ1dGVzKHZhbHVlKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBhO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzd2l0Y2ggKHQpIHtcclxuICAgICAgICAgIGNhc2UgJ3N0cmluZyc6XHJcbiAgICAgICAgICAgIHJldHVybiBTdHJpbmcodmFsdWUpO1xyXG4gICAgICAgICAgY2FzZSAnZGF0ZV9pbnRfbXMnOlxyXG4gICAgICAgICAgICByZXR1cm4gbmV3IERhdGUodmFsdWUpO1xyXG4gICAgICAgICAgY2FzZSAnbnVtYmVyJzpcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcih2YWx1ZSk7XHJcbiAgICAgICAgICBjYXNlICdib29sJzpcclxuICAgICAgICAgIGNhc2UgJ2Jvb2xlYW4nOlxyXG4gICAgICAgICAgICBpZiAodmFsdWUgPT09ICcwJykge1xyXG4gICAgICAgICAgICAgIHZhbHVlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHZhbHVlID09PSAnMScpIHtcclxuICAgICAgICAgICAgICB2YWx1ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIEJvb2xlYW4odmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuc2NoZW1hKClbbmFtZV0uJHQpKSB7XHJcbiAgICAgICAgdGhpcy5hdHRyaWJ1dGVzW25hbWVdID0gW107XHJcbiAgICAgICAgaWYgKHRoaXMuc2NoZW1hKClbbmFtZV0uJHQubGVuZ3RoICE9PSAxKSB7XHJcbiAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYEFsbG93ZWQgb25seSBvbmUgdHlwZSBmb3IgYXJyYXkgZGVmaW5pdGlvbnMgZm9yIGF0dHJpYnV0ZSBcIiR7bmFtZX1cImApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5BTExPV19OVUxMX1ZBTFVFUyA9PT0gdHJ1ZSAmJiB2YWx1ZSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IFtdO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBWYWx1ZSBtdXN0IGJlIGFycmF5IGFjY29yZGluZyB0byBzY2hlbWUgZGVmaW5pdGlvbiBmb3IgYXR0cmlidXRlIFwiJHtuYW1lfVwiYCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhbHVlLmZvckVhY2goKHZhbCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5hdHRyaWJ1dGVzW25hbWVdLnB1c2goY2FzdChuYW1lLCB2YWwsIHRoaXMuc2NoZW1hKClbbmFtZV0uJHRbMF0pKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5hdHRyaWJ1dGVzW25hbWVdID0gY2FzdChuYW1lLCB2YWx1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuZmlsbEF0dHJpYnV0ZXNDb3BpZXMoKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBmaWxsQXR0cmlidXRlc0NvcGllcygpOiB0aGlzIHtcclxuICAgIE9iamVjdFxyXG4gICAgICAua2V5cyh0aGlzLnNjaGVtYSgpKVxyXG4gICAgICAuZm9yRWFjaCgoYXR0ck5hbWUpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5zY2hlbWEoKVthdHRyTmFtZV0uJGMgJiZcclxuICAgICAgICAgIHRoaXMuYXR0cmlidXRlcy5oYXNPd25Qcm9wZXJ0eSh0aGlzLnNjaGVtYSgpW2F0dHJOYW1lXS4kYykpIHtcclxuICAgICAgICAgIHRoaXMuYXR0cmlidXRlc1thdHRyTmFtZV0gPSB0aGlzLmF0dHJpYnV0ZXNbdGhpcy5zY2hlbWEoKVthdHRyTmFtZV0uJGNdO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgbGFiZWxzKCk6IExhYmVsc1R5cGUge1xyXG4gICAgcmV0dXJuIE9iamVjdFxyXG4gICAgICAua2V5cyh0aGlzLnNjaGVtYSgpKVxyXG4gICAgICAucmVkdWNlKChwOiB7IFtzOiBzdHJpbmddOiBzdHJpbmcgfSwgYykgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnNjaGVtYSgpW2NdLiRsKSB7XHJcbiAgICAgICAgICBwW2NdID0gdGhpcy5zY2hlbWEoKVtjXS4kbDtcclxuICAgICAgICAgIHJldHVybiBwO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwW2NdID0gY1xyXG4gICAgICAgICAgLnJlcGxhY2UoLyhbQS1aXSkvZywgJyAkMScpXHJcbiAgICAgICAgICAucmVwbGFjZSgvXi4vLCBmdW5jdGlvbiAoc3RyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzdHIudG9VcHBlckNhc2UoKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIHBbY10gPSBjLnNwbGl0KCdfJykubWFwKCh3b3JkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgIHJldHVybiB3b3JkLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgd29yZC5zbGljZSgxKTtcclxuICAgICAgICB9KS5qb2luKCcgJyk7XHJcbiAgICAgICAgcmV0dXJuIHA7XHJcbiAgICAgIH0sIHt9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgZGVzY3JpcHRpb25zKCk6IERlc2NyaXB0aW9uc1R5cGUge1xyXG4gICAgcmV0dXJuIHt9O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBhdHRyaWJ1dGUobmFtZTogc3RyaW5nKTogSUF0dHJpYnV0ZSB7XHJcbiAgICByZXR1cm4gbmV3IEF1dG9BdHRyaWJ1dGUodGhpcy5sYWJlbHMoKSwgdGhpcy5hdHRyaWJ1dGVzLCBuYW1lLCB0aGlzKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFByb3ZpZGUgYXR0cmlidXRlIHZhbHVlXHJcbiAgICovXHJcbiAgdmFsdWUobmFtZTogc3RyaW5nKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZShuYW1lKS52YWx1ZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUHJvdmlkZSBsYWJlbCBieSBhdHRyaWJ1dGUgbmFtZVxyXG4gICAqL1xyXG4gIGxhYmVsKG5hbWU6IHN0cmluZyk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5hdHRyaWJ1dGUobmFtZSkubGFiZWwoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgc2V0QXR0cmlidXRlKG5hbWU6IHN0cmluZywgdmFsdWU6IGFueSk6IHRoaXMge1xyXG4gICAgcmV0dXJuIHRoaXMuc2V0QXR0cmlidXRlQnlTY2hlbWEobmFtZSwgdmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBzZXRBdHRyaWJ1dGVzKGF0dHJpYnV0ZXM6IEF0dHJpYnV0ZXNUeXBlKTogdGhpcyB7XHJcbiAgICBPYmplY3RcclxuICAgICAgLmtleXMoYXR0cmlidXRlcylcclxuICAgICAgLmZvckVhY2goKG5hbWUpID0+IHtcclxuICAgICAgICB0aGlzLnNldEF0dHJpYnV0ZUJ5U2NoZW1hKG5hbWUsIGF0dHJpYnV0ZXNbbmFtZV0pO1xyXG4gICAgICB9KTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICB2YWxpZGF0ZSgpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICB0aGlzLmNsZWFyRXJyb3JzKCk7XHJcbiAgICAgIHRoaXMuJHNlcnZpY2VzLiRlcnJvcnMgPSB2YWxpZGF0ZSh0aGlzLmF0dHJpYnV0ZXMsIE9iamVjdFxyXG4gICAgICAgIC5rZXlzKHRoaXMuc2NoZW1hKCkpXHJcbiAgICAgICAgLnJlZHVjZSgocCwgYykgPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMuc2NoZW1hKClbY10uJHYpIHtcclxuICAgICAgICAgICAgcFtjXSA9IHRoaXMuc2NoZW1hKClbY10uJHY7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXR1cm4gcDtcclxuICAgICAgICB9LCB7fSkpO1xyXG4gICAgICBpZiAodGhpcy4kc2VydmljZXMuJGVycm9ycyA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgdGhpcy4kc2VydmljZXMuJGVycm9ycyA9IHt9O1xyXG4gICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZWplY3QoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXByZXNlbnQgdGhpcyBtb2RlbCBhcyBKU09OIG9iamVjdCBhbm5vdGF0aW9uXHJcbiAgICogQHJldHVybiB7Kn1cclxuICAgKi9cclxuICB0b0pTT04oKSB7XHJcbiAgICByZXR1cm4gT2JqZWN0LmtleXModGhpcy5zY2hlbWEoKSkucmVkdWNlKChwLCBjLCBpLCBhKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnNjaGVtYSgpW2NdLiR0ID09PSAnZGF0ZV9pbnRfbXMnKSB7XHJcbiAgICAgICAgcFtjXSA9IHRoaXMuYXR0cmlidXRlc1tjXS5nZXRUaW1lKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcFtjXSA9IHRoaXMuYXR0cmlidXRlc1tjXTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gcDtcclxuICAgIH0sIHt9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbmhlcml0RG9jXHJcbiAgICovXHJcbiAgZXJyb3JzKG5hbWU6IHN0cmluZyk6IEFycmF5PHN0cmluZz4ge1xyXG4gICAgcmV0dXJuIEFycmF5LmlzQXJyYXkodGhpcy4kc2VydmljZXMuJGVycm9yc1tuYW1lXSkgPyB0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzW25hbWVdIDogW107XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gT2JqZWN0LmtleXModGhpcy4kc2VydmljZXMuJGVycm9ycykubGVuZ3RoID09PSAwO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBjbGVhckVycm9ycygpOiB0aGlzIHtcclxuICAgIE9iamVjdFxyXG4gICAgICAua2V5cyh0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzKVxyXG4gICAgICAuZm9yRWFjaCgodikgPT4ge1xyXG4gICAgICAgIGRlbGV0ZSB0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzW3ZdO1xyXG4gICAgICB9KTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQHRvZG8gYWRkIGFiaWxpdHkgdG8gZGV0ZWN0IG1vZGVsc1xyXG4gICAqIEBwYXJhbSB7QXJyYXk8c3RyaW5nPn0gbmFtZXNcclxuICAgKiBAcmV0dXJuIHt7W3A6IHN0cmluZ106IGFueX19XHJcbiAgICovXHJcbiAgY29weVJhd0F0dHJpYnV0ZXMobmFtZXM/OiBBcnJheTxzdHJpbmc+KTogeyBbbmFtZTogc3RyaW5nXTogYW55IH0ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgY29uc3Qgc2VyID0gSlNPTi5zdHJpbmdpZnkoT2JqZWN0LmtleXModGhpcy5zY2hlbWEoKSkucmVkdWNlKChwLCBjKSA9PiB7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkobmFtZXMpKSB7XHJcbiAgICAgICAgICBpZiAobmFtZXMuaW5kZXhPZihjKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgcFtjXSA9IHRoaXMuYXR0cmlidXRlKGMpLnZhbHVlKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHBbY10gPSB0aGlzLmF0dHJpYnV0ZShjKS52YWx1ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcDtcclxuICAgICAgfSwge30pKTtcclxuICAgICAgcmV0dXJuIEpTT04ucGFyc2Uoc2VyKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgY29uc29sZS5sb2coYG1vZGVsIGNsb25lOiBjb3VsZCBub3QgdG8gc2VyaWFsaXplIC8gdW4gc2VyaWFsaXplIGF0dHJpYnV0ZXMhLCBtZXNzYWdlICR7ZS5tZXNzYWdlfWAsIGUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBjbG9uZSgpOiB0aGlzIHtcclxuICAgIGNvbnN0IGM6IGFueSA9IHRoaXMuY29uc3RydWN0b3IsXHJcbiAgICAgIGNsb25lZCA9IG5ldyBjO1xyXG4gICAgY2xvbmVkLnNldEF0dHJpYnV0ZXModGhpcy5jb3B5UmF3QXR0cmlidXRlcygpKTtcclxuICAgIHJldHVybiBjbG9uZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIG9uQXR0cmlidXRlc0NoYW5nZWQoKTogRXZlbnRFbWl0dGVyPHN0cmluZz4ge1xyXG4gICAgcmV0dXJuIHRoaXMuJHNlcnZpY2VzLiRvbkF0dHJpYnV0ZXNDaGFuZ2VkO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGluaGVyaXREb2NcclxuICAgKi9cclxuICBwdWJsaWMgc2V0RXJyb3IobmFtZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpOiB0aGlzIHtcclxuICAgIGlmICh0aGlzLnNjaGVtYSgpLmhhc093blByb3BlcnR5KG5hbWUpKSB7XHJcbiAgICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzW25hbWVdKSkge1xyXG4gICAgICAgIHRoaXMuJHNlcnZpY2VzLiRlcnJvcnNbbmFtZV0gPSBbXTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLiRzZXJ2aWNlcy4kZXJyb3JzW25hbWVdLnB1c2gobWVzc2FnZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG5cclxuICAvKipcclxuICAgKiBAaW5oZXJpdERvY1xyXG4gICAqL1xyXG4gIHB1YmxpYyBzZXRFcnJvcnMoZXJyb3JzOiBFcnJvcnNUeXBlKTogdGhpcyB7XHJcbiAgICBPYmplY3Qua2V5cyhlcnJvcnMpLmZvckVhY2goKG5hbWU6IHN0cmluZykgPT4ge1xyXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheShlcnJvcnNbbmFtZV0pKSB7XHJcbiAgICAgICAgZXJyb3JzW25hbWVdLmZvckVhY2goKG1lc3NhZ2U6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgdGhpcy5zZXRFcnJvcihuYW1lLCBtZXNzYWdlKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAdG9kbyBpbXBsZW1lbnQgYXJyYXkgcGF0aCBwcm9jZXNzaW5nIGxpa2UgaXRlbXNbM11uYW1lXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBzZXRFcnJvcnNCeVBhdGgocGF0aDogc3RyaW5nLCAuLi5tZXNzYWdlOiBzdHJpbmdbXSkge1xyXG4gICAgY29uc3Qgc3RlcHMgPSBwYXRoLnNwbGl0KCcuJyk7XHJcbiAgICBsZXQgbW9kZWw6IElCYXNlTW9kZWwgPSB0aGlzO1xyXG4gICAgbGV0IGxhc3RTdGVwVyA9ICcnO1xyXG4gICAgd2hpbGUgKHN0ZXBzLmxlbmd0aCkge1xyXG4gICAgICBjb25zdCBsYXN0U3RlcCA9IHN0ZXBzLnNoaWZ0KCk7XHJcbiAgICAgIGlmIChsYXN0U3RlcCAmJiBsYXN0U3RlcC5sZW5ndGggPiAyICYmIGxhc3RTdGVwWzBdID09PSAnWycgJiYgbGFzdFN0ZXBbbGFzdFN0ZXAubGVuZ3RoIC0gMV0gPT09ICddJykge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gcGFyc2VJbnQobGFzdFN0ZXAuc3Vic3RyKDEsIGxhc3RTdGVwLmxlbmd0aCAtIDEpLCAxMCk7XHJcbiAgICAgICAgbGFzdFN0ZXBXID0gc3RlcHMuc2hpZnQoKTtcclxuICAgICAgICBpZiAoaXNOYU4oaW5kZXgpID09PSBmYWxzZSAmJiBBcnJheS5pc0FycmF5KG1vZGVsKSAmJiBtb2RlbC5sZW5ndGggLSAxID49IGluZGV4KSB7XHJcbiAgICAgICAgICBtb2RlbCA9IG1vZGVsW2luZGV4XTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29udGludWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChzdGVwcy5sZW5ndGggPj0gMSkge1xyXG4gICAgICAgIC8vIGNvZGUgZm9yIGNoZWNraW5nIGl0ZW1zWzNdbmFtZSBtdXN0IGJlIGhlcmVcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgbW9kZWwgPSBtb2RlbC5hdHRyaWJ1dGUobGFzdFN0ZXApLnZhbHVlKCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgbGFzdFN0ZXBXID0gbGFzdFN0ZXA7XHJcbiAgICB9XHJcbiAgICBpZiAobW9kZWwgIT09IG51bGwgJiYgdHlwZW9mIG1vZGVsID09PSAnb2JqZWN0JyAmJiBsYXN0U3RlcFcgIT09ICcnKSB7XHJcbiAgICAgIGNvbnN0IGVycnMgPSB7fTtcclxuICAgICAgZXJyc1tsYXN0U3RlcFddID0gbWVzc2FnZTtcclxuICAgICAgbW9kZWwuc2V0RXJyb3JzKGVycnMpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuXHJcbiAgcHVibGljIGNvdW50RXJyb3JzKC4uLnBhdGhzOiBzdHJpbmdbXSk6IG51bWJlciB7XHJcbiAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgcGF0aHMuZm9yRWFjaCgocGF0aCkgPT4ge1xyXG4gICAgICBjb3VudCA9IHRoaXMuY291bnRFcnJvcnNJbk1vZGVsKGNvdW50LCB0aGlzLCBwYXRoLnNwbGl0KCcuJykpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gY291bnQ7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY291bnRFcnJvcnNJbk1vZGVsKGNvdW50OiBudW1iZXIsIG1vZGVsOiBJQmFzZU1vZGVsLCBzdGVwczogc3RyaW5nW10pOiBudW1iZXIge1xyXG4gICAgbGV0IGxhc3RTdGVwID0gJyc7XHJcbiAgICB3aGlsZSAoc3RlcHMubGVuZ3RoKSB7XHJcbiAgICAgIGxhc3RTdGVwID0gc3RlcHMuc2hpZnQoKTtcclxuICAgICAgaWYgKGxhc3RTdGVwID09PSAnWypdJyAmJiBBcnJheS5pc0FycmF5KG1vZGVsKSkge1xyXG4gICAgICAgIG1vZGVsLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICAgIGNvdW50ID0gdGhpcy5jb3VudEVycm9yc0luTW9kZWwoY291bnQsIGl0ZW0sIHN0ZXBzKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoc3RlcHMubGVuZ3RoID49IDEgJiZcclxuICAgICAgICAhQXJyYXkuaXNBcnJheShtb2RlbCkgJiZcclxuICAgICAgICBtb2RlbCAhPT0gbnVsbCAmJlxyXG4gICAgICAgIHR5cGVvZiBtb2RlbCA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgbW9kZWwgPSBtb2RlbC5hdHRyaWJ1dGUobGFzdFN0ZXApLnZhbHVlKCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHN0ZXBzLmxlbmd0aCA9PT0gMCAmJlxyXG4gICAgICAgICFBcnJheS5pc0FycmF5KG1vZGVsKSAmJlxyXG4gICAgICAgIG1vZGVsICE9PSBudWxsICYmXHJcbiAgICAgICAgdHlwZW9mIG1vZGVsID09PSAnb2JqZWN0JyAmJlxyXG4gICAgICAgIG1vZGVsLmF0dHJpYnV0ZShsYXN0U3RlcCkuZXJyb3JzKCkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIGNvdW50Kys7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBjb3VudDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRFcnJvcnNWMihlcnJvcnM6IEVycm9yc1R5cGVWMik6IHRoaXMge1xyXG4gICAgZm9yIChjb25zdCBlcnJvcnNLZXkgaW4gZXJyb3JzKSB7XHJcbiAgICAgIGlmIChlcnJvcnMuaGFzT3duUHJvcGVydHkoZXJyb3JzS2V5KSkge1xyXG4gICAgICAgIHRoaXMuc2V0RXJyb3JzQnlQYXRoKGVycm9yc1tlcnJvcnNLZXldLmF0dHJpYnV0ZSwgLi4uZXJyb3JzW2Vycm9yc0tleV0ubWVzc2FnZXMpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG5cclxuICBwdWJsaWMgZ2V0RGF0YVRvU2F2ZSgpOiBvYmplY3Qge1xyXG4gICAgY29uc3QgZGF0YSA9IHt9O1xyXG5cclxuICAgIE9iamVjdFxyXG4gICAgICAua2V5cyh0aGlzLnNjaGVtYSgpKVxyXG4gICAgICAuZm9yRWFjaCgoYXR0ck5hbWUpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5zY2hlbWEoKVthdHRyTmFtZV0uJHMpIHtcclxuICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5hdHRyaWJ1dGUoYXR0ck5hbWUpLnZhbHVlKCk7XHJcbiAgICAgICAgICBkYXRhW2F0dHJOYW1lXSA9ICh0eXBlb2YgdmFsdWUuZ2V0RGF0YVRvU2F2ZSA9PT0gJ2Z1bmN0aW9uJyA/IHZhbHVlLmdldERhdGFUb1NhdmUoKSA6IHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIE9iamVjdC5rZXlzKGRhdGEpLmZvckVhY2goKGtleSkgPT4ge1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBkYXRhW2tleV0gPT09ICdvYmplY3QnICYmIE9iamVjdC5rZXlzKGRhdGFba2V5XSkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgZGVsZXRlIGRhdGFba2V5XTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZGF0YVtrZXldKSAmJiBkYXRhW2tleV0ubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgZGVsZXRlIGRhdGFba2V5XTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCFkYXRhW2tleV0pIHtcclxuICAgICAgICBkZWxldGUgZGF0YVtrZXldO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIHNldFNjZW5hcmlvKCkge1xyXG5cclxuICB9XHJcblxyXG4gIHB1YmxpYyBlbnVtKG5hbWUpOiBFbnVtTW9kZWwge1xyXG4gICAgaWYgKCF0aGlzLmVudW1zLmhhc093blByb3BlcnR5KG5hbWUpKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihgZW51bXMgaGF2ZSBubyBwcm9wZXJ0eSAke25hbWV9YCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuZW51bXNbbmFtZV07XHJcbiAgfVxyXG59XHJcbiJdfQ==