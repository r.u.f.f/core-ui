var EnumModel = /** @class */ (function () {
    function EnumModel(enumerable, textList) {
        var _this = this;
        this.vll = [];
        this.enumerable = enumerable;
        this.textList = textList;
        this.allValuesList = Object.keys(this.enumerable).map(function (key) { return _this.enum[key]; });
        this.allKeysList = Object.keys(this.enumerable);
    }
    Object.defineProperty(EnumModel.prototype, "enum", {
        get: function () {
            return this.enumerable;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EnumModel.prototype, "wordings", {
        get: function () {
            return this.textList;
        },
        enumerable: true,
        configurable: true
    });
    EnumModel.prototype.getAllValuesList = function () {
        return this.allValuesList;
    };
    EnumModel.prototype.getValueLabelList = function () {
        var _this = this;
        if (this.vll.length === 0) {
            this.allValuesList.forEach(function (val) {
                _this.vll.push({
                    value: val,
                    label: _this.wordings[val]
                });
            });
        }
        return this.vll;
    };
    EnumModel.prototype.getTextByValue = function (value) {
        if (!this.wordings[value]) {
            return 'Not in Enum!';
        }
        return this.wordings[value];
    };
    EnumModel.prototype.getTextByKey = function (key) {
        if (!this.wordings[this.enum[key]]) {
            return "Not wording for " + key + "! EnumModel: " + this + ".";
        }
        return this.wordings[this.enum[key]];
    };
    EnumModel.prototype.getValueByKey = function (key) {
        return this.enum[key];
    };
    return EnumModel;
}());
export { EnumModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW51bS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtdWkvIiwic291cmNlcyI6WyJsaWIvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscy9lbnVtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBUUksbUJBQVksVUFBVSxFQUFFLFFBQVE7UUFBaEMsaUJBS0M7UUFQTyxRQUFHLEdBQWdELEVBQUUsQ0FBQztRQUcxRCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQWQsQ0FBYyxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsc0JBQUksMkJBQUk7YUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLCtCQUFRO2FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7SUFFRCxvQ0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELHFDQUFpQixHQUFqQjtRQUFBLGlCQVlDO1FBWEcsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHO2dCQUUxQixLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztvQkFDVixLQUFLLEVBQUUsR0FBRztvQkFDVixLQUFLLEVBQUUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7aUJBQzVCLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDcEIsQ0FBQztJQUVELGtDQUFjLEdBQWQsVUFBZSxLQUFzQjtRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN2QixPQUFPLGNBQWMsQ0FBQztTQUN6QjtRQUVELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsZ0NBQVksR0FBWixVQUFhLEdBQVc7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ2hDLE9BQU8scUJBQW1CLEdBQUcscUJBQWdCLElBQUksTUFBRyxDQUFDO1NBQ3hEO1FBRUQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsaUNBQWEsR0FBYixVQUFjLEdBQUc7UUFDYixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQTVERCxJQTREQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBFbnVtTW9kZWwge1xyXG4gICAgcHJpdmF0ZSByZWFkb25seSBlbnVtZXJhYmxlO1xyXG4gICAgcHJpdmF0ZSByZWFkb25seSB0ZXh0TGlzdDtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgYWxsVmFsdWVzTGlzdDtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgYWxsS2V5c0xpc3Q7XHJcblxyXG4gICAgcHJpdmF0ZSB2bGw6IHsgdmFsdWU6IG51bWJlciB8IHN0cmluZywgbGFiZWw6IHN0cmluZyB9W10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihlbnVtZXJhYmxlLCB0ZXh0TGlzdCkge1xyXG4gICAgICAgIHRoaXMuZW51bWVyYWJsZSA9IGVudW1lcmFibGU7XHJcbiAgICAgICAgdGhpcy50ZXh0TGlzdCA9IHRleHRMaXN0O1xyXG4gICAgICAgIHRoaXMuYWxsVmFsdWVzTGlzdCA9IE9iamVjdC5rZXlzKHRoaXMuZW51bWVyYWJsZSkubWFwKChrZXkpID0+IHRoaXMuZW51bVtrZXldKTtcclxuICAgICAgICB0aGlzLmFsbEtleXNMaXN0ID0gT2JqZWN0LmtleXModGhpcy5lbnVtZXJhYmxlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZW51bSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbnVtZXJhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB3b3JkaW5ncygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50ZXh0TGlzdDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxWYWx1ZXNMaXN0KCk6IEFycmF5PG51bWJlciB8IHN0cmluZz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsbFZhbHVlc0xpc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVMYWJlbExpc3QoKTogQXJyYXk8eyB2YWx1ZTogbnVtYmVyIHwgc3RyaW5nLCBsYWJlbDogc3RyaW5nIH0+IHtcclxuICAgICAgICBpZiAodGhpcy52bGwubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWxsVmFsdWVzTGlzdC5mb3JFYWNoKHZhbCA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy52bGwucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHZhbCxcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbDogdGhpcy53b3JkaW5nc1t2YWxdXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy52bGw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGV4dEJ5VmFsdWUodmFsdWU6IG51bWJlciB8IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKCF0aGlzLndvcmRpbmdzW3ZhbHVlXSkge1xyXG4gICAgICAgICAgICByZXR1cm4gJ05vdCBpbiBFbnVtISc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy53b3JkaW5nc1t2YWx1ZV07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGV4dEJ5S2V5KGtleTogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAoIXRoaXMud29yZGluZ3NbdGhpcy5lbnVtW2tleV1dKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBgTm90IHdvcmRpbmcgZm9yICR7a2V5fSEgRW51bU1vZGVsOiAke3RoaXN9LmA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy53b3JkaW5nc1t0aGlzLmVudW1ba2V5XV07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVCeUtleShrZXkpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbnVtW2tleV07XHJcbiAgICB9XHJcbn1cclxuIl19