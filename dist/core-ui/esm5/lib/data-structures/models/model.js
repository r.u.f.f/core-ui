import { __extends } from "tslib";
import { ABaseModel } from 'projects/core-ui/src/lib/data-structures/models/base.model';
var AModel = /** @class */ (function (_super) {
    __extends(AModel, _super);
    function AModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @inheritDoc
     */
    AModel.prototype.dataProvider = function () {
        if (typeof this['$dp'] === 'object') {
            return this['$dp'];
        }
        throw new Error('You should use any of data provider decorators');
    };
    /**
     * @inheritDoc
     */
    AModel.prototype.pk = function () {
        if (this.schema()['id'] === undefined) {
            throw new Error("Could not to find default primary key in model.\nYou must redeclare pk() in model or define 'id' field in schema.");
        }
        return 'id';
    };
    /**
     * @inheritDoc
     */
    AModel.prototype.requestFormatter = function () {
        return {};
    };
    AModel.prototype.getDataToSave = function () {
        return _super.prototype.getDataToSave.call(this);
    };
    return AModel;
}(ABaseModel));
export { AModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLXVpLyIsInNvdXJjZXMiOlsibGliL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMvbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSw0REFBNEQsQ0FBQztBQUl0RjtJQUFxQywwQkFBVTtJQUEvQzs7SUFrQ0EsQ0FBQztJQWpDRzs7T0FFRztJQUNILDZCQUFZLEdBQVo7UUFDSSxJQUFJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLFFBQVEsRUFBRTtZQUNqQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN0QjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBRUQ7O09BRUc7SUFDSCxtQkFBRSxHQUFGO1FBQ0ksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssU0FBUyxFQUFFO1lBQ25DLE1BQU0sSUFBSSxLQUFLLENBQ1gsbUhBQ2lELENBQ3BELENBQUM7U0FDTDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNILGlDQUFnQixHQUFoQjtRQUNJLE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVELDhCQUFhLEdBQWI7UUFDSSxPQUFPLGlCQUFNLGFBQWEsV0FBRSxDQUFDO0lBQ2pDLENBQUM7SUFDTCxhQUFDO0FBQUQsQ0FBQyxBQWxDRCxDQUFxQyxVQUFVLEdBa0M5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QUJhc2VNb2RlbH0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2RhdGEtc3RydWN0dXJlcy9tb2RlbHMvYmFzZS5tb2RlbCc7XHJcbmltcG9ydCB7SU1vZGVsfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXN0cnVjdHVyZXMvbW9kZWxzL0lNb2RlbCc7XHJcbmltcG9ydCB7SU1vZGVsRGF0YVByb3ZpZGVyfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9kYXRhLXByb3ZpZGVyL0lNb2RlbERhdGFQcm92aWRlcic7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQU1vZGVsIGV4dGVuZHMgQUJhc2VNb2RlbCBpbXBsZW1lbnRzIElNb2RlbCB7XHJcbiAgICAvKipcclxuICAgICAqIEBpbmhlcml0RG9jXHJcbiAgICAgKi9cclxuICAgIGRhdGFQcm92aWRlcigpOiBJTW9kZWxEYXRhUHJvdmlkZXIge1xyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpc1snJGRwJ10gPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzWyckZHAnXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdZb3Ugc2hvdWxkIHVzZSBhbnkgb2YgZGF0YSBwcm92aWRlciBkZWNvcmF0b3JzJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAaW5oZXJpdERvY1xyXG4gICAgICovXHJcbiAgICBwaygpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmICh0aGlzLnNjaGVtYSgpWydpZCddID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxyXG4gICAgICAgICAgICAgICAgYENvdWxkIG5vdCB0byBmaW5kIGRlZmF1bHQgcHJpbWFyeSBrZXkgaW4gbW9kZWwuXHJcbllvdSBtdXN0IHJlZGVjbGFyZSBwaygpIGluIG1vZGVsIG9yIGRlZmluZSAnaWQnIGZpZWxkIGluIHNjaGVtYS5gXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAnaWQnO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGluaGVyaXREb2NcclxuICAgICAqL1xyXG4gICAgcmVxdWVzdEZvcm1hdHRlcigpOiBvYmplY3Qge1xyXG4gICAgICAgIHJldHVybiB7fTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXRhVG9TYXZlKCk6IG9iamVjdCB7XHJcbiAgICAgICAgcmV0dXJuIHN1cGVyLmdldERhdGFUb1NhdmUoKTtcclxuICAgIH1cclxufVxyXG4iXX0=