export var FilterType;
(function (FilterType) {
    FilterType["EQ"] = "eq";
    FilterType["NEQ"] = "neq";
    FilterType["IN"] = "in";
    FilterType["IN_ENUM"] = "in_enum";
    FilterType["ENUM"] = "enum";
    FilterType["GT"] = "gt";
    FilterType["LT"] = "lt";
    FilterType["GTE"] = "gte";
    FilterType["LTE"] = "lte";
    FilterType["ILIKE"] = "ilike";
    FilterType["RANGE"] = "range";
    FilterType["DATE"] = "date";
})(FilterType || (FilterType = {}));
var FilterAttribute = /** @class */ (function () {
    function FilterAttribute(lbl, initVal) {
        this.lbl = lbl;
        this.val = null;
        if (initVal !== undefined) {
            this.val = initVal;
        }
    }
    FilterAttribute.prototype.description = function () {
        return '';
    };
    FilterAttribute.prototype.errors = function () {
        return [];
    };
    FilterAttribute.prototype.is = function (val) {
        return val === this.val;
    };
    FilterAttribute.prototype.label = function () {
        return this.lbl;
    };
    FilterAttribute.prototype.setValue = function (value) {
        this.val = value;
        return this;
    };
    FilterAttribute.prototype.value = function () {
        return this.val;
    };
    return FilterAttribute;
}());
var FilterItem = /** @class */ (function () {
    function FilterItem(setting) {
        this.setting = setting;
        this.val = null;
        this.minVal = null;
        this.maxVal = null;
        this.enumVal = {};
    }
    FilterItem.prototype.getAlias = function () {
        try {
            return this.setting.alias;
        }
        catch (e) {
            return '';
        }
    };
    FilterItem.prototype.getName = function () {
        return this.setting.name;
    };
    FilterItem.prototype.getType = function () {
        return this.setting.type;
    };
    FilterItem.prototype.max = function () {
        if (this.maxVal === null) {
            this.maxVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === FilterType.DATE ? 0 : null);
        }
        return this.maxVal;
    };
    FilterItem.prototype.min = function () {
        if (this.minVal === null) {
            this.minVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === FilterType.DATE ? 0 : null);
        }
        return this.minVal;
    };
    FilterItem.prototype.vEnum = function (index) {
        if (!this.enumVal.hasOwnProperty(index)) {
            this.enumVal[index] = new FilterAttribute(this.setting.enum.getTextByKey(index), false);
        }
        return this.enumVal[index];
    };
    FilterItem.prototype.v = function () {
        if (this.val === null) {
            this.val = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name);
        }
        return this.val;
    };
    FilterItem.prototype.getEnum = function () {
        return this.setting.enum;
    };
    FilterItem.prototype.toResponse = function () {
        var _this = this;
        var result = {
            type: this.getType(),
            name: this.setting.name,
            alias: this.setting.alias,
        };
        switch (this.getType()) {
            case FilterType.IN_ENUM:
                result.val = Object
                    .keys(this.enumVal)
                    .map(function (key) {
                    return {
                        key: key,
                        enabled: !!_this.enumVal[key].value()
                    };
                })
                    .filter(function (i) {
                    return i.enabled === true;
                })
                    .map(function (i) {
                    console.log(i.key);
                    return _this.setting.enum.enum[i.key];
                });
                break;
            case FilterType.DATE:
                result.minVal = this.min().value();
                result.maxVal = this.max().value();
                if (result.minVal !== 0 && (result.minVal === result.maxVal)) {
                    result.maxVal += (3600 * 24) - 1; // adding one day minus last second
                }
                /* if (this.min().value() < this.max().value()) {
                   result.minVal = this.min().value();
                   result.maxVal = this.max().value();
                 } else {
                   result.minVal = this.max().value();
                   result.maxVal = this.min().value();
                 }*/
                break;
            default:
                result.val = this.v().value();
                break;
        }
        return result;
    };
    return FilterItem;
}());
export { FilterItem };
var FilterSettingsFactory = /** @class */ (function () {
    function FilterSettingsFactory() {
    }
    FilterSettingsFactory.fromSettings = function () {
        var settings = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            settings[_i] = arguments[_i];
        }
        return settings.map(function (item) {
            return new FilterItem(item);
        });
    };
    FilterSettingsFactory.fromSettingsAndModel = function (model) {
        var settings = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            settings[_i - 1] = arguments[_i];
        }
        return settings.map(function (item) {
            item.label = model.attribute(item.name).label();
            return new FilterItem(item);
        });
    };
    return FilterSettingsFactory;
}());
export { FilterSettingsFactory };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSUZpbHRlclNldHRpbmdzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2VzL2RhdGEtc3RydWN0dXJlcy9xdWVyaWVzL0lGaWx0ZXJTZXR0aW5ncy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQSxNQUFNLENBQU4sSUFBWSxVQWFYO0FBYkQsV0FBWSxVQUFVO0lBQ2xCLHVCQUFTLENBQUE7SUFDVCx5QkFBVyxDQUFBO0lBQ1gsdUJBQVMsQ0FBQTtJQUNULGlDQUFtQixDQUFBO0lBQ25CLDJCQUFhLENBQUE7SUFDYix1QkFBUyxDQUFBO0lBQ1QsdUJBQVMsQ0FBQTtJQUNULHlCQUFXLENBQUE7SUFDWCx5QkFBVyxDQUFBO0lBQ1gsNkJBQWUsQ0FBQTtJQUNmLDZCQUFlLENBQUE7SUFDZiwyQkFBYSxDQUFBO0FBQ2pCLENBQUMsRUFiVyxVQUFVLEtBQVYsVUFBVSxRQWFyQjtBQXlDRDtJQUlJLHlCQUFvQixHQUFXLEVBQUUsT0FBYTtRQUExQixRQUFHLEdBQUgsR0FBRyxDQUFRO1FBRnZCLFFBQUcsR0FBUSxJQUFJLENBQUM7UUFHcEIsSUFBSSxPQUFPLEtBQUssU0FBUyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDSSxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxnQ0FBTSxHQUFOO1FBQ0ksT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsNEJBQUUsR0FBRixVQUFHLEdBQVE7UUFDUCxPQUFPLEdBQUcsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQzVCLENBQUM7SUFFRCwrQkFBSyxHQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxrQ0FBUSxHQUFSLFVBQVMsS0FBVTtRQUNmLElBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCwrQkFBSyxHQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFTCxzQkFBQztBQUFELENBQUMsQUFuQ0QsSUFtQ0M7QUFFRDtJQVFJLG9CQUFvQixPQUF3QjtRQUF4QixZQUFPLEdBQVAsT0FBTyxDQUFpQjtRQVBwQyxRQUFHLEdBQXNCLElBQUksQ0FBQztRQUM5QixXQUFNLEdBQXNCLElBQUksQ0FBQztRQUNqQyxXQUFNLEdBQXNCLElBQUksQ0FBQztRQUVqQyxZQUFPLEdBQThFLEVBQUUsQ0FBQztJQUtoRyxDQUFDO0lBRUQsNkJBQVEsR0FBUjtRQUNJLElBQUk7WUFDQSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1NBQzdCO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0wsQ0FBQztJQUVELDRCQUFPLEdBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCw0QkFBTyxHQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRUQsd0JBQUcsR0FBSDtRQUNJLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGVBQWUsQ0FDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFDM0QsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNoRCxDQUFDO1NBQ0w7UUFDRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELHdCQUFHLEdBQUg7UUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxlQUFlLENBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQzNELElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FDaEQsQ0FBQztTQUNMO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCwwQkFBSyxHQUFMLFVBQU0sS0FBc0I7UUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxlQUFlLENBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFlLENBQVcsRUFDekQsS0FBSyxDQUNSLENBQUM7U0FDTDtRQUNELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsc0JBQUMsR0FBRDtRQUNJLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxJQUFJLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLGVBQWUsQ0FDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FDOUQsQ0FBQztTQUNMO1FBQ0QsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFTSw0QkFBTyxHQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRU0sK0JBQVUsR0FBakI7UUFBQSxpQkE2Q0M7UUEzQ0csSUFBTSxNQUFNLEdBQVU7WUFDbEIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDcEIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1NBQzVCLENBQUM7UUFFRixRQUFRLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNwQixLQUFLLFVBQVUsQ0FBQyxPQUFPO2dCQUNuQixNQUFNLENBQUMsR0FBRyxHQUFHLE1BQU07cUJBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7cUJBQ2xCLEdBQUcsQ0FBQyxVQUFDLEdBQVc7b0JBQ2IsT0FBTzt3QkFDSCxHQUFHLEtBQUE7d0JBQ0gsT0FBTyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRTtxQkFDdkMsQ0FBQztnQkFDTixDQUFDLENBQUM7cUJBQ0QsTUFBTSxDQUFDLFVBQUMsQ0FBQztvQkFDTixPQUFPLENBQUMsQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDO2dCQUM5QixDQUFDLENBQUM7cUJBQ0QsR0FBRyxDQUFDLFVBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbkIsT0FBTyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QyxDQUFDLENBQUMsQ0FBQztnQkFDUCxNQUFNO1lBQ1YsS0FBSyxVQUFVLENBQUMsSUFBSTtnQkFDaEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ25DLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNuQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sS0FBSyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQzFELE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsbUNBQW1DO2lCQUN4RTtnQkFDRDs7Ozs7O29CQU1JO2dCQUNKLE1BQU07WUFDVjtnQkFDSSxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDOUIsTUFBTTtTQUNiO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUNMLGlCQUFDO0FBQUQsQ0FBQyxBQXJIRCxJQXFIQzs7QUFHRDtJQUFBO0lBYUEsQ0FBQztJQVpVLGtDQUFZLEdBQW5CO1FBQW9CLGtCQUE4QjthQUE5QixVQUE4QixFQUE5QixxQkFBOEIsRUFBOUIsSUFBOEI7WUFBOUIsNkJBQThCOztRQUM5QyxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFxQjtZQUN0QyxPQUFPLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDBDQUFvQixHQUEzQixVQUE0QixLQUFpQjtRQUFFLGtCQUE4QjthQUE5QixVQUE4QixFQUE5QixxQkFBOEIsRUFBOUIsSUFBOEI7WUFBOUIsaUNBQThCOztRQUN6RSxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFxQjtZQUN0QyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hELE9BQU8sSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wsNEJBQUM7QUFBRCxDQUFDLEFBYkQsSUFhQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RW51bU1vZGVsfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscyc7XHJcbmltcG9ydCB7SUJhc2VNb2RlbH0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL21vZGVscyc7XHJcbmltcG9ydCB7SUF0dHJpYnV0ZX0gZnJvbSAncHJvamVjdHMvY29yZS11aS9zcmMvbGliL2ludGVyZmFjZXMvZGF0YS1zdHJ1Y3R1cmVzL2F0dHJpYnV0ZXMnO1xyXG5cclxuZXhwb3J0IGVudW0gRmlsdGVyVHlwZSB7XHJcbiAgICBFUSA9ICdlcScsXHJcbiAgICBORVEgPSAnbmVxJyxcclxuICAgIElOID0gJ2luJyxcclxuICAgIElOX0VOVU0gPSAnaW5fZW51bScsXHJcbiAgICBFTlVNID0gJ2VudW0nLFxyXG4gICAgR1QgPSAnZ3QnLFxyXG4gICAgTFQgPSAnbHQnLFxyXG4gICAgR1RFID0gJ2d0ZScsXHJcbiAgICBMVEUgPSAnbHRlJyxcclxuICAgIElMSUtFID0gJ2lsaWtlJyxcclxuICAgIFJBTkdFID0gJ3JhbmdlJyxcclxuICAgIERBVEUgPSAnZGF0ZScsXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSVJlc3Age1xyXG4gICAgdHlwZTogRmlsdGVyVHlwZTtcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIGFsaWFzPzogc3RyaW5nO1xyXG4gICAgdmFsPzogYW55O1xyXG4gICAgbWluVmFsPzogYW55O1xyXG4gICAgbWF4VmFsPzogYW55O1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElGaWx0ZXJJdGVtIHtcclxuXHJcbiAgICBnZXRFbnVtKCk6IEVudW1Nb2RlbDtcclxuXHJcbiAgICBnZXRBbGlhcygpOiBzdHJpbmc7XHJcblxyXG4gICAgZ2V0TmFtZSgpOiBzdHJpbmc7XHJcblxyXG4gICAgZ2V0VHlwZSgpOiBGaWx0ZXJUeXBlO1xyXG5cclxuICAgIHYoKTogSUF0dHJpYnV0ZTtcclxuXHJcbiAgICB2RW51bShpbmRleDogc3RyaW5nIHwgbnVtYmVyKTogSUF0dHJpYnV0ZTtcclxuXHJcbiAgICBtaW4oKTogSUF0dHJpYnV0ZTtcclxuXHJcbiAgICBtYXgoKTogSUF0dHJpYnV0ZTtcclxuXHJcbiAgICB0b1Jlc3BvbnNlKCk6IElSZXNwO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElGaWx0ZXJTZXR0aW5ncyB7XHJcbiAgICB0eXBlOiBGaWx0ZXJUeXBlO1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgZW51bT86IEVudW1Nb2RlbDtcclxuICAgIGxhYmVsPzogc3RyaW5nO1xyXG4gICAgYWxpYXM/OiBzdHJpbmc7XHJcbn1cclxuXHJcblxyXG5jbGFzcyBGaWx0ZXJBdHRyaWJ1dGUgaW1wbGVtZW50cyBJQXR0cmlidXRlIHtcclxuXHJcbiAgICBwcml2YXRlIHZhbDogYW55ID0gbnVsbDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxibDogc3RyaW5nLCBpbml0VmFsPzogYW55KSB7XHJcbiAgICAgICAgaWYgKGluaXRWYWwgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLnZhbCA9IGluaXRWYWw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRlc2NyaXB0aW9uKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGVycm9ycygpOiBBcnJheTxzdHJpbmc+IHtcclxuICAgICAgICByZXR1cm4gW107XHJcbiAgICB9XHJcblxyXG4gICAgaXModmFsOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdmFsID09PSB0aGlzLnZhbDtcclxuICAgIH1cclxuXHJcbiAgICBsYWJlbCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxibDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRWYWx1ZSh2YWx1ZTogYW55KTogdGhpcyB7XHJcbiAgICAgICAgdGhpcy52YWwgPSB2YWx1ZTtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICB2YWx1ZSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJJdGVtIGltcGxlbWVudHMgSUZpbHRlckl0ZW0ge1xyXG4gICAgcHJpdmF0ZSB2YWw6IElBdHRyaWJ1dGUgfCBudWxsID0gbnVsbDtcclxuICAgIHByaXZhdGUgbWluVmFsOiBJQXR0cmlidXRlIHwgbnVsbCA9IG51bGw7XHJcbiAgICBwcml2YXRlIG1heFZhbDogSUF0dHJpYnV0ZSB8IG51bGwgPSBudWxsO1xyXG5cclxuICAgIHByaXZhdGUgZW51bVZhbDogeyBbYXR0cmlidXRlOiBzdHJpbmddOiBJQXR0cmlidXRlIH0gfCB7IFthdHRyaWJ1dGU6IG51bWJlcl06IElBdHRyaWJ1dGUgfSA9IHt9O1xyXG5cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNldHRpbmc6IElGaWx0ZXJTZXR0aW5ncykge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGlhcygpOiBzdHJpbmcge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNldHRpbmcuYWxpYXM7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldE5hbWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5nLm5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VHlwZSgpOiBGaWx0ZXJUeXBlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5nLnR5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgbWF4KCk6IElBdHRyaWJ1dGUge1xyXG4gICAgICAgIGlmICh0aGlzLm1heFZhbCA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICB0aGlzLm1heFZhbCA9IG5ldyBGaWx0ZXJBdHRyaWJ1dGUoXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldHRpbmcubGFiZWwgPyB0aGlzLnNldHRpbmcubGFiZWwgOiB0aGlzLnNldHRpbmcubmFtZSxcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VHlwZSgpID09PSBGaWx0ZXJUeXBlLkRBVEUgPyAwIDogbnVsbCxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubWF4VmFsO1xyXG4gICAgfVxyXG5cclxuICAgIG1pbigpOiBJQXR0cmlidXRlIHtcclxuICAgICAgICBpZiAodGhpcy5taW5WYWwgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5taW5WYWwgPSBuZXcgRmlsdGVyQXR0cmlidXRlKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXR0aW5nLmxhYmVsID8gdGhpcy5zZXR0aW5nLmxhYmVsIDogdGhpcy5zZXR0aW5nLm5hbWUsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFR5cGUoKSA9PT0gRmlsdGVyVHlwZS5EQVRFID8gMCA6IG51bGwsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLm1pblZhbDtcclxuICAgIH1cclxuXHJcbiAgICB2RW51bShpbmRleDogc3RyaW5nIHwgbnVtYmVyKTogSUF0dHJpYnV0ZSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmVudW1WYWwuaGFzT3duUHJvcGVydHkoaW5kZXgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW51bVZhbFtpbmRleF0gPSBuZXcgRmlsdGVyQXR0cmlidXRlKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXR0aW5nLmVudW0uZ2V0VGV4dEJ5S2V5KGluZGV4IGFzIHN0cmluZykgYXMgc3RyaW5nLFxyXG4gICAgICAgICAgICAgICAgZmFsc2VcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZW51bVZhbFtpbmRleF07XHJcbiAgICB9XHJcblxyXG4gICAgdigpOiBJQXR0cmlidXRlIHtcclxuICAgICAgICBpZiAodGhpcy52YWwgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy52YWwgPSBuZXcgRmlsdGVyQXR0cmlidXRlKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXR0aW5nLmxhYmVsID8gdGhpcy5zZXR0aW5nLmxhYmVsIDogdGhpcy5zZXR0aW5nLm5hbWUsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0RW51bSgpOiBFbnVtTW9kZWwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNldHRpbmcuZW51bTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdG9SZXNwb25zZSgpOiBJUmVzcCB7XHJcblxyXG4gICAgICAgIGNvbnN0IHJlc3VsdDogSVJlc3AgPSB7XHJcbiAgICAgICAgICAgIHR5cGU6IHRoaXMuZ2V0VHlwZSgpLFxyXG4gICAgICAgICAgICBuYW1lOiB0aGlzLnNldHRpbmcubmFtZSxcclxuICAgICAgICAgICAgYWxpYXM6IHRoaXMuc2V0dGluZy5hbGlhcyxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBzd2l0Y2ggKHRoaXMuZ2V0VHlwZSgpKSB7XHJcbiAgICAgICAgICAgIGNhc2UgRmlsdGVyVHlwZS5JTl9FTlVNOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0LnZhbCA9IE9iamVjdFxyXG4gICAgICAgICAgICAgICAgICAgIC5rZXlzKHRoaXMuZW51bVZhbClcclxuICAgICAgICAgICAgICAgICAgICAubWFwKChrZXk6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZW5hYmxlZDogISF0aGlzLmVudW1WYWxba2V5XS52YWx1ZSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKChpKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBpLmVuYWJsZWQgPT09IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAubWFwKChpKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGkua2V5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZy5lbnVtLmVudW1baS5rZXldO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgRmlsdGVyVHlwZS5EQVRFOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0Lm1pblZhbCA9IHRoaXMubWluKCkudmFsdWUoKTtcclxuICAgICAgICAgICAgICAgIHJlc3VsdC5tYXhWYWwgPSB0aGlzLm1heCgpLnZhbHVlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzdWx0Lm1pblZhbCAhPT0gMCAmJiAocmVzdWx0Lm1pblZhbCA9PT0gcmVzdWx0Lm1heFZhbCkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQubWF4VmFsICs9ICgzNjAwICogMjQpIC0gMTsgLy8gYWRkaW5nIG9uZSBkYXkgbWludXMgbGFzdCBzZWNvbmRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8qIGlmICh0aGlzLm1pbigpLnZhbHVlKCkgPCB0aGlzLm1heCgpLnZhbHVlKCkpIHtcclxuICAgICAgICAgICAgICAgICAgIHJlc3VsdC5taW5WYWwgPSB0aGlzLm1pbigpLnZhbHVlKCk7XHJcbiAgICAgICAgICAgICAgICAgICByZXN1bHQubWF4VmFsID0gdGhpcy5tYXgoKS52YWx1ZSgpO1xyXG4gICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICByZXN1bHQubWluVmFsID0gdGhpcy5tYXgoKS52YWx1ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgcmVzdWx0Lm1heFZhbCA9IHRoaXMubWluKCkudmFsdWUoKTtcclxuICAgICAgICAgICAgICAgICB9Ki9cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0LnZhbCA9IHRoaXMudigpLnZhbHVlKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJTZXR0aW5nc0ZhY3Rvcnkge1xyXG4gICAgc3RhdGljIGZyb21TZXR0aW5ncyguLi5zZXR0aW5nczogSUZpbHRlclNldHRpbmdzW10pOiBJRmlsdGVySXRlbVtdIHtcclxuICAgICAgICByZXR1cm4gc2V0dGluZ3MubWFwKChpdGVtOiBJRmlsdGVyU2V0dGluZ3MpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBGaWx0ZXJJdGVtKGl0ZW0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBmcm9tU2V0dGluZ3NBbmRNb2RlbChtb2RlbDogSUJhc2VNb2RlbCwgLi4uc2V0dGluZ3M6IElGaWx0ZXJTZXR0aW5nc1tdKTogSUZpbHRlckl0ZW1bXSB7XHJcbiAgICAgICAgcmV0dXJuIHNldHRpbmdzLm1hcCgoaXRlbTogSUZpbHRlclNldHRpbmdzKSA9PiB7XHJcbiAgICAgICAgICAgIGl0ZW0ubGFiZWwgPSBtb2RlbC5hdHRyaWJ1dGUoaXRlbS5uYW1lKS5sYWJlbCgpO1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IEZpbHRlckl0ZW0oaXRlbSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19