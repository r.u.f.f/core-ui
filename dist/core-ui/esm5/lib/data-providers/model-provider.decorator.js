import { __extends } from "tslib";
import { ServiceLocator } from 'projects/core-ui/src/lib/locators/service.locator';
export function ModelProviderDecorator(options) {
    return function (constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.$dp = ServiceLocator.getDataProviderFactory().model(options.endpoint, _this);
                return _this;
            }
            return class_1;
        }(constructor));
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwtcHJvdmlkZXIuZGVjb3JhdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29yZS11aS8iLCJzb3VyY2VzIjpbImxpYi9kYXRhLXByb3ZpZGVycy9tb2RlbC1wcm92aWRlci5kZWNvcmF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxtREFBbUQsQ0FBQztBQUVqRixNQUFNLFVBQVUsc0JBQXNCLENBQUMsT0FHdEM7SUFFQyxPQUFPLFVBQWlELFdBQWM7UUFDcEU7WUFBcUIsMkJBQVc7WUFBekI7Z0JBQUEscUVBRU47Z0JBREMsU0FBRyxHQUFHLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxDQUFDOztZQUM5RSxDQUFDO1lBQUQsY0FBQztRQUFELENBQUMsQUFGTSxDQUFjLFdBQVcsR0FFOUI7SUFDSixDQUFDLENBQUM7QUFDSixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJRW5kcG9pbnR9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9pbnRlcmZhY2VzL2VuZHBvaW50L0lFbmRwb2ludCc7XHJcbmltcG9ydCB7U2VydmljZUxvY2F0b3J9IGZyb20gJ3Byb2plY3RzL2NvcmUtdWkvc3JjL2xpYi9sb2NhdG9ycy9zZXJ2aWNlLmxvY2F0b3InO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIE1vZGVsUHJvdmlkZXJEZWNvcmF0b3Iob3B0aW9uczoge1xyXG4gIGVuZHBvaW50OiBJRW5kcG9pbnQsXHJcbiAgYXBpVXJsPzogc3RyaW5nXHJcbn0pIHtcclxuXHJcbiAgcmV0dXJuIGZ1bmN0aW9uIDxUIGV4dGVuZHMgeyBuZXcoLi4uYXJnczogYW55W10pOiB7fSB9Pihjb25zdHJ1Y3RvcjogVCkge1xyXG4gICAgcmV0dXJuIGNsYXNzIGV4dGVuZHMgY29uc3RydWN0b3Ige1xyXG4gICAgICAkZHAgPSBTZXJ2aWNlTG9jYXRvci5nZXREYXRhUHJvdmlkZXJGYWN0b3J5KCkubW9kZWwob3B0aW9ucy5lbmRwb2ludCwgdGhpcyk7XHJcbiAgICB9O1xyXG4gIH07XHJcbn1cclxuIl19