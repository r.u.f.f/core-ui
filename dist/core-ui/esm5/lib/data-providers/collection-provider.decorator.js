import { __extends } from "tslib";
import { ServiceLocator } from 'projects/core-ui/src/lib/locators/service.locator';
export function CollectionProviderDecorator(options) {
    return function (constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.$dp = ServiceLocator.getDataProviderFactory().collection(options.endpoint, _this);
                return _this;
            }
            return class_1;
        }(constructor));
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi1wcm92aWRlci5kZWNvcmF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLXVpLyIsInNvdXJjZXMiOlsibGliL2RhdGEtcHJvdmlkZXJzL2NvbGxlY3Rpb24tcHJvdmlkZXIuZGVjb3JhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFFakYsTUFBTSxVQUFVLDJCQUEyQixDQUFDLE9BRzNDO0lBQ0MsT0FBTyxVQUFpRCxXQUFjO1FBQ3BFO1lBQXFCLDJCQUFXO1lBQXpCO2dCQUFBLHFFQUVOO2dCQURDLFNBQUcsR0FBRyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxLQUFJLENBQUMsQ0FBQzs7WUFDbkYsQ0FBQztZQUFELGNBQUM7UUFBRCxDQUFDLEFBRk0sQ0FBYyxXQUFXLEdBRTlCO0lBQ0osQ0FBQyxDQUFDO0FBQ0osQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SUVuZHBvaW50fSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvaW50ZXJmYWNlcy9lbmRwb2ludC9JRW5kcG9pbnQnO1xyXG5pbXBvcnQge1NlcnZpY2VMb2NhdG9yfSBmcm9tICdwcm9qZWN0cy9jb3JlLXVpL3NyYy9saWIvbG9jYXRvcnMvc2VydmljZS5sb2NhdG9yJztcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBDb2xsZWN0aW9uUHJvdmlkZXJEZWNvcmF0b3Iob3B0aW9uczoge1xyXG4gIGVuZHBvaW50OiBJRW5kcG9pbnQsXHJcbiAgYXBpVXJsPzogc3RyaW5nXHJcbn0pIHtcclxuICByZXR1cm4gZnVuY3Rpb24gPFQgZXh0ZW5kcyB7IG5ldyguLi5hcmdzOiBhbnlbXSk6IHt9IH0+KGNvbnN0cnVjdG9yOiBUKSB7XHJcbiAgICByZXR1cm4gY2xhc3MgZXh0ZW5kcyBjb25zdHJ1Y3RvciB7XHJcbiAgICAgICRkcCA9IFNlcnZpY2VMb2NhdG9yLmdldERhdGFQcm92aWRlckZhY3RvcnkoKS5jb2xsZWN0aW9uKG9wdGlvbnMuZW5kcG9pbnQsIHRoaXMpO1xyXG4gICAgfTtcclxuICB9O1xyXG59XHJcbiJdfQ==