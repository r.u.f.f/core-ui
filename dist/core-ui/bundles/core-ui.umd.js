(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('projects/core-ui/src/lib/locators/service.locator'), require('projects/core-ui/src/lib/data-structures/attributes/attribute'), require('projects/core-ui/src/lib/data-structures/collections/base.collection'), require('@angular/core'), require('validate.js'), require('projects/core-ui/src/lib/data-structures/attributes'), require('projects/core-ui/src/lib/data-structures/models/base.model'), require('projects/core-ui/src/lib/interfaces/data-structures/queries'), require('@angular/common/http'), require('rxjs'), require('rxjs/operators'), require('projects/core-ui/src/lib/providers/collection-query'), require('projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery'), require('projects/core-ui/src/lib/data-structures/queries')) :
    typeof define === 'function' && define.amd ? define('core-ui', ['exports', 'projects/core-ui/src/lib/locators/service.locator', 'projects/core-ui/src/lib/data-structures/attributes/attribute', 'projects/core-ui/src/lib/data-structures/collections/base.collection', '@angular/core', 'validate.js', 'projects/core-ui/src/lib/data-structures/attributes', 'projects/core-ui/src/lib/data-structures/models/base.model', 'projects/core-ui/src/lib/interfaces/data-structures/queries', '@angular/common/http', 'rxjs', 'rxjs/operators', 'projects/core-ui/src/lib/providers/collection-query', 'projects/core-ui/src/lib/interfaces/data-structures/queries/ICollectionQuery', 'projects/core-ui/src/lib/data-structures/queries'], factory) :
    (global = global || self, factory(global['core-ui'] = {}, global.service_locator, global.attribute, global.base_collection, global.ng.core, global.validate_js, global.attributes, global.base_model, global.queries, global.ng.common.http, global.rxjs, global.rxjs.operators, global.collectionQuery, global.ICollectionQuery, global.queries$1));
}(this, (function (exports, service_locator, attribute, base_collection, core, validate_js, attributes, base_model, queries, http, rxjs, operators, collectionQuery, ICollectionQuery, queries$1) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    function ModelProviderDecorator(options) {
        return function (constructor) {
            return /** @class */ (function (_super) {
                __extends(class_1, _super);
                function class_1() {
                    var _this = _super !== null && _super.apply(this, arguments) || this;
                    _this.$dp = service_locator.ServiceLocator.getDataProviderFactory().model(options.endpoint, _this);
                    return _this;
                }
                return class_1;
            }(constructor));
        };
    }

    function CollectionProviderDecorator(options) {
        return function (constructor) {
            return /** @class */ (function (_super) {
                __extends(class_1, _super);
                function class_1() {
                    var _this = _super !== null && _super.apply(this, arguments) || this;
                    _this.$dp = service_locator.ServiceLocator.getDataProviderFactory().collection(options.endpoint, _this);
                    return _this;
                }
                return class_1;
            }(constructor));
        };
    }

    var Attribute = /** @class */ (function () {
        function Attribute() {
        }
        return Attribute;
    }());

    var AutoAttribute = /** @class */ (function (_super) {
        __extends(AutoAttribute, _super);
        function AutoAttribute(labels, attributes, name, model) {
            var _this = _super.call(this) || this;
            _this.labels = labels;
            _this.attributes = attributes;
            _this.name = name;
            _this.model = model;
            if (!_this.model.schema()[_this.name]) {
                throw new Error("Incorrect implementation. Model " + _this.model.constructor.name + " have no attribute [" + _this.name + "]!");
            }
            return _this;
        }
        AutoAttribute.prototype.setValue = function (value) {
            this.attributes[this.name] = value;
            this.model.onAttributesChanged().emit(this.name);
            return this;
        };
        AutoAttribute.prototype.value = function () {
            return this.attributes[this.name];
        };
        AutoAttribute.prototype.valueByKey = function (key) {
            return this.attributes[this.name][key];
        };
        AutoAttribute.prototype.label = function () {
            return this.labels[this.name];
        };
        AutoAttribute.prototype.errors = function () {
            return this.model.errors(this.name);
        };
        AutoAttribute.prototype.is = function (val) {
            return this.attributes[this.name] === val;
        };
        AutoAttribute.prototype.getName = function () {
            return this.name;
        };
        AutoAttribute.prototype.description = function () {
            return this.model.schema()[this.name].$d ? this.model.schema()[this.name].$d : '';
        };
        return AutoAttribute;
    }(attribute.Attribute));

    var ReadOnlyAttribute = /** @class */ (function () {
        function ReadOnlyAttribute(lbl, val) {
            this.lbl = lbl;
            this.val = val;
            this.errs = [];
        }
        ReadOnlyAttribute.prototype.label = function () {
            return this.lbl;
        };
        ReadOnlyAttribute.prototype.setValue = function (value) {
            return this;
        };
        ReadOnlyAttribute.prototype.value = function () {
            return this.val;
        };
        ReadOnlyAttribute.prototype.errors = function () {
            return this.errs;
        };
        ReadOnlyAttribute.prototype.is = function (val) {
            return this.value() === val;
        };
        ReadOnlyAttribute.prototype.description = function () {
            return '';
        };
        return ReadOnlyAttribute;
    }());

    var SimpleAttribute = /** @class */ (function () {
        function SimpleAttribute(_val, _label, _description) {
            if (_description === void 0) { _description = ''; }
            this._val = _val;
            this._label = _label;
            this._description = _description;
        }
        SimpleAttribute.prototype.value = function () {
            return this._val;
        };
        SimpleAttribute.prototype.setValue = function (value) {
            this._val = value;
            return this;
        };
        SimpleAttribute.prototype.label = function () {
            return this._label;
        };
        SimpleAttribute.prototype.description = function () {
            return this._description;
        };
        SimpleAttribute.prototype.errors = function () {
            return [];
        };
        SimpleAttribute.prototype.is = function (val) {
            return this._val === val;
        };
        return SimpleAttribute;
    }());

    var ACollection = /** @class */ (function (_super) {
        __extends(ACollection, _super);
        function ACollection() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @inheritDoc
         */
        ACollection.prototype.dataProvider = function () {
            if (typeof this['$dp'] === 'object') {
                return this['$dp'];
            }
            throw new Error('You should use any of data provider decorators');
        };
        /**
         *
         * @returns {string[]}
         */
        ACollection.prototype.fullTextSearchKeys = function () {
            return [];
        };
        /**
         * @inheritDoc
         */
        ACollection.prototype.filter = function () {
            return [];
        };
        /**
         * Provide list of keys that able to be sorted
         * @returns {string[]}
         */
        ACollection.prototype.sortKeys = function () {
            return [];
        };
        return ACollection;
    }(base_collection.BaseCollection));

    var BaseCollection = /** @class */ (function () {
        function BaseCollection() {
            this._data = [];
        }
        /**
         * @inheritDoc
         */
        BaseCollection.prototype.count = function () {
            return this._data.length;
        };
        /**
         * @inheritDoc
         */
        BaseCollection.prototype.data = function () {
            return this._data;
        };
        /**
         * @inheritDoc
         */
        BaseCollection.prototype.add = function (model) {
            this._data.push(model);
            return this;
        };
        /**
         * @inheritDoc
         */
        BaseCollection.prototype.clear = function () {
            this._data.splice(0, this.count());
            return this;
        };
        return BaseCollection;
    }());

    var ABaseModel = /** @class */ (function () {
        function ABaseModel() {
            this.ALLOW_NULL_VALUES = true;
            /**
             * Internal dictionary for keeping attributes
             * @private
             */
            this.attributes = {};
            this.enums = {};
            this.editMode = false;
            this.$services = {
                $onAttributesChanged: new core.EventEmitter(),
                $nullValuesMap: {
                    string: '',
                    number: 0,
                    boolean: false,
                    bool: false,
                    array: [],
                    date_int_ms: new Date(),
                },
                $errors: {},
                $scenario: {
                    active: {},
                    list: [],
                }
            };
            this.initEmptyAttributes();
        }
        ABaseModel.prototype.startEdit = function () {
            this.editMode = true;
            return this;
        };
        ABaseModel.prototype.finishEdit = function () {
            this.editMode = false;
            return this;
        };
        ABaseModel.prototype.isEditModeEnabled = function () {
            return this.editMode;
        };
        /**
         * Detect null values for each type
         * @param {*} type
         * @return {*}
         */
        ABaseModel.prototype.nullValueByType = function (type) {
            if (Array.isArray(type)) {
                return this.$services.$nullValuesMap.array;
            }
            if (typeof type === 'function') {
                var c = type;
                return new c();
            }
            if (this.$services.$nullValuesMap.hasOwnProperty(type)) {
                return this.$services.$nullValuesMap[type];
            }
            return undefined;
        };
        /**
         * Initi attributes after model construct
         * @return {IBaseModel}
         */
        ABaseModel.prototype.initEmptyAttributes = function () {
            var _this = this;
            Object
                .keys(this.schema())
                .forEach(function (n) { return _this.setAttributeBySchema(n, _this.nullValueByType(_this.schema()[n].$t)); });
            return this;
        };
        /**
         * Defining each attribute by declared schema
         * @param {string} name
         * @param value
         * @return {IBaseModel}
         */
        ABaseModel.prototype.setAttributeBySchema = function (name, value) {
            var _this = this;
            if (this.schema().hasOwnProperty(name)) {
                var cast_1 = function (name, value, type) {
                    /**@override IBaseModel this */
                    var t = type ? type : _this.schema()[name].$t;
                    if (typeof t === 'function') { // we think that it is another model
                        var a = (new t());
                        if (value != null) {
                            a.setAttributes(value);
                        }
                        return a;
                    }
                    switch (t) {
                        case 'string':
                            return String(value);
                        case 'date_int_ms':
                            return new Date(value);
                        case 'number':
                            return Number(value);
                        case 'bool':
                        case 'boolean':
                            if (value === '0') {
                                value = false;
                            }
                            if (value === '1') {
                                value = true;
                            }
                            return Boolean(value);
                    }
                };
                if (Array.isArray(this.schema()[name].$t)) {
                    this.attributes[name] = [];
                    if (this.schema()[name].$t.length !== 1) {
                        throw new Error("Allowed only one type for array definitions for attribute \"" + name + "\"");
                    }
                    if (!Array.isArray(value)) {
                        if (this.ALLOW_NULL_VALUES === true && value === null) {
                            value = [];
                        }
                        else {
                            throw new Error("Value must be array according to scheme definition for attribute \"" + name + "\"");
                        }
                    }
                    value.forEach(function (val) {
                        _this.attributes[name].push(cast_1(name, val, _this.schema()[name].$t[0]));
                    });
                }
                else {
                    this.attributes[name] = cast_1(name, value);
                }
                this.fillAttributesCopies();
            }
            return this;
        };
        ABaseModel.prototype.fillAttributesCopies = function () {
            var _this = this;
            Object
                .keys(this.schema())
                .forEach(function (attrName) {
                if (_this.schema()[attrName].$c &&
                    _this.attributes.hasOwnProperty(_this.schema()[attrName].$c)) {
                    _this.attributes[attrName] = _this.attributes[_this.schema()[attrName].$c];
                }
            });
            return this;
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.labels = function () {
            var _this = this;
            return Object
                .keys(this.schema())
                .reduce(function (p, c) {
                if (_this.schema()[c].$l) {
                    p[c] = _this.schema()[c].$l;
                    return p;
                }
                p[c] = c
                    .replace(/([A-Z])/g, ' $1')
                    .replace(/^./, function (str) {
                    return str.toUpperCase();
                });
                p[c] = c.split('_').map(function (word) {
                    return word.charAt(0).toUpperCase() + word.slice(1);
                }).join(' ');
                return p;
            }, {});
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.descriptions = function () {
            return {};
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.attribute = function (name) {
            return new attributes.AutoAttribute(this.labels(), this.attributes, name, this);
        };
        /**
         * Provide attribute value
         */
        ABaseModel.prototype.value = function (name) {
            return this.attribute(name).value();
        };
        /**
         * Provide label by attribute name
         */
        ABaseModel.prototype.label = function (name) {
            return this.attribute(name).label();
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.setAttribute = function (name, value) {
            return this.setAttributeBySchema(name, value);
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.setAttributes = function (attributes) {
            var _this = this;
            Object
                .keys(attributes)
                .forEach(function (name) {
                _this.setAttributeBySchema(name, attributes[name]);
            });
            return this;
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.validate = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.clearErrors();
                _this.$services.$errors = validate_js.validate(_this.attributes, Object
                    .keys(_this.schema())
                    .reduce(function (p, c) {
                    if (_this.schema()[c].$v) {
                        p[c] = _this.schema()[c].$v;
                    }
                    return p;
                }, {}));
                if (_this.$services.$errors === undefined) {
                    _this.$services.$errors = {};
                    resolve();
                }
                else {
                    reject();
                }
            });
        };
        /**
         * Represent this model as JSON object annotation
         * @return {*}
         */
        ABaseModel.prototype.toJSON = function () {
            var _this = this;
            return Object.keys(this.schema()).reduce(function (p, c, i, a) {
                if (_this.schema()[c].$t === 'date_int_ms') {
                    p[c] = _this.attributes[c].getTime();
                }
                else {
                    p[c] = _this.attributes[c];
                }
                return p;
            }, {});
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.errors = function (name) {
            return Array.isArray(this.$services.$errors[name]) ? this.$services.$errors[name] : [];
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.isValid = function () {
            return Object.keys(this.$services.$errors).length === 0;
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.clearErrors = function () {
            var _this = this;
            Object
                .keys(this.$services.$errors)
                .forEach(function (v) {
                delete _this.$services.$errors[v];
            });
            return this;
        };
        /**
         * @todo add ability to detect models
         * @param {Array<string>} names
         * @return {{[p: string]: any}}
         */
        ABaseModel.prototype.copyRawAttributes = function (names) {
            var _this = this;
            try {
                var ser = JSON.stringify(Object.keys(this.schema()).reduce(function (p, c) {
                    if (Array.isArray(names)) {
                        if (names.indexOf(c) !== -1) {
                            p[c] = _this.attribute(c).value();
                        }
                    }
                    else {
                        p[c] = _this.attribute(c).value();
                    }
                    return p;
                }, {}));
                return JSON.parse(ser);
            }
            catch (e) {
                console.log("model clone: could not to serialize / un serialize attributes!, message " + e.message, e);
            }
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.clone = function () {
            var c = this.constructor, cloned = new c;
            cloned.setAttributes(this.copyRawAttributes());
            return cloned;
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.onAttributesChanged = function () {
            return this.$services.$onAttributesChanged;
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.setError = function (name, message) {
            if (this.schema().hasOwnProperty(name)) {
                if (!Array.isArray(this.$services.$errors[name])) {
                    this.$services.$errors[name] = [];
                }
                this.$services.$errors[name].push(message);
            }
            return this;
        };
        /**
         * @inheritDoc
         */
        ABaseModel.prototype.setErrors = function (errors) {
            var _this = this;
            Object.keys(errors).forEach(function (name) {
                if (Array.isArray(errors[name])) {
                    errors[name].forEach(function (message) {
                        _this.setError(name, message);
                    });
                }
            });
            return this;
        };
        /**
         * @todo implement array path processing like items[3]name
         */
        ABaseModel.prototype.setErrorsByPath = function (path) {
            var message = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                message[_i - 1] = arguments[_i];
            }
            var steps = path.split('.');
            var model = this;
            var lastStepW = '';
            while (steps.length) {
                var lastStep = steps.shift();
                if (lastStep && lastStep.length > 2 && lastStep[0] === '[' && lastStep[lastStep.length - 1] === ']') {
                    var index = parseInt(lastStep.substr(1, lastStep.length - 1), 10);
                    lastStepW = steps.shift();
                    if (isNaN(index) === false && Array.isArray(model) && model.length - 1 >= index) {
                        model = model[index];
                    }
                    continue;
                }
                if (steps.length >= 1) {
                    // code for checking items[3]name must be here
                    try {
                        model = model.attribute(lastStep).value();
                    }
                    catch (e) {
                    }
                }
                lastStepW = lastStep;
            }
            if (model !== null && typeof model === 'object' && lastStepW !== '') {
                var errs = {};
                errs[lastStepW] = message;
                model.setErrors(errs);
            }
            return this;
        };
        ABaseModel.prototype.countErrors = function () {
            var _this = this;
            var paths = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                paths[_i] = arguments[_i];
            }
            var count = 0;
            paths.forEach(function (path) {
                count = _this.countErrorsInModel(count, _this, path.split('.'));
            });
            return count;
        };
        ABaseModel.prototype.countErrorsInModel = function (count, model, steps) {
            var _this = this;
            var lastStep = '';
            while (steps.length) {
                lastStep = steps.shift();
                if (lastStep === '[*]' && Array.isArray(model)) {
                    model.forEach(function (item) {
                        count = _this.countErrorsInModel(count, item, steps);
                    });
                }
                if (steps.length >= 1 &&
                    !Array.isArray(model) &&
                    model !== null &&
                    typeof model === 'object') {
                    try {
                        model = model.attribute(lastStep).value();
                    }
                    catch (e) {
                    }
                }
                if (steps.length === 0 &&
                    !Array.isArray(model) &&
                    model !== null &&
                    typeof model === 'object' &&
                    model.attribute(lastStep).errors().length > 0) {
                    count++;
                }
            }
            return count;
        };
        ABaseModel.prototype.setErrorsV2 = function (errors) {
            for (var errorsKey in errors) {
                if (errors.hasOwnProperty(errorsKey)) {
                    this.setErrorsByPath.apply(this, __spread([errors[errorsKey].attribute], errors[errorsKey].messages));
                }
            }
            return this;
        };
        ABaseModel.prototype.getDataToSave = function () {
            var _this = this;
            var data = {};
            Object
                .keys(this.schema())
                .forEach(function (attrName) {
                if (_this.schema()[attrName].$s) {
                    var value = _this.attribute(attrName).value();
                    data[attrName] = (typeof value.getDataToSave === 'function' ? value.getDataToSave() : value);
                }
            });
            Object.keys(data).forEach(function (key) {
                if (typeof data[key] === 'object' && Object.keys(data[key]).length === 0) {
                    delete data[key];
                }
                if (Array.isArray(data[key]) && data[key].length === 0) {
                    delete data[key];
                }
                if (!data[key]) {
                    delete data[key];
                }
            });
            return data;
        };
        ABaseModel.prototype.setScenario = function () {
        };
        ABaseModel.prototype.enum = function (name) {
            if (!this.enums.hasOwnProperty(name)) {
                throw new Error("enums have no property " + name);
            }
            return this.enums[name];
        };
        return ABaseModel;
    }());

    var EnumModel = /** @class */ (function () {
        function EnumModel(enumerable, textList) {
            var _this = this;
            this.vll = [];
            this.enumerable = enumerable;
            this.textList = textList;
            this.allValuesList = Object.keys(this.enumerable).map(function (key) { return _this.enum[key]; });
            this.allKeysList = Object.keys(this.enumerable);
        }
        Object.defineProperty(EnumModel.prototype, "enum", {
            get: function () {
                return this.enumerable;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EnumModel.prototype, "wordings", {
            get: function () {
                return this.textList;
            },
            enumerable: true,
            configurable: true
        });
        EnumModel.prototype.getAllValuesList = function () {
            return this.allValuesList;
        };
        EnumModel.prototype.getValueLabelList = function () {
            var _this = this;
            if (this.vll.length === 0) {
                this.allValuesList.forEach(function (val) {
                    _this.vll.push({
                        value: val,
                        label: _this.wordings[val]
                    });
                });
            }
            return this.vll;
        };
        EnumModel.prototype.getTextByValue = function (value) {
            if (!this.wordings[value]) {
                return 'Not in Enum!';
            }
            return this.wordings[value];
        };
        EnumModel.prototype.getTextByKey = function (key) {
            if (!this.wordings[this.enum[key]]) {
                return "Not wording for " + key + "! EnumModel: " + this + ".";
            }
            return this.wordings[this.enum[key]];
        };
        EnumModel.prototype.getValueByKey = function (key) {
            return this.enum[key];
        };
        return EnumModel;
    }());

    var AModel = /** @class */ (function (_super) {
        __extends(AModel, _super);
        function AModel() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @inheritDoc
         */
        AModel.prototype.dataProvider = function () {
            if (typeof this['$dp'] === 'object') {
                return this['$dp'];
            }
            throw new Error('You should use any of data provider decorators');
        };
        /**
         * @inheritDoc
         */
        AModel.prototype.pk = function () {
            if (this.schema()['id'] === undefined) {
                throw new Error("Could not to find default primary key in model.\nYou must redeclare pk() in model or define 'id' field in schema.");
            }
            return 'id';
        };
        /**
         * @inheritDoc
         */
        AModel.prototype.requestFormatter = function () {
            return {};
        };
        AModel.prototype.getDataToSave = function () {
            return _super.prototype.getDataToSave.call(this);
        };
        return AModel;
    }(base_model.ABaseModel));

    var BaseQuery = /** @class */ (function () {
        function BaseQuery() {
            this._limit = 20;
            this._offset = 0;
        }
        BaseQuery.prototype.limit = function () {
            return this._limit;
        };
        BaseQuery.prototype.offset = function () {
            return this._offset;
        };
        BaseQuery.prototype.toJSON = function () {
            return {
                forGrid: true,
                limit: this.limit(),
                skip: this.offset()
            };
        };
        BaseQuery.prototype.setLimit = function (limit) {
            this._limit = limit;
            return this;
        };
        BaseQuery.prototype.setOffset = function (offset) {
            this._offset = offset;
            return this;
        };
        return BaseQuery;
    }());

    var Filter = /** @class */ (function () {
        function Filter(filterItems) {
            this.filterItems = filterItems;
        }
        Filter.prototype.toRequestJSON = function () {
            return this
                .getSettings()
                .map(function (i) { return i.toResponse(); })
                .filter(function (i) {
                return (i.val !== null && i.val !== undefined) ||
                    (i.minVal !== null && i.minVal !== undefined) ||
                    (i.maxVal !== null && i.maxVal !== undefined);
            })
                .filter(function (i) {
                return !(Array.isArray(i.val) && i.val.length === 0);
            })
                .filter(function (i) {
                return !(i.type === queries.FilterType.ILIKE && !i.val);
            });
        };
        Filter.prototype.getSettings = function () {
            return this.filterItems;
        };
        return Filter;
    }());

    var ContentTypeInterceptor = /** @class */ (function () {
        function ContentTypeInterceptor() {
        }
        ContentTypeInterceptor.prototype.intercept = function (req, next) {
            if (req.body instanceof FormData) {
                return next.handle(req);
            }
            var authReq = req.clone({
                headers: req.headers
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
            });
            return next.handle(authReq);
        };
        ContentTypeInterceptor.ɵfac = function ContentTypeInterceptor_Factory(t) { return new (t || ContentTypeInterceptor)(); };
        ContentTypeInterceptor.ɵprov = core.ɵɵdefineInjectable({ token: ContentTypeInterceptor, factory: ContentTypeInterceptor.ɵfac });
        return ContentTypeInterceptor;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(ContentTypeInterceptor, [{
            type: core.Injectable
        }], function () { return []; }, null); })();

    var LangInterceptor = /** @class */ (function () {
        function LangInterceptor() {
            // private langService: LangService
        }
        LangInterceptor.prototype.intercept = function (req, next) {
            var authReq = req.clone({
            // headers: req.headers.set(this.langService.getLangHeader(), this.langService.getLangCode())
            });
            return next.handle(authReq);
        };
        LangInterceptor.ɵfac = function LangInterceptor_Factory(t) { return new (t || LangInterceptor)(); };
        LangInterceptor.ɵprov = core.ɵɵdefineInjectable({ token: LangInterceptor, factory: LangInterceptor.ɵfac });
        return LangInterceptor;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(LangInterceptor, [{
            type: core.Injectable
        }], function () { return []; }, null); })();

    var SystemErrorInterceptor = /** @class */ (function () {
        function SystemErrorInterceptor() {
        }
        SystemErrorInterceptor.prototype.intercept = function (req, next) {
            return next.handle(req)
                .pipe(operators.catchError(function (response) {
                if (response instanceof http.HttpErrorResponse) {
                    var error = response.error;
                    var systemError = error && error.errors ? error.errors.find(function (item) { return item.attribute === 'system'; }) : null;
                    if (systemError) {
                        // systemError.messages.forEach((message => this.toast.danger(message)));
                    }
                }
                return rxjs.of(response);
            }));
        };
        SystemErrorInterceptor.ɵfac = function SystemErrorInterceptor_Factory(t) { return new (t || SystemErrorInterceptor)(); };
        SystemErrorInterceptor.ɵprov = core.ɵɵdefineInjectable({ token: SystemErrorInterceptor, factory: SystemErrorInterceptor.ɵfac });
        return SystemErrorInterceptor;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(SystemErrorInterceptor, [{
            type: core.Injectable
        }], function () { return []; }, null); })();


    (function (ActionNames) {
        ActionNames["PLinkAction"] = "p-link-action";
        ActionNames["PSearchAction"] = "p-search-action";
        ActionNames["PModalAction"] = "p-modal-action";
        ActionNames["PModelBulkRemoveAction"] = "p-model-bulk-remove-action";
        ActionNames["PCollectionBulkRemoveAction"] = "p-collection-bulk-remove-action";
        ActionNames["PCollectionChooseItemsAction"] = "p-collection-choose-items-action";
    })(exports.ActionNames || (exports.ActionNames = {}));

    var SORT = {
        ASC: 'asc',
        DESC: 'desc'
    };


    (function (FilterType) {
        FilterType["EQ"] = "eq";
        FilterType["NEQ"] = "neq";
        FilterType["IN"] = "in";
        FilterType["IN_ENUM"] = "in_enum";
        FilterType["ENUM"] = "enum";
        FilterType["GT"] = "gt";
        FilterType["LT"] = "lt";
        FilterType["GTE"] = "gte";
        FilterType["LTE"] = "lte";
        FilterType["ILIKE"] = "ilike";
        FilterType["RANGE"] = "range";
        FilterType["DATE"] = "date";
    })(exports.FilterType || (exports.FilterType = {}));
    var FilterAttribute = /** @class */ (function () {
        function FilterAttribute(lbl, initVal) {
            this.lbl = lbl;
            this.val = null;
            if (initVal !== undefined) {
                this.val = initVal;
            }
        }
        FilterAttribute.prototype.description = function () {
            return '';
        };
        FilterAttribute.prototype.errors = function () {
            return [];
        };
        FilterAttribute.prototype.is = function (val) {
            return val === this.val;
        };
        FilterAttribute.prototype.label = function () {
            return this.lbl;
        };
        FilterAttribute.prototype.setValue = function (value) {
            this.val = value;
            return this;
        };
        FilterAttribute.prototype.value = function () {
            return this.val;
        };
        return FilterAttribute;
    }());
    var FilterItem = /** @class */ (function () {
        function FilterItem(setting) {
            this.setting = setting;
            this.val = null;
            this.minVal = null;
            this.maxVal = null;
            this.enumVal = {};
        }
        FilterItem.prototype.getAlias = function () {
            try {
                return this.setting.alias;
            }
            catch (e) {
                return '';
            }
        };
        FilterItem.prototype.getName = function () {
            return this.setting.name;
        };
        FilterItem.prototype.getType = function () {
            return this.setting.type;
        };
        FilterItem.prototype.max = function () {
            if (this.maxVal === null) {
                this.maxVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === exports.FilterType.DATE ? 0 : null);
            }
            return this.maxVal;
        };
        FilterItem.prototype.min = function () {
            if (this.minVal === null) {
                this.minVal = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name, this.getType() === exports.FilterType.DATE ? 0 : null);
            }
            return this.minVal;
        };
        FilterItem.prototype.vEnum = function (index) {
            if (!this.enumVal.hasOwnProperty(index)) {
                this.enumVal[index] = new FilterAttribute(this.setting.enum.getTextByKey(index), false);
            }
            return this.enumVal[index];
        };
        FilterItem.prototype.v = function () {
            if (this.val === null) {
                this.val = new FilterAttribute(this.setting.label ? this.setting.label : this.setting.name);
            }
            return this.val;
        };
        FilterItem.prototype.getEnum = function () {
            return this.setting.enum;
        };
        FilterItem.prototype.toResponse = function () {
            var _this = this;
            var result = {
                type: this.getType(),
                name: this.setting.name,
                alias: this.setting.alias,
            };
            switch (this.getType()) {
                case exports.FilterType.IN_ENUM:
                    result.val = Object
                        .keys(this.enumVal)
                        .map(function (key) {
                        return {
                            key: key,
                            enabled: !!_this.enumVal[key].value()
                        };
                    })
                        .filter(function (i) {
                        return i.enabled === true;
                    })
                        .map(function (i) {
                        console.log(i.key);
                        return _this.setting.enum.enum[i.key];
                    });
                    break;
                case exports.FilterType.DATE:
                    result.minVal = this.min().value();
                    result.maxVal = this.max().value();
                    if (result.minVal !== 0 && (result.minVal === result.maxVal)) {
                        result.maxVal += (3600 * 24) - 1; // adding one day minus last second
                    }
                    /* if (this.min().value() < this.max().value()) {
                       result.minVal = this.min().value();
                       result.maxVal = this.max().value();
                     } else {
                       result.minVal = this.max().value();
                       result.maxVal = this.min().value();
                     }*/
                    break;
                default:
                    result.val = this.v().value();
                    break;
            }
            return result;
        };
        return FilterItem;
    }());
    var FilterSettingsFactory = /** @class */ (function () {
        function FilterSettingsFactory() {
        }
        FilterSettingsFactory.fromSettings = function () {
            var settings = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                settings[_i] = arguments[_i];
            }
            return settings.map(function (item) {
                return new FilterItem(item);
            });
        };
        FilterSettingsFactory.fromSettingsAndModel = function (model) {
            var settings = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                settings[_i - 1] = arguments[_i];
            }
            return settings.map(function (item) {
                item.label = model.attribute(item.name).label();
                return new FilterItem(item);
            });
        };
        return FilterSettingsFactory;
    }());

    var ServiceLocator = /** @class */ (function () {
        function ServiceLocator() {
        }
        ServiceLocator.http = function () {
            return ServiceLocator.injector.get(http.HttpClient);
        };
        ServiceLocator.colQuery = function () {
            return ServiceLocator.injector.get(collectionQuery.CollectionQuery);
        };
        /**
         * Provide data provider factory
         */
        ServiceLocator.setDataProviderFactory = function (dpf) {
            ServiceLocator.dataProviderFactory = dpf;
        };
        /**
         * Provide instance of data provider
         */
        ServiceLocator.getDataProviderFactory = function () {
            return ServiceLocator.dataProviderFactory;
        };
        return ServiceLocator;
    }());

    var CollectionQuery = /** @class */ (function (_super) {
        __extends(CollectionQuery, _super);
        function CollectionQuery() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.s = {};
            _this.q = {};
            _this._sort = {};
            _this._query = {};
            return _this;
        }
        CollectionQuery.prototype.setTerm = function (term, fts) {
            if (fts === void 0) { fts = []; }
            try {
                this.q = JSON.parse(term);
                return this;
            }
            catch (e) {
            }
            if (fts.length === 0) {
                fts.push('name');
            }
            this.q = term ? fts.reduce(function (p, c) {
                var t = {};
                t[c] = { '$regex': term, $options: 'i' };
                p.$or.push(t);
                return p;
            }, { $or: [] }) : {};
            return this;
        };
        // toJSON(): any {
        //     return {
        //         forGrid: true,
        //         jsonQuery: JSON.stringify(this.q),
        //         limit: this.limit(),
        //         skip: this.offset()
        //     };
        // }
        CollectionQuery.prototype.toJSON = function () {
            var settings = [];
            try {
                settings = this.filterSettings.toRequestJSON();
            }
            catch (e) {
            }
            return {
                filter: settings,
                limit: this.limit(),
                skip: this.offset(),
                sort: this.sort(),
            };
        };
        CollectionQuery.prototype.setFilter = function (filter) {
            this.filterSettings = filter;
            return this;
        };
        CollectionQuery.prototype.filter = function () {
            return this.q;
        };
        CollectionQuery.prototype.sort = function () {
            return this.s;
        };
        /**
         *
         * @param {string} attributeName
         * @param {string} direction
         * @returns {this}
         */
        CollectionQuery.prototype.setSort = function (attributeName, direction) {
            var _this = this;
            if ([ICollectionQuery.SORT.ASC, ICollectionQuery.SORT.DESC].indexOf(direction) === -1) {
                return this;
            }
            Object.keys(this.s).forEach(function (n) {
                delete _this.s[n];
            });
            switch (direction) {
                case ICollectionQuery.SORT.ASC:
                    this.s[attributeName] = 1;
                    break;
                case ICollectionQuery.SORT.DESC:
                    this.s[attributeName] = -1;
            }
            return this;
        };
        CollectionQuery.ɵfac = function CollectionQuery_Factory(t) { return ɵCollectionQuery_BaseFactory(t || CollectionQuery); };
        CollectionQuery.ɵprov = core.ɵɵdefineInjectable({ token: CollectionQuery, factory: CollectionQuery.ɵfac });
        return CollectionQuery;
    }(queries$1.BaseQuery));
    var ɵCollectionQuery_BaseFactory = core.ɵɵgetInheritedFactory(CollectionQuery);
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(CollectionQuery, [{
            type: core.Injectable
        }], null, null); })();

    var ModelList = /** @class */ (function () {
        function ModelList() {
            this._list = [];
            this._em = new core.EventEmitter();
        }
        /**
         * @return EventEmitter<IModelListActionEvent>
         */
        ModelList.prototype.em = function () {
            return this._em;
        };
        /**
         * Provide reference for list
         * @return {IModel[]}
         */
        ModelList.prototype.list = function () {
            return this._list;
        };
        ModelList.ɵfac = function ModelList_Factory(t) { return new (t || ModelList)(); };
        ModelList.ɵprov = core.ɵɵdefineInjectable({ token: ModelList, factory: ModelList.ɵfac });
        return ModelList;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(ModelList, [{
            type: core.Injectable
        }], null, null); })();

    exports.ABaseModel = ABaseModel;
    exports.ACollection = ACollection;
    exports.AModel = AModel;
    exports.Attribute = Attribute;
    exports.AutoAttribute = AutoAttribute;
    exports.BaseCollection = BaseCollection;
    exports.BaseQuery = BaseQuery;
    exports.CollectionProviderDecorator = CollectionProviderDecorator;
    exports.CollectionQuery = CollectionQuery;
    exports.ContentTypeInterceptor = ContentTypeInterceptor;
    exports.EnumModel = EnumModel;
    exports.Filter = Filter;
    exports.FilterItem = FilterItem;
    exports.FilterSettingsFactory = FilterSettingsFactory;
    exports.LangInterceptor = LangInterceptor;
    exports.ModelList = ModelList;
    exports.ModelProviderDecorator = ModelProviderDecorator;
    exports.ReadOnlyAttribute = ReadOnlyAttribute;
    exports.SORT = SORT;
    exports.ServiceLocator = ServiceLocator;
    exports.SimpleAttribute = SimpleAttribute;
    exports.SystemErrorInterceptor = SystemErrorInterceptor;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=core-ui.umd.js.map
